import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/member_data_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MemberDataEntity {

	late int status=0;
	late MemberDataMessage message;
  
  MemberDataEntity();

  factory MemberDataEntity.fromJson(Map<String, dynamic> json) => $MemberDataEntityFromJson(json);

  Map<String, dynamic> toJson() => $MemberDataEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class MemberDataMessage {

	late String id="";
	@JSONField(name: "full_name")
	late String fullName="";
	@JSONField(name: "reg_id")
	late String regId="";
	@JSONField(name: "reg_code")
	late String regCode="";
	@JSONField(name: "country_id")
	late String countryId="";
	@JSONField(name: "state_id")
	late String stateId="";
	late String mobile="";
	@JSONField(name: "email_id")
	late String emailId="";
	late String currency="";
	@JSONField(name: "join_date")
	late String joinDate="";
	@JSONField(name: "activation_date")
	late String activationDate="";
	@JSONField(name: "activation_key")
	dynamic activationKey="";
	@JSONField(name: "join_source")
	late String joinSource="";
	@JSONField(name: "used_link_for_registration")
	late String usedLinkForRegistration="";
	@JSONField(name: "sponser_reg_id")
	late String sponserRegId="";
	@JSONField(name: "sponser_reg_code")
	late String sponserRegCode="";
	@JSONField(name: "parent_reg_id")
	late String parentRegId="";
	@JSONField(name: "parent_reg_code")
	late String parentRegCode="";
	late String username="";
	@JSONField(name: "encr_password")
	late String encrPassword="";
	@JSONField(name: "default_lang")
	late String defaultLang="";
	@JSONField(name: "sponsered_count")
	late String sponseredCount="";
	@JSONField(name: "sponsed_count_left")
	late String sponsedCountLeft="";
	@JSONField(name: "sponsed_count_right")
	late String sponsedCountRight="";
	late String position="";
	@JSONField(name: "position_downline_setup_default")
	late String positionDownlineSetupDefault="";
	@JSONField(name: "position_next")
	late String positionNext="";
	@JSONField(name: "bank_account_no")
	late String bankAccountNo="";
	@JSONField(name: "bank_account_name")
	late String bankAccountName="";
	@JSONField(name: "bank_name")
	late String bankName="";
	@JSONField(name: "branch_name")
	late String branchName="";
	@JSONField(name: "pan_card")
	dynamic panCard="";
	@JSONField(name: "binary_left")
	late String binaryLeft="";
	@JSONField(name: "binary_right")
	late String binaryRight="";
	@JSONField(name: "binary_matched")
	late String binaryMatched="";
	@JSONField(name: "carry_left")
	late String carryLeft="";
	@JSONField(name: "carry_right")
	late String carryRight="";
	@JSONField(name: "binary_amt")
	late String binaryAmt="";
	@JSONField(name: "binary_amt_pre")
	late String binaryAmtPre="";
	@JSONField(name: "binary_lastgn_date")
	late String binaryLastgnDate="";
	@JSONField(name: "referral_commission_amt")
	late String referralCommissionAmt="";
	@JSONField(name: "referral_commission_amt_pre")
	late String referralCommissionAmtPre="";
	@JSONField(name: "referral_commission_lastgn_date")
	late String referralCommissionLastgnDate="";
	@JSONField(name: "level_amt")
	late String levelAmt="";
	@JSONField(name: "level_amt_pre")
	late String levelAmtPre="";
	@JSONField(name: "level_lastgn_date")
	late String levelLastgnDate="";
	@JSONField(name: "last_achieved_rank")
	late String lastAchievedRank="";
	@JSONField(name: "achievement_amt")
	late String achievementAmt="";
	@JSONField(name: "achievement_amt_pre")
	late String achievementAmtPre="";
	@JSONField(name: "achievement_lastgn_date")
	dynamic achievementLastgnDate="";
	@JSONField(name: "member_cancel")
	late String memberCancel="";
	@JSONField(name: "member_terminated")
	late String memberTerminated="";
	@JSONField(name: "profile_photo")
	late String profilePhoto="";
	@JSONField(name: "pan_no")
	late String panNo="";
	@JSONField(name: "pan_photo")
	late String panPhoto="";
	@JSONField(name: "adhar_no")
	late String adharNo="";
	@JSONField(name: "adhar_photo")
	late String adharPhoto="";
	@JSONField(name: "id_no")
	dynamic idNo="";
	@JSONField(name: "idproof_photo")
	dynamic idproofPhoto="";
	@JSONField(name: "bank_checkleaf_photo")
	dynamic bankCheckleafPhoto="";
	dynamic branch="";
	late String ifsc="";
	@JSONField(name: "pan_varified")
	late String panVarified="";
	@JSONField(name: "pan_varify_date")
	dynamic panVarifyDate="";
	@JSONField(name: "adhar_varified")
	late String adharVarified="";
	@JSONField(name: "adhar_varify_date")
	dynamic adharVarifyDate="";
	@JSONField(name: "idproof_varified")
	late String idproofVarified="";
	@JSONField(name: "idproof_varify_date")
	dynamic idproofVarifyDate="";
	@JSONField(name: "bank_varified")
	late String bankVarified="";
	@JSONField(name: "bank_varify_date")
	dynamic bankVarifyDate="";
	late String updatestatus="";
	@JSONField(name: "remark_varification")
	late String remarkVarification="";
	@JSONField(name: "last_varified_by")
	dynamic lastVarifiedBy="";
	@JSONField(name: "last_varified_date")
	dynamic lastVarifiedDate="";
	@JSONField(name: "member_status")
	late String memberStatus="";
	@JSONField(name: "member_entry_date")
	late String memberEntryDate="";
	@JSONField(name: "free_member_count")
	late String freeMemberCount="";
	@JSONField(name: "group_entry_date")
	dynamic groupEntryDate="";
	@JSONField(name: "group_count")
	late String groupCount="";
	@JSONField(name: "groupadmin_reg_id")
	dynamic groupadminRegId="";
	@JSONField(name: "group_admin_status")
	late String groupAdminStatus="";
	dynamic relationship="";
	@JSONField(name: "related_appuser_name")
	dynamic relatedAppuserName="";
	@JSONField(name: "cancellation_date")
	dynamic cancellationDate="";
	@JSONField(name: "cancellation_reason")
	dynamic cancellationReason="";
	@JSONField(name: "nominee_name")
	late String nomineeName="";
	@JSONField(name: "nominee_relation")
	late String nomineeRelation="";
	@JSONField(name: "nominee_id_no")
	late String nomineeIdNo="";
	@JSONField(name: "nominee_id_path")
	dynamic nomineeIdPath="";
	@JSONField(name: "auto_pool_member")
	late String autoPoolMember="";
	@JSONField(name: "auto_pool_entry_date")
	dynamic autoPoolEntryDate="";
	@JSONField(name: "autopool_deposit")
	late String autopoolDeposit="";
	@JSONField(name: "autopool_collected")
	late String autopoolCollected="";
	@JSONField(name: "autopool_agreed")
	dynamic autopoolAgreed="";
	@JSONField(name: "autopool_group")
	late String autopoolGroup="";
	@JSONField(name: "block_level")
	late String blockLevel="";
	@JSONField(name: "actvation_product")
	late String actvationProduct="";
	@JSONField(name: "maths_shift_date")
	dynamic mathsShiftDate="";
	@JSONField(name: "wondermaths_id")
	dynamic wondermathsId="";
  
  MemberDataMessage();

  factory MemberDataMessage.fromJson(Map<String, dynamic> json) => $MemberDataMessageFromJson(json);

  Map<String, dynamic> toJson() => $MemberDataMessageToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}