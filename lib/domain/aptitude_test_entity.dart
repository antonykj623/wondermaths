import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/aptitude_test_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class AptitudeTestEntity {

	late int status=0;
	late List<AptitudeTestData> data=[];
	late String message="";
  
  AptitudeTestEntity();

  factory AptitudeTestEntity.fromJson(Map<String, dynamic> json) => $AptitudeTestEntityFromJson(json);

  Map<String, dynamic> toJson() => $AptitudeTestEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class AptitudeTestData {

	late String id="";
	late String param1="";
	dynamic param2="";
	@JSONField(name: "test_name")
	late String testName="";
	@JSONField(name: "created_ts")
	late String createdTs="";
	@JSONField(name: "updated_ts")
	late String updatedTs="";
  
  AptitudeTestData();

  factory AptitudeTestData.fromJson(Map<String, dynamic> json) => $AptitudeTestDataFromJson(json);

  Map<String, dynamic> toJson() => $AptitudeTestDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}