import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/speed_maths_data_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class SpeedMathsDataEntity {

	late int status=0;
	late String message="";
	late List<SpeedMathsDataData> data=[];
  
  SpeedMathsDataEntity();

  factory SpeedMathsDataEntity.fromJson(Map<String, dynamic> json) => $SpeedMathsDataEntityFromJson(json);

  Map<String, dynamic> toJson() => $SpeedMathsDataEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class SpeedMathsDataData {

	late String name="";
  
  SpeedMathsDataData();

  factory SpeedMathsDataData.fromJson(Map<String, dynamic> json) => $SpeedMathsDataDataFromJson(json);

  Map<String, dynamic> toJson() => $SpeedMathsDataDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}