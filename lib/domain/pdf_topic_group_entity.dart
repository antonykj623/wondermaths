import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/pdf_topic_group_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class PdfTopicGroupEntity {

	late int status=0;
	late List<PdfTopicGroupData> data=[];
	late String message="";
  
  PdfTopicGroupEntity();

  factory PdfTopicGroupEntity.fromJson(Map<String, dynamic> json) => $PdfTopicGroupEntityFromJson(json);

  Map<String, dynamic> toJson() => $PdfTopicGroupEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class PdfTopicGroupData {

	@JSONField(name: "Id")
	late String id="";
	@JSONField(name: "Topic")
	late String topic="";
	@JSONField(name: "SrNo")
	late String srNo="";
	late String priority="";
	@JSONField(name: "Subject")
	late String subject="";
	@JSONField(name: "Chapter")
	late String chapter="";
	@JSONField(name: "FileName")
	late String fileName="";
	@JSONField(name: "CBSC")
	late String cBSC="";
	@JSONField(name: "State")
	late String state="";
	late String topicId="";
	late String description="";
  
  PdfTopicGroupData();

  factory PdfTopicGroupData.fromJson(Map<String, dynamic> json) => $PdfTopicGroupDataFromJson(json);

  Map<String, dynamic> toJson() => $PdfTopicGroupDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}