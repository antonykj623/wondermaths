import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/video_by_class_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class VideoByClassEntity {

	late int status=0;
	late List<VideoByClassData> data=[];
	late String message="";
  
  VideoByClassEntity();

  factory VideoByClassEntity.fromJson(Map<String, dynamic> json) => $VideoByClassEntityFromJson(json);

  Map<String, dynamic> toJson() => $VideoByClassEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class VideoByClassData {

	@JSONField(name: "Id")
	late String id="";
	@JSONField(name: "Subject")
	late String subject="";
	@JSONField(name: "Topic")
	late String topic="";
	@JSONField(name: "Link")
	late String link="";
	@JSONField(name: "Class")
	late String xClass="";
  
  VideoByClassData();

  factory VideoByClassData.fromJson(Map<String, dynamic> json) => $VideoByClassDataFromJson(json);

  Map<String, dynamic> toJson() => $VideoByClassDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}