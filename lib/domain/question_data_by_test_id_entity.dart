import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/question_data_by_test_id_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class QuestionDataByTestIdEntity {

	late int status=0;
	late List<QuestionDataByTestIdData> data=[];
	late String message="";
  
  QuestionDataByTestIdEntity();

  factory QuestionDataByTestIdEntity.fromJson(Map<String, dynamic> json) => $QuestionDataByTestIdEntityFromJson(json);

  Map<String, dynamic> toJson() => $QuestionDataByTestIdEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class QuestionDataByTestIdData {

	@JSONField(name: "question_options")
	late List<QuestionDataByTestIdDataQuestionOptions> questionOptions=[];
	late List<QuestionDataByTestIdDataQuestion> question=[];
  
  QuestionDataByTestIdData();

  factory QuestionDataByTestIdData.fromJson(Map<String, dynamic> json) => $QuestionDataByTestIdDataFromJson(json);

  Map<String, dynamic> toJson() => $QuestionDataByTestIdDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class QuestionDataByTestIdDataQuestionOptions {

	late String id="";
	@JSONField(name: "question_id")
	late String questionId="";
	@JSONField(name: "opt_number")
	late String optNumber="";
	late String option="";
	@JSONField(name: "created_at")
	late String createdAt="";
	@JSONField(name: "updated_at")
	late String updatedAt="";

	int selected=0;
  
  QuestionDataByTestIdDataQuestionOptions();

  factory QuestionDataByTestIdDataQuestionOptions.fromJson(Map<String, dynamic> json) => $QuestionDataByTestIdDataQuestionOptionsFromJson(json);

  Map<String, dynamic> toJson() => $QuestionDataByTestIdDataQuestionOptionsToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class QuestionDataByTestIdDataQuestion {

	late String id;
	@JSONField(name: "course_id")
	late String courseId;
	@JSONField(name: "section_id")
	late String sectionId;
	@JSONField(name: "part_no")
	late String partNo;
	@JSONField(name: "question_no")
	late String questionNo;
	late String question;
	late String answer;
	late String explanation;
	late String status;
	@JSONField(name: "created_at")
	late String createdAt;
	@JSONField(name: "updated_at")
	late String updatedAt;
	@JSONField(name: "status_w_h")
	late String statusWH;
  
  QuestionDataByTestIdDataQuestion();

  factory QuestionDataByTestIdDataQuestion.fromJson(Map<String, dynamic> json) => $QuestionDataByTestIdDataQuestionFromJson(json);

  Map<String, dynamic> toJson() => $QuestionDataByTestIdDataQuestionToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}