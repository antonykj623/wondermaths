import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/version_data_app_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class VersionDataAppEntity {

	late int status=0;
	late VersionDataAppData data;
	late String message="";
  
  VersionDataAppEntity();

  factory VersionDataAppEntity.fromJson(Map<String, dynamic> json) => $VersionDataAppEntityFromJson(json);

  Map<String, dynamic> toJson() => $VersionDataAppEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class VersionDataAppData {

	late String id="";
	late String versionname="";
  
  VersionDataAppData();

  factory VersionDataAppData.fromJson(Map<String, dynamic> json) => $VersionDataAppDataFromJson(json);

  Map<String, dynamic> toJson() => $VersionDataAppDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}