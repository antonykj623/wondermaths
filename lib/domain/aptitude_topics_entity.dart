import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/aptitude_topics_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class AptitudeTopicsEntity {

	late int status=0;
	late List<AptitudeTopicsData> data=[];
	late String message="";
  
  AptitudeTopicsEntity();

  factory AptitudeTopicsEntity.fromJson(Map<String, dynamic> json) => $AptitudeTopicsEntityFromJson(json);

  Map<String, dynamic> toJson() => $AptitudeTopicsEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class AptitudeTopicsData {

	@JSONField(name: "Id")
	late String id="";
	@JSONField(name: "Topic")
	late String topic="";
	@JSONField(name: "SrNo")
	late String srNo="";
	late String priority="";
	@JSONField(name: "Subject")
	late String subject="";
	@JSONField(name: "Chapter")
	late String chapter="";
	@JSONField(name: "FileName")
	late String fileName="";
	@JSONField(name: "CBSC")
	late String cBSC="";
	@JSONField(name: "State")
	late String state="";
	late String topicId="";
	late String description="";
  
  AptitudeTopicsData();

  factory AptitudeTopicsData.fromJson(Map<String, dynamic> json) => $AptitudeTopicsDataFromJson(json);

  Map<String, dynamic> toJson() => $AptitudeTopicsDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}