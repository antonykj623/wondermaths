import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/slider_data_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class SliderDataEntity {

	late int status=0;
	late List<SliderDataData> data=[];
	late String message="";
  
  SliderDataEntity();

  factory SliderDataEntity.fromJson(Map<String, dynamic> json) => $SliderDataEntityFromJson(json);

  Map<String, dynamic> toJson() => $SliderDataEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class SliderDataData {

	late String id="";
	late String name="";
	late String status="";
  
  SliderDataData();

  factory SliderDataData.fromJson(Map<String, dynamic> json) => $SliderDataDataFromJson(json);

  Map<String, dynamic> toJson() => $SliderDataDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}