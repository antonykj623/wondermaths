import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/class_sylabus_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class ClassSylabusEntity {

	late int status=0;
	late List<ClassSylabusData> data=[];
	late String message="";
  
  ClassSylabusEntity();

  factory ClassSylabusEntity.fromJson(Map<String, dynamic> json) => $ClassSylabusEntityFromJson(json);

  Map<String, dynamic> toJson() => $ClassSylabusEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class ClassSylabusData {

	late String id="";
	late String name="";
	late List<ClassSylabusDataClassesdata> classesdata=[];
  
  ClassSylabusData();

  factory ClassSylabusData.fromJson(Map<String, dynamic> json) => $ClassSylabusDataFromJson(json);

  Map<String, dynamic> toJson() => $ClassSylabusDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class ClassSylabusDataClassesdata {

	late String id="";
	late String name="";
	@JSONField(name: "syllabus_id")
	late String syllabusId="";
	late String status="";
  int selected=0;
  
  ClassSylabusDataClassesdata();

  factory ClassSylabusDataClassesdata.fromJson(Map<String, dynamic> json) => $ClassSylabusDataClassesdataFromJson(json);

  Map<String, dynamic> toJson() => $ClassSylabusDataClassesdataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}