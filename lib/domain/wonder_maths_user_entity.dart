import 'package:myapp/generated/json/base/json_field.dart';
import 'package:myapp/generated/json/wonder_maths_user_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class WonderMathsUserEntity {

	late int status;
	late WonderMathsUserData data;
	late String message;
  
  WonderMathsUserEntity();

  factory WonderMathsUserEntity.fromJson(Map<String, dynamic> json) => $WonderMathsUserEntityFromJson(json);

  Map<String, dynamic> toJson() => $WonderMathsUserEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class WonderMathsUserData {

	late String id;
	@JSONField(name: "full_name")
	late String fullName;
	@JSONField(name: "reg_code")
	late String regCode;
	@JSONField(name: "country_id")
	late String countryId;
	@JSONField(name: "state_id")
	late String stateId;
	late String mobile;
	@JSONField(name: "profile_image")
	late String profileImage;
	@JSONField(name: "email_id")
	late String emailId;
	late String currency;
	@JSONField(name: "join_date")
	late String joinDate;
	@JSONField(name: "activation_date")
	late String activationDate;
	@JSONField(name: "activation_key")
	dynamic activationKey;
	@JSONField(name: "join_source")
	late String joinSource;
	@JSONField(name: "used_link_for_registration")
	late String usedLinkForRegistration;
	@JSONField(name: "sp_reg_id")
	late String spRegId;
	@JSONField(name: "device_id")
	late String deviceId;
	@JSONField(name: "w_device_id")
	late String wDeviceId;
	@JSONField(name: "w_platform")
	late String wPlatform;
	@JSONField(name: "sp_reg_code")
	late String spRegCode;
	@JSONField(name: "default_lang")
	late String defaultLang;
	late String username;
	@JSONField(name: "encr_password")
	late String encrPassword;
	@JSONField(name: "gdrive_fileid")
	late String gdriveFileid;
	@JSONField(name: "unique_deviceId")
	late String uniqueDeviceid;
	@JSONField(name: "member_status")
	late String memberStatus;
	@JSONField(name: "reselling_partner")
	late String resellingPartner;
	late String coupon;
	@JSONField(name: "coup_stus")
	late String coupStus;
	@JSONField(name: "current_app_version")
	late String currentAppVersion;
	@JSONField(name: "phone_type")
	late String phoneType;
	@JSONField(name: "drive_mailId")
	late String driveMailid;
	@JSONField(name: "serverbackup_fileid")
	late String serverbackupFileid;
	@JSONField(name: "maths_trial_number")
	late String mathsTrialNumber;
	@JSONField(name: "maths_trial_status")
	late String mathsTrialStatus;
	@JSONField(name: "link_active")
	late String linkActive;
	@JSONField(name: "w_total_pts")
	late String wTotalPts;
	@JSONField(name: "w_redeemed_pts")
	late String wRedeemedPts;
	@JSONField(name: "w_balance_pts")
	late String wBalancePts;
  
  WonderMathsUserData();

  factory WonderMathsUserData.fromJson(Map<String, dynamic> json) => $WonderMathsUserDataFromJson(json);

  Map<String, dynamic> toJson() => $WonderMathsUserDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}