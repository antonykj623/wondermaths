import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/question_data_by_test_id_entity.dart';

QuestionDataByTestIdEntity $QuestionDataByTestIdEntityFromJson(Map<String, dynamic> json) {
	final QuestionDataByTestIdEntity questionDataByTestIdEntity = QuestionDataByTestIdEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		questionDataByTestIdEntity.status = status;
	}
	final List<QuestionDataByTestIdData>? data = jsonConvert.convertListNotNull<QuestionDataByTestIdData>(json['data']);
	if (data != null) {
		questionDataByTestIdEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		questionDataByTestIdEntity.message = message;
	}
	return questionDataByTestIdEntity;
}

Map<String, dynamic> $QuestionDataByTestIdEntityToJson(QuestionDataByTestIdEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

QuestionDataByTestIdData $QuestionDataByTestIdDataFromJson(Map<String, dynamic> json) {
	final QuestionDataByTestIdData questionDataByTestIdData = QuestionDataByTestIdData();
	final List<QuestionDataByTestIdDataQuestionOptions>? questionOptions = jsonConvert.convertListNotNull<QuestionDataByTestIdDataQuestionOptions>(json['question_options']);
	if (questionOptions != null) {
		questionDataByTestIdData.questionOptions = questionOptions;
	}
	final List<QuestionDataByTestIdDataQuestion>? question = jsonConvert.convertListNotNull<QuestionDataByTestIdDataQuestion>(json['question']);
	if (question != null) {
		questionDataByTestIdData.question = question;
	}
	return questionDataByTestIdData;
}

Map<String, dynamic> $QuestionDataByTestIdDataToJson(QuestionDataByTestIdData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['question_options'] =  entity.questionOptions.map((v) => v.toJson()).toList();
	data['question'] =  entity.question.map((v) => v.toJson()).toList();
	return data;
}

QuestionDataByTestIdDataQuestionOptions $QuestionDataByTestIdDataQuestionOptionsFromJson(Map<String, dynamic> json) {
	final QuestionDataByTestIdDataQuestionOptions questionDataByTestIdDataQuestionOptions = QuestionDataByTestIdDataQuestionOptions();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		questionDataByTestIdDataQuestionOptions.id = id;
	}
	final String? questionId = jsonConvert.convert<String>(json['question_id']);
	if (questionId != null) {
		questionDataByTestIdDataQuestionOptions.questionId = questionId;
	}
	final String? optNumber = jsonConvert.convert<String>(json['opt_number']);
	if (optNumber != null) {
		questionDataByTestIdDataQuestionOptions.optNumber = optNumber;
	}
	final String? option = jsonConvert.convert<String>(json['option']);
	if (option != null) {
		questionDataByTestIdDataQuestionOptions.option = option;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		questionDataByTestIdDataQuestionOptions.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		questionDataByTestIdDataQuestionOptions.updatedAt = updatedAt;
	}
	final int? selected = jsonConvert.convert<int>(json['selected']);
	if (selected != null) {
		questionDataByTestIdDataQuestionOptions.selected = selected;
	}
	return questionDataByTestIdDataQuestionOptions;
}

Map<String, dynamic> $QuestionDataByTestIdDataQuestionOptionsToJson(QuestionDataByTestIdDataQuestionOptions entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['question_id'] = entity.questionId;
	data['opt_number'] = entity.optNumber;
	data['option'] = entity.option;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['selected'] = entity.selected;
	return data;
}

QuestionDataByTestIdDataQuestion $QuestionDataByTestIdDataQuestionFromJson(Map<String, dynamic> json) {
	final QuestionDataByTestIdDataQuestion questionDataByTestIdDataQuestion = QuestionDataByTestIdDataQuestion();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		questionDataByTestIdDataQuestion.id = id;
	}
	final String? courseId = jsonConvert.convert<String>(json['course_id']);
	if (courseId != null) {
		questionDataByTestIdDataQuestion.courseId = courseId;
	}
	final String? sectionId = jsonConvert.convert<String>(json['section_id']);
	if (sectionId != null) {
		questionDataByTestIdDataQuestion.sectionId = sectionId;
	}
	final String? partNo = jsonConvert.convert<String>(json['part_no']);
	if (partNo != null) {
		questionDataByTestIdDataQuestion.partNo = partNo;
	}
	final String? questionNo = jsonConvert.convert<String>(json['question_no']);
	if (questionNo != null) {
		questionDataByTestIdDataQuestion.questionNo = questionNo;
	}
	final String? question = jsonConvert.convert<String>(json['question']);
	if (question != null) {
		questionDataByTestIdDataQuestion.question = question;
	}
	final String? answer = jsonConvert.convert<String>(json['answer']);
	if (answer != null) {
		questionDataByTestIdDataQuestion.answer = answer;
	}
	final String? explanation = jsonConvert.convert<String>(json['explanation']);
	if (explanation != null) {
		questionDataByTestIdDataQuestion.explanation = explanation;
	}
	final String? status = jsonConvert.convert<String>(json['status']);
	if (status != null) {
		questionDataByTestIdDataQuestion.status = status;
	}
	final String? createdAt = jsonConvert.convert<String>(json['created_at']);
	if (createdAt != null) {
		questionDataByTestIdDataQuestion.createdAt = createdAt;
	}
	final String? updatedAt = jsonConvert.convert<String>(json['updated_at']);
	if (updatedAt != null) {
		questionDataByTestIdDataQuestion.updatedAt = updatedAt;
	}
	final String? statusWH = jsonConvert.convert<String>(json['status_w_h']);
	if (statusWH != null) {
		questionDataByTestIdDataQuestion.statusWH = statusWH;
	}
	return questionDataByTestIdDataQuestion;
}

Map<String, dynamic> $QuestionDataByTestIdDataQuestionToJson(QuestionDataByTestIdDataQuestion entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['course_id'] = entity.courseId;
	data['section_id'] = entity.sectionId;
	data['part_no'] = entity.partNo;
	data['question_no'] = entity.questionNo;
	data['question'] = entity.question;
	data['answer'] = entity.answer;
	data['explanation'] = entity.explanation;
	data['status'] = entity.status;
	data['created_at'] = entity.createdAt;
	data['updated_at'] = entity.updatedAt;
	data['status_w_h'] = entity.statusWH;
	return data;
}