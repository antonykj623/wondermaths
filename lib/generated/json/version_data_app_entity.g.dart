import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/version_data_app_entity.dart';

VersionDataAppEntity $VersionDataAppEntityFromJson(Map<String, dynamic> json) {
	final VersionDataAppEntity versionDataAppEntity = VersionDataAppEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		versionDataAppEntity.status = status;
	}
	final VersionDataAppData? data = jsonConvert.convert<VersionDataAppData>(json['data']);
	if (data != null) {
		versionDataAppEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		versionDataAppEntity.message = message;
	}
	return versionDataAppEntity;
}

Map<String, dynamic> $VersionDataAppEntityToJson(VersionDataAppEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] = entity.data.toJson();
	data['message'] = entity.message;
	return data;
}

VersionDataAppData $VersionDataAppDataFromJson(Map<String, dynamic> json) {
	final VersionDataAppData versionDataAppData = VersionDataAppData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		versionDataAppData.id = id;
	}
	final String? versionname = jsonConvert.convert<String>(json['versionname']);
	if (versionname != null) {
		versionDataAppData.versionname = versionname;
	}
	return versionDataAppData;
}

Map<String, dynamic> $VersionDataAppDataToJson(VersionDataAppData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['versionname'] = entity.versionname;
	return data;
}