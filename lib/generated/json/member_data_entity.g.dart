import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/member_data_entity.dart';

MemberDataEntity $MemberDataEntityFromJson(Map<String, dynamic> json) {
	final MemberDataEntity memberDataEntity = MemberDataEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		memberDataEntity.status = status;
	}
	final MemberDataMessage? message = jsonConvert.convert<MemberDataMessage>(json['message']);
	if (message != null) {
		memberDataEntity.message = message;
	}
	return memberDataEntity;
}

Map<String, dynamic> $MemberDataEntityToJson(MemberDataEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message.toJson();
	return data;
}

MemberDataMessage $MemberDataMessageFromJson(Map<String, dynamic> json) {
	final MemberDataMessage memberDataMessage = MemberDataMessage();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		memberDataMessage.id = id;
	}
	final String? fullName = jsonConvert.convert<String>(json['full_name']);
	if (fullName != null) {
		memberDataMessage.fullName = fullName;
	}
	final String? regId = jsonConvert.convert<String>(json['reg_id']);
	if (regId != null) {
		memberDataMessage.regId = regId;
	}
	final String? regCode = jsonConvert.convert<String>(json['reg_code']);
	if (regCode != null) {
		memberDataMessage.regCode = regCode;
	}
	final String? countryId = jsonConvert.convert<String>(json['country_id']);
	if (countryId != null) {
		memberDataMessage.countryId = countryId;
	}
	final String? stateId = jsonConvert.convert<String>(json['state_id']);
	if (stateId != null) {
		memberDataMessage.stateId = stateId;
	}
	final String? mobile = jsonConvert.convert<String>(json['mobile']);
	if (mobile != null) {
		memberDataMessage.mobile = mobile;
	}
	final String? emailId = jsonConvert.convert<String>(json['email_id']);
	if (emailId != null) {
		memberDataMessage.emailId = emailId;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		memberDataMessage.currency = currency;
	}
	final String? joinDate = jsonConvert.convert<String>(json['join_date']);
	if (joinDate != null) {
		memberDataMessage.joinDate = joinDate;
	}
	final String? activationDate = jsonConvert.convert<String>(json['activation_date']);
	if (activationDate != null) {
		memberDataMessage.activationDate = activationDate;
	}
	final dynamic? activationKey = jsonConvert.convert<dynamic>(json['activation_key']);
	if (activationKey != null) {
		memberDataMessage.activationKey = activationKey;
	}
	final String? joinSource = jsonConvert.convert<String>(json['join_source']);
	if (joinSource != null) {
		memberDataMessage.joinSource = joinSource;
	}
	final String? usedLinkForRegistration = jsonConvert.convert<String>(json['used_link_for_registration']);
	if (usedLinkForRegistration != null) {
		memberDataMessage.usedLinkForRegistration = usedLinkForRegistration;
	}
	final String? sponserRegId = jsonConvert.convert<String>(json['sponser_reg_id']);
	if (sponserRegId != null) {
		memberDataMessage.sponserRegId = sponserRegId;
	}
	final String? sponserRegCode = jsonConvert.convert<String>(json['sponser_reg_code']);
	if (sponserRegCode != null) {
		memberDataMessage.sponserRegCode = sponserRegCode;
	}
	final String? parentRegId = jsonConvert.convert<String>(json['parent_reg_id']);
	if (parentRegId != null) {
		memberDataMessage.parentRegId = parentRegId;
	}
	final String? parentRegCode = jsonConvert.convert<String>(json['parent_reg_code']);
	if (parentRegCode != null) {
		memberDataMessage.parentRegCode = parentRegCode;
	}
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		memberDataMessage.username = username;
	}
	final String? encrPassword = jsonConvert.convert<String>(json['encr_password']);
	if (encrPassword != null) {
		memberDataMessage.encrPassword = encrPassword;
	}
	final String? defaultLang = jsonConvert.convert<String>(json['default_lang']);
	if (defaultLang != null) {
		memberDataMessage.defaultLang = defaultLang;
	}
	final String? sponseredCount = jsonConvert.convert<String>(json['sponsered_count']);
	if (sponseredCount != null) {
		memberDataMessage.sponseredCount = sponseredCount;
	}
	final String? sponsedCountLeft = jsonConvert.convert<String>(json['sponsed_count_left']);
	if (sponsedCountLeft != null) {
		memberDataMessage.sponsedCountLeft = sponsedCountLeft;
	}
	final String? sponsedCountRight = jsonConvert.convert<String>(json['sponsed_count_right']);
	if (sponsedCountRight != null) {
		memberDataMessage.sponsedCountRight = sponsedCountRight;
	}
	final String? position = jsonConvert.convert<String>(json['position']);
	if (position != null) {
		memberDataMessage.position = position;
	}
	final String? positionDownlineSetupDefault = jsonConvert.convert<String>(json['position_downline_setup_default']);
	if (positionDownlineSetupDefault != null) {
		memberDataMessage.positionDownlineSetupDefault = positionDownlineSetupDefault;
	}
	final String? positionNext = jsonConvert.convert<String>(json['position_next']);
	if (positionNext != null) {
		memberDataMessage.positionNext = positionNext;
	}
	final String? bankAccountNo = jsonConvert.convert<String>(json['bank_account_no']);
	if (bankAccountNo != null) {
		memberDataMessage.bankAccountNo = bankAccountNo;
	}
	final String? bankAccountName = jsonConvert.convert<String>(json['bank_account_name']);
	if (bankAccountName != null) {
		memberDataMessage.bankAccountName = bankAccountName;
	}
	final String? bankName = jsonConvert.convert<String>(json['bank_name']);
	if (bankName != null) {
		memberDataMessage.bankName = bankName;
	}
	final String? branchName = jsonConvert.convert<String>(json['branch_name']);
	if (branchName != null) {
		memberDataMessage.branchName = branchName;
	}
	final dynamic? panCard = jsonConvert.convert<dynamic>(json['pan_card']);
	if (panCard != null) {
		memberDataMessage.panCard = panCard;
	}
	final String? binaryLeft = jsonConvert.convert<String>(json['binary_left']);
	if (binaryLeft != null) {
		memberDataMessage.binaryLeft = binaryLeft;
	}
	final String? binaryRight = jsonConvert.convert<String>(json['binary_right']);
	if (binaryRight != null) {
		memberDataMessage.binaryRight = binaryRight;
	}
	final String? binaryMatched = jsonConvert.convert<String>(json['binary_matched']);
	if (binaryMatched != null) {
		memberDataMessage.binaryMatched = binaryMatched;
	}
	final String? carryLeft = jsonConvert.convert<String>(json['carry_left']);
	if (carryLeft != null) {
		memberDataMessage.carryLeft = carryLeft;
	}
	final String? carryRight = jsonConvert.convert<String>(json['carry_right']);
	if (carryRight != null) {
		memberDataMessage.carryRight = carryRight;
	}
	final String? binaryAmt = jsonConvert.convert<String>(json['binary_amt']);
	if (binaryAmt != null) {
		memberDataMessage.binaryAmt = binaryAmt;
	}
	final String? binaryAmtPre = jsonConvert.convert<String>(json['binary_amt_pre']);
	if (binaryAmtPre != null) {
		memberDataMessage.binaryAmtPre = binaryAmtPre;
	}
	final String? binaryLastgnDate = jsonConvert.convert<String>(json['binary_lastgn_date']);
	if (binaryLastgnDate != null) {
		memberDataMessage.binaryLastgnDate = binaryLastgnDate;
	}
	final String? referralCommissionAmt = jsonConvert.convert<String>(json['referral_commission_amt']);
	if (referralCommissionAmt != null) {
		memberDataMessage.referralCommissionAmt = referralCommissionAmt;
	}
	final String? referralCommissionAmtPre = jsonConvert.convert<String>(json['referral_commission_amt_pre']);
	if (referralCommissionAmtPre != null) {
		memberDataMessage.referralCommissionAmtPre = referralCommissionAmtPre;
	}
	final String? referralCommissionLastgnDate = jsonConvert.convert<String>(json['referral_commission_lastgn_date']);
	if (referralCommissionLastgnDate != null) {
		memberDataMessage.referralCommissionLastgnDate = referralCommissionLastgnDate;
	}
	final String? levelAmt = jsonConvert.convert<String>(json['level_amt']);
	if (levelAmt != null) {
		memberDataMessage.levelAmt = levelAmt;
	}
	final String? levelAmtPre = jsonConvert.convert<String>(json['level_amt_pre']);
	if (levelAmtPre != null) {
		memberDataMessage.levelAmtPre = levelAmtPre;
	}
	final String? levelLastgnDate = jsonConvert.convert<String>(json['level_lastgn_date']);
	if (levelLastgnDate != null) {
		memberDataMessage.levelLastgnDate = levelLastgnDate;
	}
	final String? lastAchievedRank = jsonConvert.convert<String>(json['last_achieved_rank']);
	if (lastAchievedRank != null) {
		memberDataMessage.lastAchievedRank = lastAchievedRank;
	}
	final String? achievementAmt = jsonConvert.convert<String>(json['achievement_amt']);
	if (achievementAmt != null) {
		memberDataMessage.achievementAmt = achievementAmt;
	}
	final String? achievementAmtPre = jsonConvert.convert<String>(json['achievement_amt_pre']);
	if (achievementAmtPre != null) {
		memberDataMessage.achievementAmtPre = achievementAmtPre;
	}
	final dynamic? achievementLastgnDate = jsonConvert.convert<dynamic>(json['achievement_lastgn_date']);
	if (achievementLastgnDate != null) {
		memberDataMessage.achievementLastgnDate = achievementLastgnDate;
	}
	final String? memberCancel = jsonConvert.convert<String>(json['member_cancel']);
	if (memberCancel != null) {
		memberDataMessage.memberCancel = memberCancel;
	}
	final String? memberTerminated = jsonConvert.convert<String>(json['member_terminated']);
	if (memberTerminated != null) {
		memberDataMessage.memberTerminated = memberTerminated;
	}
	final String? profilePhoto = jsonConvert.convert<String>(json['profile_photo']);
	if (profilePhoto != null) {
		memberDataMessage.profilePhoto = profilePhoto;
	}
	final String? panNo = jsonConvert.convert<String>(json['pan_no']);
	if (panNo != null) {
		memberDataMessage.panNo = panNo;
	}
	final String? panPhoto = jsonConvert.convert<String>(json['pan_photo']);
	if (panPhoto != null) {
		memberDataMessage.panPhoto = panPhoto;
	}
	final String? adharNo = jsonConvert.convert<String>(json['adhar_no']);
	if (adharNo != null) {
		memberDataMessage.adharNo = adharNo;
	}
	final String? adharPhoto = jsonConvert.convert<String>(json['adhar_photo']);
	if (adharPhoto != null) {
		memberDataMessage.adharPhoto = adharPhoto;
	}
	final dynamic? idNo = jsonConvert.convert<dynamic>(json['id_no']);
	if (idNo != null) {
		memberDataMessage.idNo = idNo;
	}
	final dynamic? idproofPhoto = jsonConvert.convert<dynamic>(json['idproof_photo']);
	if (idproofPhoto != null) {
		memberDataMessage.idproofPhoto = idproofPhoto;
	}
	final dynamic? bankCheckleafPhoto = jsonConvert.convert<dynamic>(json['bank_checkleaf_photo']);
	if (bankCheckleafPhoto != null) {
		memberDataMessage.bankCheckleafPhoto = bankCheckleafPhoto;
	}
	final dynamic? branch = jsonConvert.convert<dynamic>(json['branch']);
	if (branch != null) {
		memberDataMessage.branch = branch;
	}
	final String? ifsc = jsonConvert.convert<String>(json['ifsc']);
	if (ifsc != null) {
		memberDataMessage.ifsc = ifsc;
	}
	final String? panVarified = jsonConvert.convert<String>(json['pan_varified']);
	if (panVarified != null) {
		memberDataMessage.panVarified = panVarified;
	}
	final dynamic? panVarifyDate = jsonConvert.convert<dynamic>(json['pan_varify_date']);
	if (panVarifyDate != null) {
		memberDataMessage.panVarifyDate = panVarifyDate;
	}
	final String? adharVarified = jsonConvert.convert<String>(json['adhar_varified']);
	if (adharVarified != null) {
		memberDataMessage.adharVarified = adharVarified;
	}
	final dynamic? adharVarifyDate = jsonConvert.convert<dynamic>(json['adhar_varify_date']);
	if (adharVarifyDate != null) {
		memberDataMessage.adharVarifyDate = adharVarifyDate;
	}
	final String? idproofVarified = jsonConvert.convert<String>(json['idproof_varified']);
	if (idproofVarified != null) {
		memberDataMessage.idproofVarified = idproofVarified;
	}
	final dynamic? idproofVarifyDate = jsonConvert.convert<dynamic>(json['idproof_varify_date']);
	if (idproofVarifyDate != null) {
		memberDataMessage.idproofVarifyDate = idproofVarifyDate;
	}
	final String? bankVarified = jsonConvert.convert<String>(json['bank_varified']);
	if (bankVarified != null) {
		memberDataMessage.bankVarified = bankVarified;
	}
	final dynamic? bankVarifyDate = jsonConvert.convert<dynamic>(json['bank_varify_date']);
	if (bankVarifyDate != null) {
		memberDataMessage.bankVarifyDate = bankVarifyDate;
	}
	final String? updatestatus = jsonConvert.convert<String>(json['updatestatus']);
	if (updatestatus != null) {
		memberDataMessage.updatestatus = updatestatus;
	}
	final String? remarkVarification = jsonConvert.convert<String>(json['remark_varification']);
	if (remarkVarification != null) {
		memberDataMessage.remarkVarification = remarkVarification;
	}
	final dynamic? lastVarifiedBy = jsonConvert.convert<dynamic>(json['last_varified_by']);
	if (lastVarifiedBy != null) {
		memberDataMessage.lastVarifiedBy = lastVarifiedBy;
	}
	final dynamic? lastVarifiedDate = jsonConvert.convert<dynamic>(json['last_varified_date']);
	if (lastVarifiedDate != null) {
		memberDataMessage.lastVarifiedDate = lastVarifiedDate;
	}
	final String? memberStatus = jsonConvert.convert<String>(json['member_status']);
	if (memberStatus != null) {
		memberDataMessage.memberStatus = memberStatus;
	}
	final String? memberEntryDate = jsonConvert.convert<String>(json['member_entry_date']);
	if (memberEntryDate != null) {
		memberDataMessage.memberEntryDate = memberEntryDate;
	}
	final String? freeMemberCount = jsonConvert.convert<String>(json['free_member_count']);
	if (freeMemberCount != null) {
		memberDataMessage.freeMemberCount = freeMemberCount;
	}
	final dynamic? groupEntryDate = jsonConvert.convert<dynamic>(json['group_entry_date']);
	if (groupEntryDate != null) {
		memberDataMessage.groupEntryDate = groupEntryDate;
	}
	final String? groupCount = jsonConvert.convert<String>(json['group_count']);
	if (groupCount != null) {
		memberDataMessage.groupCount = groupCount;
	}
	final dynamic? groupadminRegId = jsonConvert.convert<dynamic>(json['groupadmin_reg_id']);
	if (groupadminRegId != null) {
		memberDataMessage.groupadminRegId = groupadminRegId;
	}
	final String? groupAdminStatus = jsonConvert.convert<String>(json['group_admin_status']);
	if (groupAdminStatus != null) {
		memberDataMessage.groupAdminStatus = groupAdminStatus;
	}
	final dynamic? relationship = jsonConvert.convert<dynamic>(json['relationship']);
	if (relationship != null) {
		memberDataMessage.relationship = relationship;
	}
	final dynamic? relatedAppuserName = jsonConvert.convert<dynamic>(json['related_appuser_name']);
	if (relatedAppuserName != null) {
		memberDataMessage.relatedAppuserName = relatedAppuserName;
	}
	final dynamic? cancellationDate = jsonConvert.convert<dynamic>(json['cancellation_date']);
	if (cancellationDate != null) {
		memberDataMessage.cancellationDate = cancellationDate;
	}
	final dynamic? cancellationReason = jsonConvert.convert<dynamic>(json['cancellation_reason']);
	if (cancellationReason != null) {
		memberDataMessage.cancellationReason = cancellationReason;
	}
	final String? nomineeName = jsonConvert.convert<String>(json['nominee_name']);
	if (nomineeName != null) {
		memberDataMessage.nomineeName = nomineeName;
	}
	final String? nomineeRelation = jsonConvert.convert<String>(json['nominee_relation']);
	if (nomineeRelation != null) {
		memberDataMessage.nomineeRelation = nomineeRelation;
	}
	final String? nomineeIdNo = jsonConvert.convert<String>(json['nominee_id_no']);
	if (nomineeIdNo != null) {
		memberDataMessage.nomineeIdNo = nomineeIdNo;
	}
	final dynamic? nomineeIdPath = jsonConvert.convert<dynamic>(json['nominee_id_path']);
	if (nomineeIdPath != null) {
		memberDataMessage.nomineeIdPath = nomineeIdPath;
	}
	final String? autoPoolMember = jsonConvert.convert<String>(json['auto_pool_member']);
	if (autoPoolMember != null) {
		memberDataMessage.autoPoolMember = autoPoolMember;
	}
	final dynamic? autoPoolEntryDate = jsonConvert.convert<dynamic>(json['auto_pool_entry_date']);
	if (autoPoolEntryDate != null) {
		memberDataMessage.autoPoolEntryDate = autoPoolEntryDate;
	}
	final String? autopoolDeposit = jsonConvert.convert<String>(json['autopool_deposit']);
	if (autopoolDeposit != null) {
		memberDataMessage.autopoolDeposit = autopoolDeposit;
	}
	final String? autopoolCollected = jsonConvert.convert<String>(json['autopool_collected']);
	if (autopoolCollected != null) {
		memberDataMessage.autopoolCollected = autopoolCollected;
	}
	final dynamic? autopoolAgreed = jsonConvert.convert<dynamic>(json['autopool_agreed']);
	if (autopoolAgreed != null) {
		memberDataMessage.autopoolAgreed = autopoolAgreed;
	}
	final String? autopoolGroup = jsonConvert.convert<String>(json['autopool_group']);
	if (autopoolGroup != null) {
		memberDataMessage.autopoolGroup = autopoolGroup;
	}
	final String? blockLevel = jsonConvert.convert<String>(json['block_level']);
	if (blockLevel != null) {
		memberDataMessage.blockLevel = blockLevel;
	}
	final String? actvationProduct = jsonConvert.convert<String>(json['actvation_product']);
	if (actvationProduct != null) {
		memberDataMessage.actvationProduct = actvationProduct;
	}
	final dynamic? mathsShiftDate = jsonConvert.convert<dynamic>(json['maths_shift_date']);
	if (mathsShiftDate != null) {
		memberDataMessage.mathsShiftDate = mathsShiftDate;
	}
	final dynamic? wondermathsId = jsonConvert.convert<dynamic>(json['wondermaths_id']);
	if (wondermathsId != null) {
		memberDataMessage.wondermathsId = wondermathsId;
	}
	return memberDataMessage;
}

Map<String, dynamic> $MemberDataMessageToJson(MemberDataMessage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['full_name'] = entity.fullName;
	data['reg_id'] = entity.regId;
	data['reg_code'] = entity.regCode;
	data['country_id'] = entity.countryId;
	data['state_id'] = entity.stateId;
	data['mobile'] = entity.mobile;
	data['email_id'] = entity.emailId;
	data['currency'] = entity.currency;
	data['join_date'] = entity.joinDate;
	data['activation_date'] = entity.activationDate;
	data['activation_key'] = entity.activationKey;
	data['join_source'] = entity.joinSource;
	data['used_link_for_registration'] = entity.usedLinkForRegistration;
	data['sponser_reg_id'] = entity.sponserRegId;
	data['sponser_reg_code'] = entity.sponserRegCode;
	data['parent_reg_id'] = entity.parentRegId;
	data['parent_reg_code'] = entity.parentRegCode;
	data['username'] = entity.username;
	data['encr_password'] = entity.encrPassword;
	data['default_lang'] = entity.defaultLang;
	data['sponsered_count'] = entity.sponseredCount;
	data['sponsed_count_left'] = entity.sponsedCountLeft;
	data['sponsed_count_right'] = entity.sponsedCountRight;
	data['position'] = entity.position;
	data['position_downline_setup_default'] = entity.positionDownlineSetupDefault;
	data['position_next'] = entity.positionNext;
	data['bank_account_no'] = entity.bankAccountNo;
	data['bank_account_name'] = entity.bankAccountName;
	data['bank_name'] = entity.bankName;
	data['branch_name'] = entity.branchName;
	data['pan_card'] = entity.panCard;
	data['binary_left'] = entity.binaryLeft;
	data['binary_right'] = entity.binaryRight;
	data['binary_matched'] = entity.binaryMatched;
	data['carry_left'] = entity.carryLeft;
	data['carry_right'] = entity.carryRight;
	data['binary_amt'] = entity.binaryAmt;
	data['binary_amt_pre'] = entity.binaryAmtPre;
	data['binary_lastgn_date'] = entity.binaryLastgnDate;
	data['referral_commission_amt'] = entity.referralCommissionAmt;
	data['referral_commission_amt_pre'] = entity.referralCommissionAmtPre;
	data['referral_commission_lastgn_date'] = entity.referralCommissionLastgnDate;
	data['level_amt'] = entity.levelAmt;
	data['level_amt_pre'] = entity.levelAmtPre;
	data['level_lastgn_date'] = entity.levelLastgnDate;
	data['last_achieved_rank'] = entity.lastAchievedRank;
	data['achievement_amt'] = entity.achievementAmt;
	data['achievement_amt_pre'] = entity.achievementAmtPre;
	data['achievement_lastgn_date'] = entity.achievementLastgnDate;
	data['member_cancel'] = entity.memberCancel;
	data['member_terminated'] = entity.memberTerminated;
	data['profile_photo'] = entity.profilePhoto;
	data['pan_no'] = entity.panNo;
	data['pan_photo'] = entity.panPhoto;
	data['adhar_no'] = entity.adharNo;
	data['adhar_photo'] = entity.adharPhoto;
	data['id_no'] = entity.idNo;
	data['idproof_photo'] = entity.idproofPhoto;
	data['bank_checkleaf_photo'] = entity.bankCheckleafPhoto;
	data['branch'] = entity.branch;
	data['ifsc'] = entity.ifsc;
	data['pan_varified'] = entity.panVarified;
	data['pan_varify_date'] = entity.panVarifyDate;
	data['adhar_varified'] = entity.adharVarified;
	data['adhar_varify_date'] = entity.adharVarifyDate;
	data['idproof_varified'] = entity.idproofVarified;
	data['idproof_varify_date'] = entity.idproofVarifyDate;
	data['bank_varified'] = entity.bankVarified;
	data['bank_varify_date'] = entity.bankVarifyDate;
	data['updatestatus'] = entity.updatestatus;
	data['remark_varification'] = entity.remarkVarification;
	data['last_varified_by'] = entity.lastVarifiedBy;
	data['last_varified_date'] = entity.lastVarifiedDate;
	data['member_status'] = entity.memberStatus;
	data['member_entry_date'] = entity.memberEntryDate;
	data['free_member_count'] = entity.freeMemberCount;
	data['group_entry_date'] = entity.groupEntryDate;
	data['group_count'] = entity.groupCount;
	data['groupadmin_reg_id'] = entity.groupadminRegId;
	data['group_admin_status'] = entity.groupAdminStatus;
	data['relationship'] = entity.relationship;
	data['related_appuser_name'] = entity.relatedAppuserName;
	data['cancellation_date'] = entity.cancellationDate;
	data['cancellation_reason'] = entity.cancellationReason;
	data['nominee_name'] = entity.nomineeName;
	data['nominee_relation'] = entity.nomineeRelation;
	data['nominee_id_no'] = entity.nomineeIdNo;
	data['nominee_id_path'] = entity.nomineeIdPath;
	data['auto_pool_member'] = entity.autoPoolMember;
	data['auto_pool_entry_date'] = entity.autoPoolEntryDate;
	data['autopool_deposit'] = entity.autopoolDeposit;
	data['autopool_collected'] = entity.autopoolCollected;
	data['autopool_agreed'] = entity.autopoolAgreed;
	data['autopool_group'] = entity.autopoolGroup;
	data['block_level'] = entity.blockLevel;
	data['actvation_product'] = entity.actvationProduct;
	data['maths_shift_date'] = entity.mathsShiftDate;
	data['wondermaths_id'] = entity.wondermathsId;
	return data;
}