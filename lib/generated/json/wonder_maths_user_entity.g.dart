import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/wonder_maths_user_entity.dart';

WonderMathsUserEntity $WonderMathsUserEntityFromJson(Map<String, dynamic> json) {
	final WonderMathsUserEntity wonderMathsUserEntity = WonderMathsUserEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		wonderMathsUserEntity.status = status;
	}
	final WonderMathsUserData? data = jsonConvert.convert<WonderMathsUserData>(json['data']);
	if (data != null) {
		wonderMathsUserEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		wonderMathsUserEntity.message = message;
	}
	return wonderMathsUserEntity;
}

Map<String, dynamic> $WonderMathsUserEntityToJson(WonderMathsUserEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] = entity.data.toJson();
	data['message'] = entity.message;
	return data;
}

WonderMathsUserData $WonderMathsUserDataFromJson(Map<String, dynamic> json) {
	final WonderMathsUserData wonderMathsUserData = WonderMathsUserData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		wonderMathsUserData.id = id;
	}
	final String? fullName = jsonConvert.convert<String>(json['full_name']);
	if (fullName != null) {
		wonderMathsUserData.fullName = fullName;
	}
	final String? regCode = jsonConvert.convert<String>(json['reg_code']);
	if (regCode != null) {
		wonderMathsUserData.regCode = regCode;
	}
	final String? countryId = jsonConvert.convert<String>(json['country_id']);
	if (countryId != null) {
		wonderMathsUserData.countryId = countryId;
	}
	final String? stateId = jsonConvert.convert<String>(json['state_id']);
	if (stateId != null) {
		wonderMathsUserData.stateId = stateId;
	}
	final String? mobile = jsonConvert.convert<String>(json['mobile']);
	if (mobile != null) {
		wonderMathsUserData.mobile = mobile;
	}
	final String? profileImage = jsonConvert.convert<String>(json['profile_image']);
	if (profileImage != null) {
		wonderMathsUserData.profileImage = profileImage;
	}
	final String? emailId = jsonConvert.convert<String>(json['email_id']);
	if (emailId != null) {
		wonderMathsUserData.emailId = emailId;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		wonderMathsUserData.currency = currency;
	}
	final String? joinDate = jsonConvert.convert<String>(json['join_date']);
	if (joinDate != null) {
		wonderMathsUserData.joinDate = joinDate;
	}
	final String? activationDate = jsonConvert.convert<String>(json['activation_date']);
	if (activationDate != null) {
		wonderMathsUserData.activationDate = activationDate;
	}
	final dynamic? activationKey = jsonConvert.convert<dynamic>(json['activation_key']);
	if (activationKey != null) {
		wonderMathsUserData.activationKey = activationKey;
	}
	final String? joinSource = jsonConvert.convert<String>(json['join_source']);
	if (joinSource != null) {
		wonderMathsUserData.joinSource = joinSource;
	}
	final String? usedLinkForRegistration = jsonConvert.convert<String>(json['used_link_for_registration']);
	if (usedLinkForRegistration != null) {
		wonderMathsUserData.usedLinkForRegistration = usedLinkForRegistration;
	}
	final String? spRegId = jsonConvert.convert<String>(json['sp_reg_id']);
	if (spRegId != null) {
		wonderMathsUserData.spRegId = spRegId;
	}
	final String? deviceId = jsonConvert.convert<String>(json['device_id']);
	if (deviceId != null) {
		wonderMathsUserData.deviceId = deviceId;
	}
	final String? wDeviceId = jsonConvert.convert<String>(json['w_device_id']);
	if (wDeviceId != null) {
		wonderMathsUserData.wDeviceId = wDeviceId;
	}
	final String? wPlatform = jsonConvert.convert<String>(json['w_platform']);
	if (wPlatform != null) {
		wonderMathsUserData.wPlatform = wPlatform;
	}
	final String? spRegCode = jsonConvert.convert<String>(json['sp_reg_code']);
	if (spRegCode != null) {
		wonderMathsUserData.spRegCode = spRegCode;
	}
	final String? defaultLang = jsonConvert.convert<String>(json['default_lang']);
	if (defaultLang != null) {
		wonderMathsUserData.defaultLang = defaultLang;
	}
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		wonderMathsUserData.username = username;
	}
	final String? encrPassword = jsonConvert.convert<String>(json['encr_password']);
	if (encrPassword != null) {
		wonderMathsUserData.encrPassword = encrPassword;
	}
	final String? gdriveFileid = jsonConvert.convert<String>(json['gdrive_fileid']);
	if (gdriveFileid != null) {
		wonderMathsUserData.gdriveFileid = gdriveFileid;
	}
	final String? uniqueDeviceid = jsonConvert.convert<String>(json['unique_deviceId']);
	if (uniqueDeviceid != null) {
		wonderMathsUserData.uniqueDeviceid = uniqueDeviceid;
	}
	final String? memberStatus = jsonConvert.convert<String>(json['member_status']);
	if (memberStatus != null) {
		wonderMathsUserData.memberStatus = memberStatus;
	}
	final String? resellingPartner = jsonConvert.convert<String>(json['reselling_partner']);
	if (resellingPartner != null) {
		wonderMathsUserData.resellingPartner = resellingPartner;
	}
	final String? coupon = jsonConvert.convert<String>(json['coupon']);
	if (coupon != null) {
		wonderMathsUserData.coupon = coupon;
	}
	final String? coupStus = jsonConvert.convert<String>(json['coup_stus']);
	if (coupStus != null) {
		wonderMathsUserData.coupStus = coupStus;
	}
	final String? currentAppVersion = jsonConvert.convert<String>(json['current_app_version']);
	if (currentAppVersion != null) {
		wonderMathsUserData.currentAppVersion = currentAppVersion;
	}
	final String? phoneType = jsonConvert.convert<String>(json['phone_type']);
	if (phoneType != null) {
		wonderMathsUserData.phoneType = phoneType;
	}
	final String? driveMailid = jsonConvert.convert<String>(json['drive_mailId']);
	if (driveMailid != null) {
		wonderMathsUserData.driveMailid = driveMailid;
	}
	final String? serverbackupFileid = jsonConvert.convert<String>(json['serverbackup_fileid']);
	if (serverbackupFileid != null) {
		wonderMathsUserData.serverbackupFileid = serverbackupFileid;
	}
	final String? mathsTrialNumber = jsonConvert.convert<String>(json['maths_trial_number']);
	if (mathsTrialNumber != null) {
		wonderMathsUserData.mathsTrialNumber = mathsTrialNumber;
	}
	final String? mathsTrialStatus = jsonConvert.convert<String>(json['maths_trial_status']);
	if (mathsTrialStatus != null) {
		wonderMathsUserData.mathsTrialStatus = mathsTrialStatus;
	}
	final String? linkActive = jsonConvert.convert<String>(json['link_active']);
	if (linkActive != null) {
		wonderMathsUserData.linkActive = linkActive;
	}
	final String? wTotalPts = jsonConvert.convert<String>(json['w_total_pts']);
	if (wTotalPts != null) {
		wonderMathsUserData.wTotalPts = wTotalPts;
	}
	final String? wRedeemedPts = jsonConvert.convert<String>(json['w_redeemed_pts']);
	if (wRedeemedPts != null) {
		wonderMathsUserData.wRedeemedPts = wRedeemedPts;
	}
	final String? wBalancePts = jsonConvert.convert<String>(json['w_balance_pts']);
	if (wBalancePts != null) {
		wonderMathsUserData.wBalancePts = wBalancePts;
	}
	return wonderMathsUserData;
}

Map<String, dynamic> $WonderMathsUserDataToJson(WonderMathsUserData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['full_name'] = entity.fullName;
	data['reg_code'] = entity.regCode;
	data['country_id'] = entity.countryId;
	data['state_id'] = entity.stateId;
	data['mobile'] = entity.mobile;
	data['profile_image'] = entity.profileImage;
	data['email_id'] = entity.emailId;
	data['currency'] = entity.currency;
	data['join_date'] = entity.joinDate;
	data['activation_date'] = entity.activationDate;
	data['activation_key'] = entity.activationKey;
	data['join_source'] = entity.joinSource;
	data['used_link_for_registration'] = entity.usedLinkForRegistration;
	data['sp_reg_id'] = entity.spRegId;
	data['device_id'] = entity.deviceId;
	data['w_device_id'] = entity.wDeviceId;
	data['w_platform'] = entity.wPlatform;
	data['sp_reg_code'] = entity.spRegCode;
	data['default_lang'] = entity.defaultLang;
	data['username'] = entity.username;
	data['encr_password'] = entity.encrPassword;
	data['gdrive_fileid'] = entity.gdriveFileid;
	data['unique_deviceId'] = entity.uniqueDeviceid;
	data['member_status'] = entity.memberStatus;
	data['reselling_partner'] = entity.resellingPartner;
	data['coupon'] = entity.coupon;
	data['coup_stus'] = entity.coupStus;
	data['current_app_version'] = entity.currentAppVersion;
	data['phone_type'] = entity.phoneType;
	data['drive_mailId'] = entity.driveMailid;
	data['serverbackup_fileid'] = entity.serverbackupFileid;
	data['maths_trial_number'] = entity.mathsTrialNumber;
	data['maths_trial_status'] = entity.mathsTrialStatus;
	data['link_active'] = entity.linkActive;
	data['w_total_pts'] = entity.wTotalPts;
	data['w_redeemed_pts'] = entity.wRedeemedPts;
	data['w_balance_pts'] = entity.wBalancePts;
	return data;
}