import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/aptitude_test_entity.dart';

AptitudeTestEntity $AptitudeTestEntityFromJson(Map<String, dynamic> json) {
	final AptitudeTestEntity aptitudeTestEntity = AptitudeTestEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		aptitudeTestEntity.status = status;
	}
	final List<AptitudeTestData>? data = jsonConvert.convertListNotNull<AptitudeTestData>(json['data']);
	if (data != null) {
		aptitudeTestEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		aptitudeTestEntity.message = message;
	}
	return aptitudeTestEntity;
}

Map<String, dynamic> $AptitudeTestEntityToJson(AptitudeTestEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

AptitudeTestData $AptitudeTestDataFromJson(Map<String, dynamic> json) {
	final AptitudeTestData aptitudeTestData = AptitudeTestData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		aptitudeTestData.id = id;
	}
	final String? param1 = jsonConvert.convert<String>(json['param1']);
	if (param1 != null) {
		aptitudeTestData.param1 = param1;
	}
	final dynamic? param2 = jsonConvert.convert<dynamic>(json['param2']);
	if (param2 != null) {
		aptitudeTestData.param2 = param2;
	}
	final String? testName = jsonConvert.convert<String>(json['test_name']);
	if (testName != null) {
		aptitudeTestData.testName = testName;
	}
	final String? createdTs = jsonConvert.convert<String>(json['created_ts']);
	if (createdTs != null) {
		aptitudeTestData.createdTs = createdTs;
	}
	final String? updatedTs = jsonConvert.convert<String>(json['updated_ts']);
	if (updatedTs != null) {
		aptitudeTestData.updatedTs = updatedTs;
	}
	return aptitudeTestData;
}

Map<String, dynamic> $AptitudeTestDataToJson(AptitudeTestData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['param1'] = entity.param1;
	data['param2'] = entity.param2;
	data['test_name'] = entity.testName;
	data['created_ts'] = entity.createdTs;
	data['updated_ts'] = entity.updatedTs;
	return data;
}