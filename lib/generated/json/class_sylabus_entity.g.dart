import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/class_sylabus_entity.dart';

ClassSylabusEntity $ClassSylabusEntityFromJson(Map<String, dynamic> json) {
	final ClassSylabusEntity classSylabusEntity = ClassSylabusEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		classSylabusEntity.status = status;
	}
	final List<ClassSylabusData>? data = jsonConvert.convertListNotNull<ClassSylabusData>(json['data']);
	if (data != null) {
		classSylabusEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		classSylabusEntity.message = message;
	}
	return classSylabusEntity;
}

Map<String, dynamic> $ClassSylabusEntityToJson(ClassSylabusEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

ClassSylabusData $ClassSylabusDataFromJson(Map<String, dynamic> json) {
	final ClassSylabusData classSylabusData = ClassSylabusData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		classSylabusData.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		classSylabusData.name = name;
	}
	final List<ClassSylabusDataClassesdata>? classesdata = jsonConvert.convertListNotNull<ClassSylabusDataClassesdata>(json['classesdata']);
	if (classesdata != null) {
		classSylabusData.classesdata = classesdata;
	}
	return classSylabusData;
}

Map<String, dynamic> $ClassSylabusDataToJson(ClassSylabusData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['classesdata'] =  entity.classesdata.map((v) => v.toJson()).toList();
	return data;
}

ClassSylabusDataClassesdata $ClassSylabusDataClassesdataFromJson(Map<String, dynamic> json) {
	final ClassSylabusDataClassesdata classSylabusDataClassesdata = ClassSylabusDataClassesdata();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		classSylabusDataClassesdata.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		classSylabusDataClassesdata.name = name;
	}
	final String? syllabusId = jsonConvert.convert<String>(json['syllabus_id']);
	if (syllabusId != null) {
		classSylabusDataClassesdata.syllabusId = syllabusId;
	}
	final String? status = jsonConvert.convert<String>(json['status']);
	if (status != null) {
		classSylabusDataClassesdata.status = status;
	}
	final int? selected = jsonConvert.convert<int>(json['selected']);
	if (selected != null) {
		classSylabusDataClassesdata.selected = selected;
	}
	return classSylabusDataClassesdata;
}

Map<String, dynamic> $ClassSylabusDataClassesdataToJson(ClassSylabusDataClassesdata entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['syllabus_id'] = entity.syllabusId;
	data['status'] = entity.status;
	data['selected'] = entity.selected;
	return data;
}