import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/video_by_class_entity.dart';

VideoByClassEntity $VideoByClassEntityFromJson(Map<String, dynamic> json) {
	final VideoByClassEntity videoByClassEntity = VideoByClassEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		videoByClassEntity.status = status;
	}
	final List<VideoByClassData>? data = jsonConvert.convertListNotNull<VideoByClassData>(json['data']);
	if (data != null) {
		videoByClassEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		videoByClassEntity.message = message;
	}
	return videoByClassEntity;
}

Map<String, dynamic> $VideoByClassEntityToJson(VideoByClassEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

VideoByClassData $VideoByClassDataFromJson(Map<String, dynamic> json) {
	final VideoByClassData videoByClassData = VideoByClassData();
	final String? id = jsonConvert.convert<String>(json['Id']);
	if (id != null) {
		videoByClassData.id = id;
	}
	final String? subject = jsonConvert.convert<String>(json['Subject']);
	if (subject != null) {
		videoByClassData.subject = subject;
	}
	final String? topic = jsonConvert.convert<String>(json['Topic']);
	if (topic != null) {
		videoByClassData.topic = topic;
	}
	final String? link = jsonConvert.convert<String>(json['Link']);
	if (link != null) {
		videoByClassData.link = link;
	}
	final String? xClass = jsonConvert.convert<String>(json['Class']);
	if (xClass != null) {
		videoByClassData.xClass = xClass;
	}
	return videoByClassData;
}

Map<String, dynamic> $VideoByClassDataToJson(VideoByClassData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['Id'] = entity.id;
	data['Subject'] = entity.subject;
	data['Topic'] = entity.topic;
	data['Link'] = entity.link;
	data['Class'] = entity.xClass;
	return data;
}