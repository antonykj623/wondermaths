import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/pdf_topic_group_entity.dart';

PdfTopicGroupEntity $PdfTopicGroupEntityFromJson(Map<String, dynamic> json) {
	final PdfTopicGroupEntity pdfTopicGroupEntity = PdfTopicGroupEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		pdfTopicGroupEntity.status = status;
	}
	final List<PdfTopicGroupData>? data = jsonConvert.convertListNotNull<PdfTopicGroupData>(json['data']);
	if (data != null) {
		pdfTopicGroupEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		pdfTopicGroupEntity.message = message;
	}
	return pdfTopicGroupEntity;
}

Map<String, dynamic> $PdfTopicGroupEntityToJson(PdfTopicGroupEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

PdfTopicGroupData $PdfTopicGroupDataFromJson(Map<String, dynamic> json) {
	final PdfTopicGroupData pdfTopicGroupData = PdfTopicGroupData();
	final String? id = jsonConvert.convert<String>(json['Id']);
	if (id != null) {
		pdfTopicGroupData.id = id;
	}
	final String? topic = jsonConvert.convert<String>(json['Topic']);
	if (topic != null) {
		pdfTopicGroupData.topic = topic;
	}
	final String? srNo = jsonConvert.convert<String>(json['SrNo']);
	if (srNo != null) {
		pdfTopicGroupData.srNo = srNo;
	}
	final String? priority = jsonConvert.convert<String>(json['priority']);
	if (priority != null) {
		pdfTopicGroupData.priority = priority;
	}
	final String? subject = jsonConvert.convert<String>(json['Subject']);
	if (subject != null) {
		pdfTopicGroupData.subject = subject;
	}
	final String? chapter = jsonConvert.convert<String>(json['Chapter']);
	if (chapter != null) {
		pdfTopicGroupData.chapter = chapter;
	}
	final String? fileName = jsonConvert.convert<String>(json['FileName']);
	if (fileName != null) {
		pdfTopicGroupData.fileName = fileName;
	}
	final String? cBSC = jsonConvert.convert<String>(json['CBSC']);
	if (cBSC != null) {
		pdfTopicGroupData.cBSC = cBSC;
	}
	final String? state = jsonConvert.convert<String>(json['State']);
	if (state != null) {
		pdfTopicGroupData.state = state;
	}
	final String? topicId = jsonConvert.convert<String>(json['topicId']);
	if (topicId != null) {
		pdfTopicGroupData.topicId = topicId;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		pdfTopicGroupData.description = description;
	}
	return pdfTopicGroupData;
}

Map<String, dynamic> $PdfTopicGroupDataToJson(PdfTopicGroupData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['Id'] = entity.id;
	data['Topic'] = entity.topic;
	data['SrNo'] = entity.srNo;
	data['priority'] = entity.priority;
	data['Subject'] = entity.subject;
	data['Chapter'] = entity.chapter;
	data['FileName'] = entity.fileName;
	data['CBSC'] = entity.cBSC;
	data['State'] = entity.state;
	data['topicId'] = entity.topicId;
	data['description'] = entity.description;
	return data;
}