import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/speed_maths_data_entity.dart';

SpeedMathsDataEntity $SpeedMathsDataEntityFromJson(Map<String, dynamic> json) {
	final SpeedMathsDataEntity speedMathsDataEntity = SpeedMathsDataEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		speedMathsDataEntity.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		speedMathsDataEntity.message = message;
	}
	final List<SpeedMathsDataData>? data = jsonConvert.convertListNotNull<SpeedMathsDataData>(json['data']);
	if (data != null) {
		speedMathsDataEntity.data = data;
	}
	return speedMathsDataEntity;
}

Map<String, dynamic> $SpeedMathsDataEntityToJson(SpeedMathsDataEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}

SpeedMathsDataData $SpeedMathsDataDataFromJson(Map<String, dynamic> json) {
	final SpeedMathsDataData speedMathsDataData = SpeedMathsDataData();
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		speedMathsDataData.name = name;
	}
	return speedMathsDataData;
}

Map<String, dynamic> $SpeedMathsDataDataToJson(SpeedMathsDataData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['name'] = entity.name;
	return data;
}