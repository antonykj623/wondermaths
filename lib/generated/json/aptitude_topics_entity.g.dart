import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/aptitude_topics_entity.dart';

AptitudeTopicsEntity $AptitudeTopicsEntityFromJson(Map<String, dynamic> json) {
	final AptitudeTopicsEntity aptitudeTopicsEntity = AptitudeTopicsEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		aptitudeTopicsEntity.status = status;
	}
	final List<AptitudeTopicsData>? data = jsonConvert.convertListNotNull<AptitudeTopicsData>(json['data']);
	if (data != null) {
		aptitudeTopicsEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		aptitudeTopicsEntity.message = message;
	}
	return aptitudeTopicsEntity;
}

Map<String, dynamic> $AptitudeTopicsEntityToJson(AptitudeTopicsEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

AptitudeTopicsData $AptitudeTopicsDataFromJson(Map<String, dynamic> json) {
	final AptitudeTopicsData aptitudeTopicsData = AptitudeTopicsData();
	final String? id = jsonConvert.convert<String>(json['Id']);
	if (id != null) {
		aptitudeTopicsData.id = id;
	}
	final String? topic = jsonConvert.convert<String>(json['Topic']);
	if (topic != null) {
		aptitudeTopicsData.topic = topic;
	}
	final String? srNo = jsonConvert.convert<String>(json['SrNo']);
	if (srNo != null) {
		aptitudeTopicsData.srNo = srNo;
	}
	final String? priority = jsonConvert.convert<String>(json['priority']);
	if (priority != null) {
		aptitudeTopicsData.priority = priority;
	}
	final String? subject = jsonConvert.convert<String>(json['Subject']);
	if (subject != null) {
		aptitudeTopicsData.subject = subject;
	}
	final String? chapter = jsonConvert.convert<String>(json['Chapter']);
	if (chapter != null) {
		aptitudeTopicsData.chapter = chapter;
	}
	final String? fileName = jsonConvert.convert<String>(json['FileName']);
	if (fileName != null) {
		aptitudeTopicsData.fileName = fileName;
	}
	final String? cBSC = jsonConvert.convert<String>(json['CBSC']);
	if (cBSC != null) {
		aptitudeTopicsData.cBSC = cBSC;
	}
	final String? state = jsonConvert.convert<String>(json['State']);
	if (state != null) {
		aptitudeTopicsData.state = state;
	}
	final String? topicId = jsonConvert.convert<String>(json['topicId']);
	if (topicId != null) {
		aptitudeTopicsData.topicId = topicId;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		aptitudeTopicsData.description = description;
	}
	return aptitudeTopicsData;
}

Map<String, dynamic> $AptitudeTopicsDataToJson(AptitudeTopicsData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['Id'] = entity.id;
	data['Topic'] = entity.topic;
	data['SrNo'] = entity.srNo;
	data['priority'] = entity.priority;
	data['Subject'] = entity.subject;
	data['Chapter'] = entity.chapter;
	data['FileName'] = entity.fileName;
	data['CBSC'] = entity.cBSC;
	data['State'] = entity.state;
	data['topicId'] = entity.topicId;
	data['description'] = entity.description;
	return data;
}