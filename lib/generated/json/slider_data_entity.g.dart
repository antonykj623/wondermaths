import 'package:myapp/generated/json/base/json_convert_content.dart';
import 'package:myapp/domain/slider_data_entity.dart';

SliderDataEntity $SliderDataEntityFromJson(Map<String, dynamic> json) {
	final SliderDataEntity sliderDataEntity = SliderDataEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		sliderDataEntity.status = status;
	}
	final List<SliderDataData>? data = jsonConvert.convertListNotNull<SliderDataData>(json['data']);
	if (data != null) {
		sliderDataEntity.data = data;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		sliderDataEntity.message = message;
	}
	return sliderDataEntity;
}

Map<String, dynamic> $SliderDataEntityToJson(SliderDataEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	data['message'] = entity.message;
	return data;
}

SliderDataData $SliderDataDataFromJson(Map<String, dynamic> json) {
	final SliderDataData sliderDataData = SliderDataData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		sliderDataData.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		sliderDataData.name = name;
	}
	final String? status = jsonConvert.convert<String>(json['status']);
	if (status != null) {
		sliderDataData.status = status;
	}
	return sliderDataData;
}

Map<String, dynamic> $SliderDataDataToJson(SliderDataData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['status'] = entity.status;
	return data;
}