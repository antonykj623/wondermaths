import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:myapp/pages/web_browser_page.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/speed_maths_data_entity.dart';
import '../network/apiservices.dart';

class CareerSuccess extends StatefulWidget {
   CareerSuccess() ;

  @override
  _CareerSuccessState createState() => _CareerSuccessState();
}

class _CareerSuccessState extends State<CareerSuccess> {


  List<SpeedMathsDataData>speddmathsdata=[];
 // List<String>classes=["Career Guidance","Scientific study technics","Concentration","Meditation Technics","How to face an interview board","Mind Control and personality"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCareerFiles();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(


      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Color(0xffFFD507)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.black87,
        elevation: 0,
        title: Text("Career Success",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Color(0xffFFD507)),),
        centerTitle: true,




      ),

      body: Container(

        child: Stack(

          children: [



            Align(


              alignment: FractionalOffset.topCenter,

              child: Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                child: GridView.builder(
                  itemCount: speddmathsdata.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemBuilder: (BuildContext context, int index) {
                    return  GestureDetector(

                      child:Card(
                          child: Container(

                            decoration: BoxDecoration(
                              // Box decoration takes a gradient
                                gradient: LinearGradient(
                                  // Where the linear gradient begins and ends
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  // Add one stop for each color. Stops should increase from 0 to 1
                                  stops: [0.1, 0.3, 0.6, 0.9],
                                  colors: [
                                    // Colors are easy thanks to Flutter's Colors class.
                                    Colors.black26,
                                    Colors.black45,
                                    Colors.black54,
                                    Colors.black,
                                  ],
                                )),

                            width: ResponsiveInfo.isMobile(context)?120:160,
                            height: ResponsiveInfo.isMobile(context)?150:200,

                            child:   Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,

                              children: [

                                Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                                  child:  Icon(Icons.picture_as_pdf,size: ResponsiveInfo.isMobile(context)?30:45,color: Color(0xffFFD507),),
                                ),

                                Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                                  child:


                                  Text(speddmathsdata[index].name.replaceAll(".pdf", ""),textAlign : TextAlign.center,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?12:15,color: Colors.white70),),
                                )


                              ],



                              //just for testing, will fill with image later
                            ),

                          )




                      ) ,

                      onTap: () async {

                        //  Navigator.pushReplacement(context,MaterialPageRoute(builder:(context) => WebBrowserPage(AppLiterals.careersuccessurl+speddmathsdata[index].name)));


                        // var uri = Uri.parse(AppLiterals.careersuccessurl+speddmathsdata[index].name);
                        // if (await canLaunchUrl(uri)){
                        // await launchUrl(uri);
                        // } else {
                        // // can't launch url
                        // }


                        var date = new DateTime.now()
                            .toIso8601String();


                        String urldata = AppLiterals.careersuccessurl+speddmathsdata[index].name;

                        Directory? _path = await getExternalStorageDirectory();
                        String _localPath = _path!.path +
                            "/" +
                            date.toString() + ".pdf";
                        final savedDir = File(
                            _localPath);
                        bool hasExisted = await savedDir
                            .exists();
                        if (!hasExisted) {
                          savedDir.create();
                        }


                        String path = _localPath;

                        var response = await http.get(
                            Uri.parse(urldata));

                        File file2 = new File(
                            path); // <-- 2
                        file2.writeAsBytesSync(
                            response.bodyBytes);

                        bool fileExists = await file2
                            .exists();

                        if (fileExists) {


                          OpenFile.open(file2.path).then((value) {
                            // File f = File(file2.path);
                            // f.delete();
                          });



                        }

                      },
                    )


                    ;
                  },
                ),



              ),


            )



          ],


        ),
        color: Colors.black87,
        width: double.infinity,
        height: double.infinity,
      )





    );
  }



  getCareerFiles()async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getCareerSuccess+"?timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );
    print(response);
    var js=jsonDecode(response);
    SpeedMathsDataEntity entity=SpeedMathsDataEntity.fromJson(js);


    if(entity.status==1)
    {
      List<SpeedMathsDataData> spmaths=[];

      if(entity.data.length>0)
      {

        for(int i=0;i<entity.data.length;i++)
        {
          if(entity.data[i].name.compareTo(".")==0 || entity.data[i].name.compareTo("..")==0 ){
            entity.data.removeAt(i);
          }
          else {
            spmaths.add(entity.data[i]);
          }
        }

        setState(() {

          speddmathsdata.addAll(spmaths);
        });


      }



    }




  }



}
