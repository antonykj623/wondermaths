import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:myapp/pages/pdf_topics.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/pdf_topic_group_entity.dart';
import '../network/apiservices.dart';

class PdfByClass extends StatefulWidget {

  String type;
  int class_index=0;
   PdfByClass(this.type,this.class_index) ;

  @override
  _PdfByClassState createState() => _PdfByClassState(this.type,this.class_index);
}

class _PdfByClassState extends State<PdfByClass> {

  String type;
  int class_index=0;

  _PdfByClassState(this.type,this.class_index);


  List<String>classes=["5","6","7","8","9","10"];
  List<PdfTopicGroupData> pdfdata=[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPdfTopics();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Color(0xffFFD507)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.black87,
        elevation: 0,
        title: Text("PDF",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Color(0xffFFD507)),),
        centerTitle: true,




      ),

      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.black87,

        child: Stack(

          children: [


            Align(

              alignment: FractionalOffset.topCenter,

              child:   Align(


                alignment: FractionalOffset.topCenter,

                child: Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                  child: GridView.builder(
                    itemCount: pdfdata.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemBuilder: (BuildContext context, int index) {
                      return  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                      child:GestureDetector(

                        child:Card(
                            child: Container(

                              width: ResponsiveInfo.isMobile(context)?120:160,
                              height: ResponsiveInfo.isMobile(context)?150:200,
                              decoration: BoxDecoration(
                                // Box decoration takes a gradient
                                  gradient: LinearGradient(
                                    // Where the linear gradient begins and ends
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    // Add one stop for each color. Stops should increase from 0 to 1
                                    stops: [0.1, 0.3, 0.5, 0.7],
                                    colors: [
                                      // Colors are easy thanks to Flutter's Colors class.
                                      Colors.black26,
                                      Colors.black45,
                                      Colors.black54,
                                      Colors.black,
                                    ],
                                  )),

                              child:   Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [

                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                                    child:  Icon(Icons.picture_as_pdf,size: ResponsiveInfo.isMobile(context)?30:45,color: Color(0xffFFD507),),
                                  ),

                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                                    child:


                                    Text(pdfdata[index].subject,textAlign : TextAlign.center,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?12:15,color: Colors.white70),),
                                  )


                                ],



                                //just for testing, will fill with image later
                              ),

                            )




                        ) ,

                        onTap: () async {

                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      PdfTopics(type,class_index,pdfdata[index].topic)));


                          // var uri = Uri.parse(AppLiterals.careersuccessurl+speddmathsdata[index].name);
                          // if (await canLaunchUrl(uri)){
                          //   await launchUrl(uri);
                          // } else {
                          //   // can't launch url
                          // }

                        },
                      ) ,

                      )





                      ;
                    },
                  ),



                ),


              ),

            )



          ],


        ),
      )





    );
  }


  getPdfTopics() async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getTopicForPdfByClass+"?param1="+classes[class_index]+"&param2="+type+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);

    PdfTopicGroupEntity entity=PdfTopicGroupEntity.fromJson(js);

    if(entity.status==1)
      {


    setState(() {

      pdfdata.clear();
      pdfdata.addAll(entity.data);


    });



      }





  }
}
