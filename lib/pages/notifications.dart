import 'dart:convert';

import 'package:flutter/material.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../network/apiservices.dart';

class Notifications extends StatefulWidget {
   Notifications() ;

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {




  @override
  Widget build(BuildContext context) {
    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Notifications",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Colors.black),),
        centerTitle: true,




      ),

      body: Center(

        child: Text("No data found",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Colors.black),),
      ),



    );
  }



  getNotifications() async
  {
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");



    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    saveapp_baseurl+ WebServiceMethodes.getNotifications+"?timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );
    print(response);
    var js=jsonDecode(response);












  }
}
