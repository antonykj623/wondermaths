import 'dart:async';
import 'dart:io';

import 'package:android_id/android_id.dart';
import 'package:flutter/material.dart';
import 'package:myapp/pages/Homepage.dart';
import 'package:myapp/pages/Login.dart';
import 'package:myapp/pages/class_list.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/AppLiterals.dart';
import '../design/ResponsiveInfo.dart';

class Splash extends StatefulWidget {
   Splash() ;

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {


 late AnimationController animationController;
  late Animation<double> animation;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController=new AnimationController(vsync: this, duration: Duration(milliseconds: 2000));


    animation= CurvedAnimation(
      parent: animationController,
      curve: Curves.easeIn,
    );


    animationController.forward();





    Timer(Duration(seconds: 6),
    (){
       redirectToPage();
    });



  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body:


      Container(
        color: Colors.black87,

        child:  Stack(

          children: [

            Align(

              alignment: FractionalOffset.center,

              child:  FadeTransition(
                opacity: animation,



            child:  Card(

                child: Padding(

                    padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                  child: Image.asset("assets/pages/images/icondata.png",width: ResponsiveInfo.isSmallMobile(context)
                      ? ResponsiveInfo.isMobile(context) ? 100 :150:180,height: ResponsiveInfo.isSmallMobile(context)
                      ? ResponsiveInfo.isMobile(context) ? 100 :150:180,fit: BoxFit.fill,),
                ),



                color: Colors.white,
                elevation: 15,
              )
              )


            )
          ],







        ),
        width: double.infinity,
        height: double.infinity,
      )



    )








      ;
  }



  redirectToPage()async
  {
    final preferenceDataStorage = await SharedPreferences
        .getInstance();

    String? token= preferenceDataStorage.getString(
        AppLiterals.Tokenkey);




    if(token!=null)
      {

        if(token.isNotEmpty)
          {

            Navigator.pushReplacement(context,MaterialPageRoute(builder:(context) => Homepage()));

          }
        else{

          Navigator.pushReplacement(context,MaterialPageRoute(builder:(context) => Login()));
        }


      }
    else{

      Navigator.pushReplacement(context,MaterialPageRoute(builder:(context) => Login()));
    }



  }
}
