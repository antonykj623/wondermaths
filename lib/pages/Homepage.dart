import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:android_id/android_id.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:intl/intl.dart';
import 'package:myapp/domain/version_data_app_entity.dart';
import 'dart:ui';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:myapp/pages/Login.dart';
import 'package:myapp/pages/career_success.dart';
import 'package:myapp/pages/class_list.dart';
import 'package:myapp/pages/foundation.dart';
import 'package:myapp/pages/notifications.dart';
import 'package:myapp/pages/profile.dart';
import 'package:myapp/pages/questionbank.dart';
import 'package:myapp/pages/scholarship.dart';
import 'package:myapp/pages/speed_maths.dart';
import 'package:myapp/pages/topics.dart';
import 'package:myapp/utils.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/profile_data_entity.dart';
import '../domain/slider_data_entity.dart';
import '../network/apiservices.dart';

class Homepage extends StatefulWidget {

  Homepage();

  @override
  _MyHomepageState createState() => _MyHomepageState();

}

class _MyHomepageState extends State<Homepage> {

  List<Widget> nsdwidget = [];

  String profiledata="",profileimage="";

  List<String>menudatas=["Class wise Package","Scholarship/olympiad","IIT foundation","Question Bank","Speed Maths","Career Success"];

  List<String>icons=[    "assets/pages/images/training.png",
    "assets/pages/images/mortarboard.png",
    "assets/pages/images/corporate.png",
    "assets/pages/images/faq.png",
    "assets/pages/images/maths.png",
    "assets/pages/images/goal.png",



  ];

  TextEditingController usernamecontroller=new TextEditingController();
  TextEditingController passwordcontroller=new TextEditingController();

  Duration oneSec = Duration(seconds:7);
 late Timer _timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSliderImages();
    getProfileImages();
    updateWonderMathsDeviceId();


    _timer=  Timer.periodic(oneSec, (Timer t) =>
        getProfileImages());



  }


  @override
  void dispose() {
    if(_timer!=null)
      {

        _timer.cancel();
      }
  }

  showDeleteConfirmationDialog()
  {

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: height/1.6,
        width: width,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: Text("Confirm your account ?",style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold),),
            ),
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: usernamecontroller,
                keyboardType: TextInputType.number,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Mobile number',


                ),
              )),
            ),
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: passwordcontroller,
                obscureText: true,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Password',


                ),
              )),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      if(usernamecontroller.text.isNotEmpty) {

                        if(passwordcontroller.text.isNotEmpty) {
                          Navigator.pop(context);


                          userLogin(usernamecontroller.text.toString(),
                              passwordcontroller.text.toString());
                        }
                      }






                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),
            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);

  }



  userLogin(String username,String password) async {
    final preferenceDataStorage = await SharedPreferences
        .getInstance();

    String? classid = preferenceDataStorage.getString(
        AppLiterals.Classid);


    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices = new ApiServices();

    String url = AppLiterals.saveapp_baseurl +
        WebServiceMethodes.UserLoginSave + "?timestamp=" + date.toString();


    Map mp = new HashMap();
    mp['password'] = passwordcontroller.text;
    mp['mobile'] = usernamecontroller.text;
    mp['uuid'] = date;
    mp['timestamp'] = date;


    String? response = await apiServices.postmethod(context, mp, url
    );


    if (response
        .toString()
        .isNotEmpty) {
      var jsondata = jsonDecode(response.toString());

      if (jsondata['status'] != 0) {
        String url = AppLiterals.saveapp_baseurl +
            WebServiceMethodes.deleteAccount + "?timestamp=" + date.toString();


        String? response = await apiServices.getmethod(context, null, url
        );
        print(response);
        var json = jsonDecode(response);

        if (json['status'] == 1) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(
            content: Text("Account deleted "),
          ));
          preferenceDataStorage.setString(AppLiterals.Tokenkey, "");

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (BuildContext context) => Login()),
              ModalRoute.withName('/')
          );
        }

        else {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(
            content: Text("Account deleted "),
          ));
        }
      }
      else {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(
          content: Text("Account authentication failed "),
        ));
      }
    }
  }


    @override
    Widget build(BuildContext context) {
      double baseWidth = 360;
      double fem = MediaQuery
          .of(context)
          .size
          .width / baseWidth;
      double ffem = fem * 0.97;

      List<String> stds = [
        "7th Standard",
        "8th Standard",
        "9th Standard",
        "10th Standard"
      ];

      final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<
          ScaffoldState>();


      return Scaffold(
        resizeToAvoidBottomInset: true,

        drawer: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the drawer if there isn't enough vertical
          // space to fit everything.
          child: ListView(

            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.white70,
                ),
                child: Container(
                  width: double.infinity,
                  height: ResponsiveInfo.isMobile(context) ? 180 : 220,

                  child:
                  Center(

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,

                        children: [

                          GestureDetector(

                            child:  Icon(
                              Icons.account_circle,
                              size: ResponsiveInfo.isMobile(context) ? 60 : 100,
                              color: Colors.black,) ,

                            onTap: () {
                              // Scaffold.of(context).closeDrawer();
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Profile()));
                            },
                          ),
                          Padding(padding: EdgeInsets.all(ResponsiveInfo
                              .isMobile(context) ? 10 : 15),


                            child: Text(profiledata, style: TextStyle(
                                color: Colors.white70,
                                fontSize: ResponsiveInfo.isMobile(context)
                                    ? 13
                                    : 15),),

                          )


                        ],
                      )


                  ),
                ),


              ),
              ListTile(
                leading: Icon(Icons.home, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('Home', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () {
                  // Scaffold.of(context).closeDrawer();

                },
              ),
              ListTile(
                leading: Icon(Icons.notifications, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('Notifications', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () {
                  // Scaffold.of(context).closeDrawer();

                  Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) =>
                              Notifications()));
                },
              ),

              ListTile(
                leading: Icon(Icons.update, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('App Update', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () {
                  // Scaffold.of(context).closeDrawer();

                  showAppUpdateDialog();







                },
              ),

              ListTile(
                leading: Icon(Icons.privacy_tip_outlined, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('Privacy Policy', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () async{
                  // Scaffold.of(context).closeDrawer();

                  String privacypolicy=AppLiterals.domain+"index.php/web/memTerms2";

                  var uri = Uri.parse(privacypolicy);
                  if (await canLaunchUrl(uri)){
                  await launchUrl(uri);
                  } else {
                  // can't launch url
                  }





                },
              ),

              ListTile(
                leading: Icon(Icons.account_box_rounded, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('About us', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () async{
                  // Scaffold.of(context).closeDrawer();

                  String privacypolicy=AppLiterals.domain+"index.php/web/about";

                  var uri = Uri.parse(privacypolicy);
                  if (await canLaunchUrl(uri)){
                    await launchUrl(uri);
                  } else {
                    // can't launch url
                  }





                },
              ),

              ListTile(
                leading: Icon(Icons.settings, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('Delete Account', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () {
                  // Scaffold.of(context).closeDrawer();


                  Dialog forgotpasswordDialog = Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    //this right here
                    child: Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height / 1.4,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Theme(data: new ThemeData(
                                hintColor: Colors.black38
                            ),
                                child: Text(
                                  "Demo account/App License deletion \n\n",
                                  style: TextStyle(
                                      fontSize: ResponsiveInfo.isSmallMobile(
                                          context)
                                          ? ResponsiveInfo.isMobile(context)
                                          ? 14
                                          : 17 : 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),

                                )),
                          )

                          ,
                          Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Theme(data: new ThemeData(
                                hintColor: Colors.black38
                            ),
                                child: Text(
                                  "If deleted , all your credentials will be removed from the database and you cannot login to the app and website anymore\n\nDo you want to contitnue ?",

                                )),
                          ),
                          Padding(
                              padding: EdgeInsets.all(15.0),

                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [

                                  Container(


                                    child: Align(
                                      alignment: Alignment.center,
                                      child: TextButton(

                                        onPressed: () {
                                          Navigator.of(
                                              context, rootNavigator: true)
                                              .pop();


                                          showDeleteConfirmationDialog();
                                        },

                                        child: Text('Yes', style: TextStyle(
                                            color: Colors.blue),),),
                                    ),


                                    //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                  ),

                                  Container(

                                    child: Align(
                                      alignment: Alignment.center,
                                      child: TextButton(

                                        onPressed: () {
                                          Navigator.of(
                                              context, rootNavigator: true)
                                              .pop();
                                        },

                                        child: Text('No',
                                          style: TextStyle(color: Colors.blue
                                          ),),),
                                    ),


                                    //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                  )
                                ],


                              )


                            // ,
                          ),
                          // Padding(padding: EdgeInsets.only(top: 50.0)),
                          // TextButton(onPressed: () {
                          //   Navigator.of(context).pop();
                          // },
                          //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                        ],
                      ),
                    ),
                  );


                  showDialog(context: context,
                      builder: (BuildContext context) => forgotpasswordDialog);
                },
              ),
              ListTile(
                leading: Icon(Icons.logout, color: Colors.black,
                  size: ResponsiveInfo.isMobile(context) ? 25 : 30,),
                title: Text('Logout', style: TextStyle(
                    fontSize: ResponsiveInfo.isMobile(context) ? 13 : 16),),
                onTap: () {
                  // Scaffold.of(context).closeDrawer();
                  AwesomeDialog(
                    context: context,
                    dialogType: DialogType.INFO,
                    headerAnimationLoop: false,
                    animType: AnimType.RIGHSLIDE,
                    showCloseIcon: true,
                    closeIcon: const Icon(Icons.close_fullscreen_outlined),
                    title: 'Wonder Maths',
                    desc: 'Do you want to exit now?',
                    btnOkOnPress: () async {
                      final preferenceDataStorage = await SharedPreferences
                          .getInstance();

                      preferenceDataStorage.setString(
                          AppLiterals.Tokenkey, "");
                      preferenceDataStorage.setString(
                          AppLiterals.Classid, "");

                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => Login()),
                          ModalRoute.withName('/')
                      );
                    },
                    btnCancelOnPress: () {
                      // Navigator.pop(context, "no");
                    },
                    btnOkColor: Colors.blue,
                    onDissmissCallback: (type) {
                      // debugPrint('Dialog Dissmiss from callback $type');
                    },
                  ).show();
                },
              ),
            ],
          ),
        ),

        appBar: AppBar(
          automaticallyImplyLeading: true,
          toolbarHeight: ResponsiveInfo.isMobile(context) ? 50 : 70,
          leading: Builder(
            builder: (context) =>
                IconButton(
                  icon: new Icon(Icons.menu, color: Color(0xffFFD507),
                    size: ResponsiveInfo.isMobile(context) ? 25 : 35,),
                  onPressed: () => Scaffold.of(context).openDrawer(),
                ),
          ),


          backgroundColor:    Colors.black87,
          elevation: 0,
          title: Text("Wonder Maths", style: TextStyle(
              fontSize: ResponsiveInfo.isMobile(context) ? 17 : 22,
              color: Color(0xffFFD507)),),
          centerTitle: false,

          actions: [


            Padding(padding: EdgeInsets.all(
                ResponsiveInfo.isMobile(context) ? 10 : 15),

              child: IconButton(
                icon: Icon(Icons.exit_to_app, color: Color(0xffFFD507)),
                onPressed: () async {
                  AwesomeDialog(
                    context: context,
                    dialogType: DialogType.INFO,
                    headerAnimationLoop: false,
                    animType: AnimType.RIGHSLIDE,
                    showCloseIcon: true,
                    closeIcon: const Icon(Icons.close_fullscreen_outlined),
                    title: 'Wonder Maths',
                    desc: 'Do you want to exit now?',
                    btnOkOnPress: () async {
                      final preferenceDataStorage = await SharedPreferences
                          .getInstance();

                      preferenceDataStorage.setString(
                          AppLiterals.Tokenkey, "");
                      preferenceDataStorage.setString(
                          AppLiterals.Classid, "");

                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => Login()),
                          ModalRoute.withName('/')
                      );
                    },
                    btnCancelOnPress: () {
                      // Navigator.pop(context, "no");
                    },
                    btnOkColor: Colors.blue,
                    onDissmissCallback: (type) {
                      // debugPrint('Dialog Dissmiss from callback $type');
                    },
                  ).show();
                },
              ),


            )


          ],


        ),

        body:Container(

          width: double.infinity,
          height: double.infinity,
          color: Colors.black87,

          child:Stack(

            children: [

              Align(

                alignment: FractionalOffset.topCenter,

                child: Container(
                    height: (MediaQuery
                        .of(context)
                        .size
                        .width) / 1.79349112,
                    width: double.infinity,
                    child: PageIndicatorContainer(
                        length: nsdwidget.length,
                        align: IndicatorAlign.bottom,
                        indicatorSpace: 14.0,
                        padding: const EdgeInsets.all(10),
                        indicatorColor: Colors.black12,
                        indicatorSelectorColor: Colors.blueGrey,
                        shape: IndicatorShape.circle(size: 10),
                        child: PageView.builder(
                          scrollDirection: Axis.horizontal,
                          // controller: controller,
                          itemBuilder: (BuildContext context, int index) {
                            return nsdwidget[index];
                          },
                          itemCount: nsdwidget.length,
                          // children: nsdwidget,
                        ))),
              ),

              Align(
                alignment: FractionalOffset.topCenter,

                child: Padding(padding: EdgeInsets.only(left: ResponsiveInfo.isMobile(context)?10:15, top: MediaQuery
                    .of(context)
                    .size
                    .width / 1.7266, right: ResponsiveInfo.isMobile(context)?10:15, bottom: ResponsiveInfo.isMobile(context)?10:15),


                    child: GridView.builder(
                      itemCount: menudatas.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,


                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                          child: GestureDetector(

                            child: Card(
                                elevation: ResponsiveInfo.isMobile(context)?10:15,

                                child: Container(
                        decoration: BoxDecoration(
                        // Box decoration takes a gradient
                        gradient: LinearGradient(
                        // Where the linear gradient begins and ends
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        // Add one stop for each color. Stops should increase from 0 to 1
                        stops: [0.1, 0.3, 0.6, 0.9],
                        colors: [
                        // Colors are easy thanks to Flutter's Colors class.
                        Colors.black26,
                        Colors.black45,
                        Colors.black54,
                        Colors.black,
                        ],
                        )),



                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,

                                    children: [

                                      ColorFiltered(
                                          colorFilter:
                                          ColorFilter.mode(Color(0xffFFD507), BlendMode.srcIn),
                                          child: Image.asset(icons[index],
                                            width: ResponsiveInfo.isMobile(context)
                                                ? 40
                                                : 50,
                                            height: ResponsiveInfo.isMobile(context)
                                                ? 40
                                                : 50,
                                            fit: BoxFit.fill,)
                                      )

                                      ,

                                      Padding(padding: EdgeInsets.all(
                                          ResponsiveInfo.isMobile(context)
                                              ? 10
                                              : 15),

                                        child: Text(menudatas[index],
                                          style: TextStyle(
                                              fontSize: ResponsiveInfo.isMobile(
                                                  context) ? 12 : 15,
                                              color: Colors.white70,fontWeight: FontWeight.bold),),
                                      )


                                    ],


                                    //just for testing, will fill with image later
                                  ),

                                )


                            ),

                            onTap: () async {

                              final preferenceDataStorage = await SharedPreferences
                                  .getInstance();

                              bool? expired=     preferenceDataStorage.getBool(AppLiterals.activationperiodexpire);

                              // if(!expired!) {
                              if (index == 0) {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ClassList()));
                              }
                              else if (index == 1) {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Scholarship()));
                              }
                              else if (index == 2) {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Foundation()));
                              }

                              else if (index == 3) {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Questionbank()));
                              }

                              else if (index == 4) {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            SpeedMaths()));
                              }


                              else if (index == 5) {
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            CareerSuccess()));
                              }
                              //  }
                              // else{
                              //
                              //   AwesomeDialog(
                              //     context: context,
                              //     dialogType: DialogType.INFO,
                              //     headerAnimationLoop: false,
                              //     animType: AnimType.RIGHSLIDE,
                              //     showCloseIcon: true,
                              //     closeIcon: const Icon(Icons.close_fullscreen_outlined),
                              //     title: 'Wonder Maths',
                              //     desc: 'Your activation is expired. Do you want to renew now ?',
                              //     btnOkOnPress: () async {
                              //       var uri = Uri.parse(AppLiterals.activationredirecturl);
                              //       if (await canLaunchUrl(uri)){
                              //         await launchUrl(uri);
                              //       } else {
                              //         // can't launch url
                              //       }
                              //     },
                              //     btnCancelOnPress: () {
                              //       // Navigator.pop(context, "no");
                              //     },
                              //     btnOkColor: Colors.blue,
                              //     onDissmissCallback: (type) {
                              //       // debugPrint('Dialog Dissmiss from callback $type');
                              //     },
                              //   ).show();
                              //
                              //
                              //
                              // }

                            },
                          ),


                        )




                        ;
                      },
                    )

                ),

              )


            ],
          ),
        )




      );
    }



    updateWonderMathsDeviceId()async
    {



      if(Platform.isAndroid)
      {

        const androidId=AndroidId();
        String? deviceId = await androidId.getId();

        print("Android Device ID : "+deviceId.toString());
        updateDeviceIdToServer(deviceId.toString());

      }
      else{


      }
    }

    updateDeviceIdToServer(String deviceid)async
    {

      final datacount = await SharedPreferences.getInstance();
      var date = new DateTime.now().toIso8601String();

      Map mp = new HashMap();
      mp['device_id'] = deviceid;
      mp['platform'] = "Android";

      mp['timestamp'] = date;

      ApiServices apiServices = new ApiServices();

      String url = AppLiterals.
      saveapp_baseurl + WebServiceMethodes.updateWondermathsDeviceId + "?timestamp=" +
          date.toString();

      String? response = await apiServices.postmethod(context, mp, url
      );
      print(response.toString());

    }




    showAppUpdateDialog()async
    {



      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      String appName = packageInfo.appName;
      String packageName = packageInfo.packageName;
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;

      print(version);

      ApiServices apiServices=new ApiServices();
      var date = new DateTime.now().toIso8601String();

      String url=AppLiterals.saveapp_baseurl+WebServiceMethodes.wonderMathsVersion+"?date="+date;

      String? response = await apiServices.getmethod(context,  null, url
      );
      print(response);
      dynamic jsondata = jsonDecode(response);
      VersionDataAppEntity entity=VersionDataAppEntity.fromJson(jsondata);
      if(entity.status==1)
      {
        String v=entity.data.versionname;
        double versionno=double.parse(v);
        double appversion=double.parse(version);

        Dialog forgotpasswordDialog = Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
          child: Container(
            height: 300.0,
            width: 500.0,

            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding:  EdgeInsets.all(15.0),
                  child: Text("App Version : "+appversion.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?13:15),)
                ),
                (appversion<versionno)? Padding(
                  padding: EdgeInsets.all(15.0),

                  child: Container(

                    width: 150,
                    height: 55,
                    decoration: BoxDecoration(

                        color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                    child:Align(
                      alignment: Alignment.center,
                      child: TextButton(

                        onPressed:() async{
                          Navigator.pop(context);

                          String url="https://play.google.com/store/apps/details?id="+packageName;

                          var uri = Uri.parse(url);
                          if (await canLaunchUrl(uri)){
                          await launchUrl(uri);
                          } else {
                          // can't launch url
                          }


                        },

                        child: Text('Update', style: TextStyle(color: Colors.white) ,) ,),
                    ),



                    //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                  ),


                  // ,
                ) : Container(),
                // Padding(padding: EdgeInsets.only(top: 50.0)),
                // TextButton(onPressed: () {
                //   Navigator.of(context).pop();
                // },
                //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
              ],
            ),
          ),
        );



        showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);








      }





    }


    getSliderImages() async
    {
      var date = new DateTime.now().toIso8601String();

      ApiServices apiServices = new ApiServices();

      String url = AppLiterals.
      wondermaths_baseurl + WebServiceMethodes.getSliderImages + "?timestamp=" +
          date.toString();

      String response = await apiServices.getmethod(context, null, url
      );
      print(response);
      var js = jsonDecode(response);

      SliderDataEntity sliderDataEntity = SliderDataEntity.fromJson(js);

      if (sliderDataEntity.status == 1) {
        setState(() {
          for (int i = 0; i < sliderDataEntity.data.length; i++) {
            nsdwidget.add(Container(
              height: (MediaQuery
                  .of(context)
                  .size
                  .width) / 1.89349112,
              width: double.infinity,
              child: Card(
                  child:
                  Image.network(AppLiterals.wondermaths_slider_image_baseurl +
                      sliderDataEntity.data[i].name, fit: BoxFit.fill,)),
            ));
          }
        });
      }
    }

    getProfileImages() async {
      final datacount = await SharedPreferences.getInstance();
      var date = new DateTime.now().toIso8601String();

      ApiServices apiServices = new ApiServices();

      String url = AppLiterals.
      saveapp_baseurl + WebServiceMethodes.getUserDetails + "?timestamp=" +
          date.toString();

      String? response = await apiServices.postmethod(context, null, url
      );
      print(response.toString());
      var js = jsonDecode(response.toString());

      ProfileDataEntity profileDataEntity = ProfileDataEntity.fromJson(js);

      if (profileDataEntity.status == 1) {
        setState(() {
          profiledata = profileDataEntity.data.fullName + "\n" +
              profileDataEntity.data.mobile;
          profileimage = profileDataEntity.data.profileImage;
        });


        String date=profileDataEntity.data.activationDate;

        DateTime accountsdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
        DateTime dtnow=new DateTime.now();

        if(accountsdateparsed.isBefore(dtnow))
        {

          datacount.setBool(AppLiterals.activationperiodexpire, true);
        }
        else{
          datacount.setBool(AppLiterals.activationperiodexpire, false);



        }



      }
    }



}