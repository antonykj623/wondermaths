import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:myapp/constants/AppLiterals.dart';
import 'package:myapp/constants/WebMethodes.dart';
import 'package:myapp/network/apiservices.dart';
import 'package:myapp/pages/Login.dart';
import 'package:myapp/pages/class_wise_test.dart';
import 'package:myapp/pages/olympiad.dart';
import 'package:myapp/pages/pdf_by_class.dart';
import 'package:myapp/pages/registration.dart';
import 'package:myapp/pages/scholar_ship_class_wise.dart';
import 'package:myapp/pages/videos_by_class.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../design/ResponsiveInfo.dart';
import '../domain/class_sylabus_entity.dart';
import '../utils.dart';
import 'menta_ability_class_wise_test.dart';

class ClassList extends StatefulWidget {
   ClassList() ;

  @override
  _ClassListState createState() => _ClassListState();
}

class _ClassListState extends State<ClassList> {


  List<ClassSylabusData> classdata=[];

  List<ClassSylabusDataClassesdata> classesylabusdata=[];

  int selectedindex=-1,selectedclassindex=-1;

  List<String>sylabustype=["State","CBSC"];

  List<String>classes=["Class 5","Class 6","Class 7","Class 8","Class 9","Class 10"];

  List<String>options=["Video","Pdf","Class Wise Test","Olympiad","Scholarship","Mental Ability"];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getClassWithSylabus();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_new, color: Color(0xffFFD507)),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Colors.black87,
          elevation: 0,
          title: Text("Class wise package",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Color(0xffFFD507)),),
          centerTitle: true,




        ),

        body: Container(
          color: Colors.black87,

          child:  Stack(

            children: [

                Align(

                  alignment: FractionalOffset.topCenter,

                  child: Column(

                    // crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,

                    children: [

                      SizedBox(
                        width: double.infinity,
                        height: ResponsiveInfo.isMobile(context)? 60 : 90 ,

                        child: (sylabustype.length>0)? ListView.builder(
                            itemCount: sylabustype.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                  padding: EdgeInsets.all(
                                      ResponsiveInfo.isMobile(context) ? 10 : 15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(0xffFFD507)),
                                      borderRadius: BorderRadius.circular(8),
                                      color: (selectedindex==index)? Color(0xffFFD507) : Colors.white12,
                                    ),
                                    height: ResponsiveInfo.isMobile(context) ? 40 : 55,

                                    child: TextButton(
                                        onPressed: () async {


                                          setState(() {
                                            selectedindex=index;



                                          });

                                        },
                                        child: Text(
                                          sylabustype[index] ,
                                          style:  TextStyle(
                                              color:(selectedindex==index)?  Colors.white : Color(0xffFFD507),
                                              fontFamily: 'poppins'),
                                        )),
                                  )
                              ) ;
                            }) : Container(),
                      ),

                      (selectedindex!=-1)? SizedBox(
                        width: double.infinity,
                        height: ResponsiveInfo.isMobile(context)? 60 : 90 ,

                        child: (classes.length>0)? ListView.builder(
                            itemCount: classes.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                  padding: EdgeInsets.all(
                                      ResponsiveInfo.isMobile(context) ? 10 : 15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(0xffFFD507)),
                                      borderRadius: BorderRadius.circular(8),
                                      color: (selectedclassindex==index)? Color(0xffFFD507) : Colors.white12,
                                    ),
                                    height: ResponsiveInfo.isMobile(context) ? 40 : 55,

                                    child: TextButton(
                                        onPressed: () async {


                                          setState(() {
                                            selectedclassindex=index;



                                          });

                                        },
                                        child: Text(
                                          classes[index] ,
                                          style:  TextStyle(
                                              color:(selectedclassindex==index)?  Colors.white : Color(0xffFFD507),
                                              fontFamily: 'poppins'),
                                        )),
                                  )
                              ) ;
                            }) : Container(),
                      ) : Container(),

                      (selectedclassindex!=-1)?  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                      child: ListView.builder(
                          itemCount: options.length,
                          shrinkWrap: true,
                          primary: false,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(

                              child:Card(

                                child: Container(

                                  decoration: BoxDecoration(
                                    // Box decoration takes a gradient
                                      gradient: LinearGradient(
                                        // Where the linear gradient begins and ends
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        // Add one stop for each color. Stops should increase from 0 to 1
                                        stops: [0.1, 0.5, 0.7, 0.9],
                                        colors: [
                                          // Colors are easy thanks to Flutter's Colors class.
                                          Colors.black26,
                                          Colors.black45,
                                          Colors.black54,
                                          Colors.black,
                                        ],
                                      )),

                                  child: ListTile(

                                      trailing:  Icon(Icons.arrow_forward_ios_rounded,size: ResponsiveInfo.isMobile(context)?25:35,color: Color(0xffFFD507),),
                                      title:Text(options[index],style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?15:18,color: Colors.white70),) ),
                                ),



                                elevation: 8,
                              ) ,

                              onTap: (){

                                if(index==0)
                                  {
                                    Navigator.push(context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                VideosByClass(selectedclassindex)));

                                  }
                                else if(index==1)
                                  {

                                    Navigator.push(context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                PdfByClass(sylabustype[selectedindex],selectedclassindex)));

                                  }

                                else if(index==2)
                                {

                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ClassWiseTest(sylabustype[selectedindex],selectedclassindex)));

                                }

                                else if(index==3)
                                {

                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              Olympiad(sylabustype[selectedindex],selectedclassindex)));

                                }

                                else if(index==4)
                                {

                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ScholarShipClassWise(sylabustype[selectedindex],selectedclassindex)));

                                }
                                else if(index==5)
                                {

                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              MentaAbilityClassWiseTest(sylabustype[selectedindex],selectedclassindex)));

                                }

                              },


                            )




                              ;
                          }),


                      ) : Container()





                    ],
                  )




                   ,

                ),







              
              
              
              

              
              
              
              
              
              
              
              
              
            ],







          ),
          width: double.infinity,
          height: double.infinity,
        )



    );
  }




}
