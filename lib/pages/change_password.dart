import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../network/apiservices.dart';

class ChangePassword extends StatefulWidget {

  String mobile;

   ChangePassword(this.mobile) ;

  @override
  _ChangePasswordState createState() => _ChangePasswordState(this.mobile);
}

class _ChangePasswordState extends State<ChangePassword> {

  String mobile;



  _ChangePasswordState(this.mobile);



  TextEditingController passwordcontroller=new TextEditingController();
  TextEditingController confirmpasswordcontroller=new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    double baseWidth = 360;
    double fem = MediaQuery
        .of(context)
        .size
        .width / baseWidth;


    return Scaffold(

      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Change Password",style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?13:16),),
        centerTitle: false,

      ),

      body: Column(


        children: [

          Container(
            // frame1uaR (3:13)
            margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 15*fem, 29*fem),
            padding: EdgeInsets.fromLTRB(31*fem, 0, 0, 0),
            width: double.infinity,

            decoration: BoxDecoration (
              border: Border.all(color: Color(0xfcc0b9b9)),
              color: Color(0xfff5f5f5),
              borderRadius: BorderRadius.circular(10*fem),
            ),
            child: TextField(
              // emailX61 (3:11)
              keyboardType: TextInputType.text,
              obscureText: true,
              controller: passwordcontroller,

              decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: 'Password',
                  hintText: 'Password'

              ),

              style: TextStyle (


                fontSize: ResponsiveInfo.isMobile(context)?13:15,
                fontWeight: FontWeight.w600,

                color: Color(0x82000000),


              ),
            ),
          ),
          Container(
            // frame1uaR (3:13)
            margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 15*fem, 29*fem),
            padding: EdgeInsets.fromLTRB(31*fem, 0, 0, 0),
            width: double.infinity,

            decoration: BoxDecoration (
              border: Border.all(color: Color(0xfcc0b9b9)),
              color: Color(0xfff5f5f5),
              borderRadius: BorderRadius.circular(10*fem),
            ),
            child: TextField(
              // emailX61 (3:11)
              keyboardType: TextInputType.text,
              obscureText: true,
              controller: confirmpasswordcontroller,

              decoration: InputDecoration(
                  border: InputBorder.none,
                  labelText: 'Confirm Password',
                  hintText: 'Confirm Password'

              ),

              style: TextStyle (


                fontSize: ResponsiveInfo.isMobile(context)?13:15,
                fontWeight: FontWeight.w600,

                color: Color(0x82000000),


              ),
            ),
          ),


          Container(
            // frame3sxV (3:17)
              margin: EdgeInsets.fromLTRB(81*fem, 0*fem, 96*fem, 34*fem),
              width: double.infinity,
              height: ResponsiveInfo.isMobile(context)?50:65,
              decoration: BoxDecoration (
                border: Border.all(color: Color(0xfcc0b9b9)),
                color: Color(0xff1994ee),
                borderRadius: BorderRadius.circular(10*fem),
              ),
              child: GestureDetector(

                child: Center(
                  child: Text(
                    'Submit',
                    style: TextStyle (

                      fontSize: ResponsiveInfo.isMobile(context)?13:15,
                      fontWeight: FontWeight.w700,
                      height: ResponsiveInfo.isMobile(context)?50:60,
                      color: Color(0xfff5f5f5),
                    ),
                  ),
                ),
                onTap: () async {
                  // Navigator.push(context,MaterialPageRoute(builder:(context) => Homepage()));

                  if(passwordcontroller.text.isNotEmpty && confirmpasswordcontroller.text.isNotEmpty)
                  {

                    if(passwordcontroller.text.length>=8)
                    {

                      if(passwordcontroller.text.compareTo(confirmpasswordcontroller.text)==0)
                      {

                        var date = new DateTime.now().toIso8601String();

                        ApiServices apiServices=new ApiServices();

                        String url = AppLiterals.saveapp_baseurl + WebServiceMethodes.changePassword+"?timestamp="+date.toString();

                        Map mp = new HashMap();

                        mp['mobile'] = mobile;
                        mp['password']=passwordcontroller.text.toString();

                        String? response = await apiServices.postmethod(context,  mp, url
                        );
                        if(response.toString().isNotEmpty) {

                          var jsondata = jsonDecode(response.toString());

                          if(jsondata['status']==1)
                          {

Navigator.pop(context);
                          }







                        }



                      }
                      else{


                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Your password confirmation failed"),
                        ));
                      }

                    }
                    else{


                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Your password must have 8 characters"),
                      ));
                    }
                  }
                  else{


                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Please enter your password and confirm it"),
                    ));
                  }






                },
              )



          ),


        ],


      ),


    );
  }
}
