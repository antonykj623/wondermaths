import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:myapp/domain/video_by_class_entity.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../network/apiservices.dart';

class VideosByClass extends StatefulWidget {

  int selectedindex=0;

   VideosByClass(this.selectedindex) ;

  @override
  _VideosByClassState createState() => _VideosByClassState(selectedindex);
}

class _VideosByClassState extends State<VideosByClass> {


  List<String>classes=["5","6","7","8","9","10"];
  List<VideoByClassData> videosbyclass=[];
  int selectedindex=0;
  List<String>dropdowndata=[];

  String data="Select topic";

  _VideosByClassState(selectedindex);

  String videoLink="",videoid="";




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getClassVideos();

  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(

        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Color(0xffFFD507)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.black87,
        elevation: 0,
        title: Text("Videos",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Color(0xffFFD507)),),
        centerTitle: true,




      ),

      body:Container(
        color: Colors.black87,
        child: Stack(

          children: [


            Align(

                alignment: FractionalOffset.topCenter,

                child:  Column(

                  children: [


                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),






                        child:
                        (dropdowndata.length>0)?
                            Container(

                              child:DropdownMenu<String>(
                                initialSelection:  dropdowndata.first ,

                                onSelected: (String? value) {
                                  // This is called when the user selects an item.
                                  setState(() {
                                    data = value!;

                                    if(data.compareTo("Select topic")!=0) {
                                      for (int i = 0; i <
                                          videosbyclass.length; i++) {

                                        if(data.compareTo(videosbyclass[i].topic)==0) {
                                          videoLink = videosbyclass[i]
                                              .link;

                                          videoid = videosbyclass[i]
                                              .link.replaceAll(
                                              "https://www.youtube.com/embed/", "");
                                          break;
                                        }







                                      }
                                    }


                                  });
                                },
                                dropdownMenuEntries: dropdowndata.map<DropdownMenuEntry<String>>((String value) {
                                  return DropdownMenuEntry<String>(value: value, label: value);
                                }).toList(),
                              ) ,
                              color: Color(0xffFFD507),
                            )







                         : Container()



                    ),


                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                      child: (videoid.isNotEmpty)? GestureDetector(

                        child: Stack(

                          children: [


                            Align(

                              alignment: FractionalOffset.center,

                              child: Container(

                                  width: double.infinity,
                                  height: (MediaQuery.of(context).size.height) /2.5,

                                  child: Image.network("https://img.youtube.com/vi/"+videoid+"/0.jpg" ,width: double.infinity,height: (MediaQuery.of(context).size.height) /2.6,fit: BoxFit.fill,)
                              ),
                            ),

                            Align(
                                alignment: FractionalOffset.center,

                                child:Container(

                                  width: double.infinity,
                                  height: (MediaQuery.of(context).size.height) /2.5,

                                  child:


                                  Icon(Icons.play_circle,color: Colors.black87,size: ResponsiveInfo.isMobile(context)?50:75,),)

                            )


                          ],


                        ),
                        onTap: () async {



                          var uri = Uri.parse(videoLink);
                          if (await canLaunchUrl(uri)){
                            await launchUrl(uri);
                          } else {
                            // can't launch url
                          }

                        },

                      )






                          : Container(),


                    )



                  ],
                )




            )




          ],

        ),
        width: double.infinity,
        height: double.infinity,
      )





    );
  }




  getClassVideos() async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getVideosbyClass+"?param="+classes[selectedindex]+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);

    VideoByClassEntity entity=VideoByClassEntity.fromJson(js);

    setState(() {
      if(entity.status==1)
      {
        videosbyclass.clear();
        dropdowndata.clear();
        VideoByClassData videoByClassData=new VideoByClassData();
        videoByClassData.subject=data;
        videosbyclass.add(videoByClassData);

        videosbyclass.addAll(entity.data);

         dropdowndata.add(data);

        for(int i=0;i<entity.data.length;i++)
        {
          dropdowndata.add(entity.data[i].topic);

        }


      }


    });





  }





}
