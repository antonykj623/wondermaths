import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';

import 'package:myapp/utils.dart';

class Scene extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 360;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Container(
      width: double.infinity,
      child: Container(
        // androidsmall7e3b (30:189)
        padding: EdgeInsets.fromLTRB(17*fem, 26.45*fem, 17*fem, 41*fem),
        width: double.infinity,
        decoration: BoxDecoration (
          border: Border.all(color: Color(0xff000000)),
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(30*fem),
          boxShadow: [
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
            BoxShadow(
              color: Color(0x3f000000),
              offset: Offset(0*fem, 4*fem),
              blurRadius: 2*fem,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupchpbboP (11KeKnjEiGpB4ux4Mchpb)
              margin: EdgeInsets.fromLTRB(3*fem, 0*fem, 18*fem, 56.55*fem),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // arrowWQZ (30:210)
                    margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 270*fem, 0*fem),
                    width: 18*fem,
                    height: 22*fem,
                    child: Image.asset(
                      'assets/pages/images/arrow.png',
                      width: 18*fem,
                      height: 22*fem,
                    ),
                  ),
                  Container(
                    // menuoPf (30:211)
                    margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 1.61*fem),
                    width: 17*fem,
                    height: 17.3*fem,
                    child: Image.asset(
                      'assets/pages/images/menu-Ptu.png',
                      width: 17*fem,
                      height: 17.3*fem,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupyqdd79T (11Kp51VSQFgbNWd51yQDD)
              margin: EdgeInsets.fromLTRB(8*fem, 0*fem, 6*fem, 21*fem),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // trignometry2nD (30:208)
                    margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 135*fem, 0*fem),
                    child: Text(
                      'Trignometry',
                      style: TextStyle (
                        
                        fontSize: 18*ffem,
                        fontWeight: FontWeight.w600,
                        height: 1.2175*ffem/fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                  Container(
                    // videos5kV (30:212)
                    margin: EdgeInsets.fromLTRB(0*fem, 3*fem, 0*fem, 0*fem),
                    child: Text(
                      '50 Videos',
                      style: TextStyle (
                        
                        fontSize: 12*ffem,
                        fontWeight: FontWeight.w600,
                        height: 1.2175*ffem/fem,
                        color: Color(0x84000000),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // loremipsumdolorsitametconsecte (30:213)
              margin: EdgeInsets.fromLTRB(3*fem, 0*fem, 0*fem, 13*fem),
              constraints: BoxConstraints (
                maxWidth: 319*fem,
              ),
              child: Text(
                'Lorem ipsum dolor sit amet, consectetur adipiscingelit, sed do eiusmod tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\nProbability Lorem ipsum dolor sit amet, consectetur adipiscingelit, sed do eiusmod tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temporProbability',
                style: TextStyle (
                  
                  fontSize: 15*ffem,
                  fontWeight: FontWeight.w600,
                  height: 1.2175*ffem/fem,
                  color: Color(0x84000000),
                ),
              ),
            ),
            Container(
              // lessonsL49 (30:209)
              margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 230*fem, 20*fem),
              child: Text(
                'Lessons',
                style: TextStyle (
                  
                  fontSize: 18*ffem,
                  fontWeight: FontWeight.w600,
                  height: 1.2175*ffem/fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              // frame20e4q (30:190)
              margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 15*fem),
              padding: EdgeInsets.fromLTRB(20*fem, 5*fem, 14*fem, 4*fem),
              width: double.infinity,
              height: 69*fem,
              decoration: BoxDecoration (
                border: Border.all(color: Color(0x30c0b9b9)),
                color: Color(0x2bb6b2b2),
                borderRadius: BorderRadius.circular(10*fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // videoplayer1Wsj (30:192)
                    margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 18*fem, 0*fem),
                    width: 60*fem,
                    height: 60*fem,
                    child: Image.asset(
                      'assets/pages/images/video-player-1.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    // autogroupethh2r5 (11L3EJEGuTYLbsAvzEThH)
                    margin: EdgeInsets.fromLTRB(0*fem, 15*fem, 111*fem, 13*fem),
                    height: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // introduction9vh (30:193)
                          margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 3*fem),
                          child: Text(
                            'Introduction',
                            style: TextStyle (
                              
                              fontSize: 12*ffem,
                              fontWeight: FontWeight.w600,
                              height: 1.2175*ffem/fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                        Text(
                          // p9f (30:194)
                          '02:00',
                          style: TextStyle (
                            
                            fontSize: 11*ffem,
                            fontWeight: FontWeight.w600,
                            height: 1.2175*ffem/fem,
                            color: Color(0x84000000),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // download1k3K (30:195)
                    margin: EdgeInsets.fromLTRB(0*fem, 3*fem, 0*fem, 0*fem),
                    width: 25*fem,
                    height: 25*fem,
                    child: Image.asset(
                      'assets/pages/images/download-1-oRK.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // frame21Fkm (30:196)
              margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 11*fem),
              padding: EdgeInsets.fromLTRB(20*fem, 5*fem, 14*fem, 4*fem),
              width: double.infinity,
              height: 69*fem,
              decoration: BoxDecoration (
                border: Border.all(color: Color(0x30c0b9b9)),
                color: Color(0x2bb6b2b2),
                borderRadius: BorderRadius.circular(10*fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // videoplayer1u4d (30:198)
                    margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 18*fem, 0*fem),
                    width: 60*fem,
                    height: 60*fem,
                    child: Image.asset(
                      'assets/pages/images/video-player-1-Q6m.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    // autogroupfvascUq (11LG4GByaCfiBSR6nFvas)
                    margin: EdgeInsets.fromLTRB(0*fem, 15*fem, 64*fem, 13*fem),
                    height: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // whatisprobabilityYNV (30:199)
                          margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 3*fem),
                          child: Text(
                            'What is probability?',
                            style: TextStyle (
                              
                              fontSize: 12*ffem,
                              fontWeight: FontWeight.w600,
                              height: 1.2175*ffem/fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                        Text(
                          // FGu (30:200)
                          '04:30 ',
                          style: TextStyle (
                            
                            fontSize: 11*ffem,
                            fontWeight: FontWeight.w600,
                            height: 1.2175*ffem/fem,
                            color: Color(0x84000000),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // download1apy (30:201)
                    margin: EdgeInsets.fromLTRB(0*fem, 3*fem, 0*fem, 0*fem),
                    width: 25*fem,
                    height: 25*fem,
                    child: Image.asset(
                      'assets/pages/images/download-1-rho.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // frame22ucM (30:202)
              padding: EdgeInsets.fromLTRB(20*fem, 5*fem, 14*fem, 4*fem),
              width: double.infinity,
              height: 69*fem,
              decoration: BoxDecoration (
                border: Border.all(color: Color(0x30c0b9b9)),
                color: Color(0x2bb6b2b2),
                borderRadius: BorderRadius.circular(10*fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // videoplayer1nw3 (30:204)
                    margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 18*fem, 0*fem),
                    width: 60*fem,
                    height: 60*fem,
                    child: Image.asset(
                      'assets/pages/images/video-player-1-WHP.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    // autogroup5patKRB (11LSU8WSw6zsnbhVo5pAT)
                    margin: EdgeInsets.fromLTRB(0*fem, 15*fem, 39*fem, 13*fem),
                    height: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // trignometricfunctions3ry (30:205)
                          margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 0*fem, 3*fem),
                          child: Text(
                            'Trignometric functions?',
                            style: TextStyle (
                              
                              fontSize: 12*ffem,
                              fontWeight: FontWeight.w600,
                              height: 1.2175*ffem/fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                        Text(
                          // xj3 (30:206)
                          '06:00 ',
                          style: TextStyle (
                            
                            fontSize: 11*ffem,
                            fontWeight: FontWeight.w600,
                            height: 1.2175*ffem/fem,
                            color: Color(0x84000000),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // download1VDB (30:207)
                    margin: EdgeInsets.fromLTRB(0*fem, 3*fem, 0*fem, 0*fem),
                    width: 25*fem,
                    height: 25*fem,
                    child: Image.asset(
                      'assets/page-1/images/download-1-pMP.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
          );
  }
}