import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/pdf_topic_group_entity.dart';
import '../network/apiservices.dart';

class PdfTopics extends StatefulWidget {

  String type;
  int class_index=0;
  String topicid="0";


   PdfTopics(this.type,this.class_index,this.topicid) ;

  @override
  _PdfTopicsState createState() => _PdfTopicsState(this.type,this.class_index,this.topicid);
}

class _PdfTopicsState extends State<PdfTopics> {


  String type;
  int class_index=0;
  String topicid="0";


  _PdfTopicsState(this.type,this.class_index,this.topicid);

  List<String>classes=["5","6","7","8","9","10"];
  List<PdfTopicGroupData> pdfdata=[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPdfTopicDetails();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Topics",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Colors.black),),
        centerTitle: true,




      ),

      body: Stack(

        children: [


          Align(

            alignment: FractionalOffset.topCenter,

            child:   Align(


              alignment: FractionalOffset.topCenter,

              child: Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                child: ListView.builder(
                    itemCount: pdfdata.length,
                    shrinkWrap: true,
                    primary: false,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(

                        child:Card(

                          child: ListTile(

                              trailing:  Icon(Icons.arrow_forward_ios_rounded,size: ResponsiveInfo.isMobile(context)?25:35,),
                              title:Text(pdfdata[index].chapter,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?15:18),) ),
                          elevation: 8,
                        ) ,

                        onTap: () async {



                          var date = new DateTime.now()
                              .toIso8601String();


                          String urldata = AppLiterals.questionbakdataurl+pdfdata[index].fileName;

                          Directory? _path = await getExternalStorageDirectory();
                          String _localPath = _path!.path +
                              "/" +
                              date.toString() + ".pdf";
                          final savedDir = File(
                              _localPath);
                          bool hasExisted = await savedDir
                              .exists();
                          if (!hasExisted) {
                            savedDir.create();
                          }


                          String path = _localPath;

                          var response = await http.get(
                              Uri.parse(urldata));

                          File file2 = new File(
                              path); // <-- 2
                          file2.writeAsBytesSync(
                              response.bodyBytes);

                          bool fileExists = await file2
                              .exists();

                          if (fileExists) {


                            OpenFile.open(file2.path).then((value) {
                              // File f = File(file2.path);
                              // f.delete();
                            });



                          }






                        },


                      )




                      ;
                    }),


              )


            ),

          )



        ],


      ),


    );
  }

  getPdfTopicDetails() async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getPdfByTopicIdAndClass+"?param1="+classes[class_index]+"&param2="+type+"&param3="+topicid+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);

    PdfTopicGroupEntity entity=PdfTopicGroupEntity.fromJson(js);

    if(entity.status==1)
    {


      setState(() {

        pdfdata.clear();
        pdfdata.addAll(entity.data);


      });



    }





  }
}
