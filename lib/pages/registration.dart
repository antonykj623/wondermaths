import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';

import 'package:myapp/constants/BaseMethodes.dart';
import 'package:myapp/pages/verify_o_t_p.dart';
import 'package:myapp/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../network/apiservices.dart';
import 'Homepage.dart';

class Registration extends StatefulWidget {


  Registration();

  @override
  _RegistrationState createState() => _RegistrationState();

}


class _RegistrationState extends State<Registration> {

  TextEditingController namecontroller=new TextEditingController();


  TextEditingController emailcontroller=new TextEditingController();


  TextEditingController mobilecontroller=new TextEditingController();


  TextEditingController sponsermobilecontroller=new TextEditingController();



  TextEditingController passwordcontroller=new TextEditingController();


  TextEditingController confirmpasswordcontroller=new TextEditingController();

  bool istermsconditionchecked=false;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();


  }


  @override
  Widget build(BuildContext context) {
    double baseWidth = 360;
    double fem = MediaQuery
        .of(context)
        .size
        .width / baseWidth;
    double ffem = fem * 0.97;
    return new Scaffold(

      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black87,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Registration",style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)?13:16),),
        centerTitle: false,

      ),

      body: Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(

              color: Colors.black87

            ),
          ),
          SingleChildScrollView(
            child: Column(

                children: <Widget>[



                  Padding(
                    padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                    // padding: EdgeInsets.all(15),
                    child: Container(

    decoration: BoxDecoration (
    border: Border.all(color: Color(0xfcc0b9b9)),
    color: Color(0xfff5f5f5),
    borderRadius: BorderRadius.circular(10*fem),
    ),

    child:  new Theme(data: new ThemeData(
      hintColor: Colors.black38,

    ), child: TextField(

      controller: namecontroller,


      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white70, width: 0.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        hintText: 'Name',



      ),

      onChanged: (text) {


      },


    )),

    )



                   ,
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                    // padding: EdgeInsets.all(15),
                    child: Container(
                      decoration: BoxDecoration (
                        border: Border.all(color: Color(0xfcc0b9b9)),
                        color: Color(0xfff5f5f5),
                        borderRadius: BorderRadius.circular(10*fem),
                      ),

    child:new Theme(data: new ThemeData(
        hintColor: Colors.black38
    ), child: TextField(
      controller: emailcontroller,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        hintText: 'Email',
      ),

      onChanged: (text) {


      },
    )) ,

    )




                    ,
                  ),

                  Padding(padding: EdgeInsets.all(15),
                    child: Row(

                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,

                      children: [

                        Expanded(child: Text("Country",style: TextStyle(fontSize: 14,color: Colors.white70),),flex: 1,),
                        Expanded(child: Text(" : ",style: TextStyle(fontSize: 14,color: Colors.white70),),flex: 1,),
                        Expanded(child: Text("India",style: TextStyle(fontSize: 14,color: Colors.white70),),flex: 1,)

                      ],
                    ),


                  ),



                  //   Padding(
                  //       padding: const EdgeInsets.all(15),
                  //       child:Container(
                  //         width: double.infinity,
                  //         height: 60.0,
                  //         decoration: BoxDecoration(
                  //           border: Border.all(
                  //             color: Colors.white,
                  //             // red as border color
                  //           ),
                  //         ),
                  //         child: DropdownButtonHideUnderline(
                  //           child: ButtonTheme(
                  //             alignedDropdown: true,
                  //             child: InputDecorator(
                  //               decoration: const InputDecoration(border: OutlineInputBorder()),
                  //               child: DropdownButtonHideUnderline(
                  //                 child: DropdownButton(
                  //
                  //                   value: dropdownValue,
                  //                   items: countr_data
                  //                       .map<DropdownMenuItem<String>>((String value) {
                  //                     return DropdownMenuItem<String>(
                  //                       value: value,
                  //                       child: Text(value),
                  //                     );
                  //                   }).toList(),
                  //                   onChanged: (String? newValue) {
                  //                     setState(() {
                  //                       dropdownValue = newValue!;
                  //
                  //                       DataConnectionStatus.check().then((value)
                  //                       {
                  //
                  //                         if (value != null && value) {
                  //                           // Internet Present Case
                  //
                  //                           getState(dropdownValue);
                  //
                  //
                  //                         }
                  //                         else{
                  //
                  //                           ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  //                             content: Text("Check your internet connection"),
                  //                           ));
                  //
                  //                         }
                  //                       }
                  //                       );
                  //
                  //
                  //
                  //                     });
                  //
                  //                   },
                  //                 //  value: dropdownValue,
                  //                   style: Theme.of(context).textTheme.bodyText1,
                  //
                  //                 ),
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //       )),
                  //
                  //
                  // isstateVisible?  Padding(
                  //       padding: const EdgeInsets.all(15),
                  //       child:Container(
                  //         width: double.infinity,
                  //         height: 60.0,
                  //         decoration: BoxDecoration(
                  //           border: Border.all(
                  //             color: Colors.white,
                  //             // red as border color
                  //           ),
                  //         ),
                  //         child: DropdownButtonHideUnderline(
                  //
                  //           child: ButtonTheme(
                  //             alignedDropdown: true,
                  //             child: InputDecorator(
                  //               decoration: const InputDecoration(border: OutlineInputBorder()),
                  //               child: DropdownButtonHideUnderline(
                  //                 child: DropdownButton(
                  //
                  //                   isExpanded: true,
                  //                   value: dropdownstate,
                  //                   items: statedata_data
                  //                       .map<DropdownMenuItem<String>>((String value) {
                  //                     return DropdownMenuItem<String>(
                  //                       value: value,
                  //                       child: Text(value),
                  //                     );
                  //                   }).toList(),
                  //                   onChanged: (String? newValue) {
                  //                     setState(() {
                  //                       dropdownstate = newValue!;
                  //
                  //                       checkState(dropdownstate);
                  //
                  //
                  //                     });
                  //                   },
                  //                   style: Theme.of(context).textTheme.bodyText1,
                  //
                  //                 ),
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //       )):new Container(),

                  Padding(
                    padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                    // padding: EdgeInsets.all(15),
                    child: Container(
                      decoration: BoxDecoration (
                        border: Border.all(color: Color(0xfcc0b9b9)),
                        color: Color(0xfff5f5f5),
                        borderRadius: BorderRadius.circular(10*fem),
                      ),

    child: new Theme(data: new ThemeData(
        hintColor: Colors.black38
    ), child: TextField(
      controller: mobilecontroller,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        hintText: 'Mobile number',
      ),
      onChanged: (text) {


      },
    )),
    )




                    ,
                  ),

                  // Padding(
                  //     padding: const EdgeInsets.all(15),
                  //     child:Text("Do you have promotional coupon code ?",style: TextStyle(fontSize: 14,color: Colors.white),)),
                  //
                  // (regval==1) ? Padding(
                  //     padding: const EdgeInsets.all(15),
                  //     child:Text("Please enter a promotional 'coupon code' and avail a discount of Rs. 499/- at the time of activation",style: TextStyle(fontSize: 14,color: Colors.white),)):Container(),


                  // Padding(padding:  const EdgeInsets.all(15),
                  //
                  // child: Column(
                  //   children: [
                  //     ListTile(
                  //       title: const Text('Yes'),
                  //       leading: Radio(
                  //         value: 1,
                  //         groupValue: regval,
                  //         onChanged: (var  value) {
                  //           setState(() {
                  //             ispromotionalcoupon=true;
                  //             regval = int.parse(value.toString());
                  //           });
                  //         },
                  //       ),
                  //     ),
                  //
                  //
                  //     ListTile(
                  //       title: const Text('No'),
                  //       leading: Radio(
                  //         value: 0,
                  //         groupValue: regval,
                  //         onChanged: (var value) {
                  //           setState(() {
                  //             ispromotionalcoupon=false;
                  //             regval = int.parse(value.toString());
                  //             sponsermobilenumber="";
                  //             sponserregCode="";
                  //           });
                  //         },
                  //       ),
                  //     ),
                  //
                  //   ],
                  //
                  // ) ,
                  // ),






                  //    (ispromotionalcoupon)?Padding(
                  //      padding: const EdgeInsets.all(15),
                  //
                  //      //padding: EdgeInsets.symmetric(horizontal: 15),
                  //      child: Row(
                  //  textDirection: TextDirection.rtl,
                  //  children: <Widget>[
                  //  TextButton(onPressed: () {
                  //
                  //    DataConnectionStatus.check().then((value)
                  //    {
                  //
                  //      if (value != null && value) {
                  //        // Internet Present Case
                  //
                  //        getSponser(sponsermobilecontroller.value.text.trim());
                  //
                  //
                  //      }
                  //      else{
                  //
                  //        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  //          content: Text("Check your internet connection"),
                  //        ));
                  //
                  //      }
                  //    }
                  //    );
                  //
                  //
                  //
                  //
                  //
                  //
                  //  }, child: Text("Submit"),
                  //
                  //  ),
                  //
                  //
                  //
                  //
                  //
                  //  Expanded(child: new Theme(data: new ThemeData(
                  //      hintColor: Colors.white
                  //  ), child: TextField(
                  //    controller: sponsermobilecontroller,
                  //    keyboardType: TextInputType.text,
                  //    decoration: InputDecoration(
                  //      focusedBorder: OutlineInputBorder(
                  //        borderSide: BorderSide(color: Colors.white, width: 0.5),
                  //      ),
                  //      enabledBorder: OutlineInputBorder(
                  //        borderSide: BorderSide(color: Colors.white, width: 0.5),
                  //      ),
                  //      hintText: 'Enter promotional coupon code',
                  //    ),
                  //
                  //    onChanged: (text) {
                  //      TemRegData.sponsermobilenumber=text;
                  //
                  //    },
                  //  )))
                  //  ],
                  //  ),
                  //    ):Container(),
                  //
                  //
                  // isSponserVisible?   Padding(
                  //      padding: const EdgeInsets.all(20),
                  //
                  //      //padding: EdgeInsets.symmetric(horizontal: 15),
                  //      child: Row(
                  //        textDirection: TextDirection.ltr,
                  //        children: <Widget>[
                  //          Text('Name' ,style: new TextStyle(color: Colors.white),
                  //          ),
                  //          Expanded(child: new Theme(data: new ThemeData(
                  //              hintColor: Colors.white
                  //          ), child: Text( ' :   '+sponsermobilenumber,style: new TextStyle(color: Colors.white),
                  //
                  //          )))
                  //        ],
                  //      ),
                  //    ):new Container(),
                  //
                  //    isSponserVisible?    Padding(
                  //      padding: const EdgeInsets.all(20),
                  //
                  //      //padding: EdgeInsets.symmetric(horizontal: 15),
                  //      child: Row(
                  //        textDirection: TextDirection.ltr,
                  //        children: <Widget>[
                  //          Text('Coupon Code' ,style: new TextStyle(color: Colors.white),
                  //          ),
                  //          Expanded(child: new Theme(data: new ThemeData(
                  //              hintColor: Colors.white
                  //          ), child: Text( ' :   '+sponserregCode,style: new TextStyle(color: Colors.white),
                  //
                  //          )))
                  //        ],
                  //      ),
                  //    ):new Container(),






                  Padding(
                    padding: const EdgeInsets.all(15),
                    //padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Container(

                    decoration: BoxDecoration (
                    border: Border.all(color: Color(0xfcc0b9b9)),
    color: Color(0xfff5f5f5),
    borderRadius: BorderRadius.circular(10*fem),
    ),

    child:  new Theme(data: new ThemeData(
        hintColor: Colors.black38
    ), child: TextField(
      controller: passwordcontroller,
      obscureText: true,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black38, width: 0.5),
        ),
        hintText: 'Password',
      ),
      onChanged: (text) {


      },
    )),
                    )




                  ),

                  Padding(
                    padding: const EdgeInsets.all(15),
                    //padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Container(

                      child:  new Theme(data: new ThemeData(
    hintColor: Colors.black38
    ), child: TextField(
    controller: confirmpasswordcontroller,
    obscureText: true,
    decoration: InputDecoration(
    focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black38, width: 0.5),
    ),
    enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black38, width: 0.5),
    ),
    hintText: 'Confirm password',
    ),
    onChanged: (text) {


    }
    )),

                    decoration: BoxDecoration (
                    border: Border.all(color: Color(0xfcc0b9b9)),
    color: Color(0xfff5f5f5),
    borderRadius: BorderRadius.circular(10*fem),
    ),


                    )





                  ),







                  // Padding(
                  //     padding: const EdgeInsets.all(15),
                  //     child:Container(
                  //       width: double.infinity,
                  //       height: 60.0,
                  //       decoration: BoxDecoration(
                  //         border: Border.all(
                  //           color: Colors.white,
                  //           // red as border color
                  //         ),
                  //       ),
                  //       child: DropdownButtonHideUnderline(
                  //         child: ButtonTheme(
                  //           alignedDropdown: true,
                  //           child: InputDecorator(
                  //             decoration: const InputDecoration(border: OutlineInputBorder()),
                  //             child: DropdownButtonHideUnderline(
                  //               child: DropdownButton(
                  //
                  //                 value: languagedropdown,
                  //                 items: DataConstants.arrofLanguages
                  //                     .map<DropdownMenuItem<String>>((String value) {
                  //                   return DropdownMenuItem<String>(
                  //                     value: value,
                  //                     child: Text(value),
                  //                   );
                  //                 }).toList(),
                  //                 onChanged: (String? newValue) {
                  //                   setState(() {
                  //                     languagedropdown = newValue!;
                  //
                  //                     TemRegData.language=languagedropdown;
                  //                   });
                  //                 },
                  //                 style: Theme.of(context).textTheme.bodyText1,
                  //
                  //               ),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     )),





                  Padding(
                    padding: const EdgeInsets.all(20),

                    //padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      textDirection: TextDirection.ltr,
                      children: <Widget>[
                        Checkbox(
                          checkColor: Colors.yellow,
                          // checkColor: Colors.white10,
                           activeColor: Colors.white,
                          fillColor: MaterialStateProperty.resolveWith(getColor),

                          value: istermsconditionchecked, onChanged: (bool? value) {

                          setState(() {
                            istermsconditionchecked=value!;


                          });

                        },
                        )
                        ,
                        Expanded(child: new Theme(data: new ThemeData(
                            hintColor: Colors.black38
                        ), child: Text( 'I agree your terms and conditions ',style: new TextStyle(color: Colors.white70),

                        )))
                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(15),
                    child :   Container(
                      height: 50,
                      width: 150,
                      decoration: BoxDecoration(
                          color: Color(0xffFFD507), borderRadius: BorderRadius.circular(10)),
                      child: TextButton(
                        onPressed: () {
                          // Navigator.push(
                          //     context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

                          // TemRegData trmg=new TemRegData(namecontroller.value.text,emailcontroller.value.text
                          // ,dropdownValue,dropdownstate,mobilecontroller.value.text,sponsermobilecontroller.value.text,passwordcontroller.value.text,languagedropdown);



                          checkAllDatas(namecontroller.value.text,emailcontroller.value.text,"1","0",mobilecontroller.value.text,
                              "","",passwordcontroller.value.text,confirmpasswordcontroller.value.text,"",istermsconditionchecked );




                        },
                        child: Text(
                          'Submit',
                          style: TextStyle(color: Colors.black87, fontSize: 15),
                        ),
                      ),
                    ),),


                  Padding(
                      padding: const EdgeInsets.all(15),

                      child:  TextButton(
                        style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.black38),
                        ),
                        onPressed: () {

                          Navigator.of(context).pop();
                        },
                        child: Text('Already a member ? Login',style: TextStyle(color: Colors.white70),),
                      ))



                ]






            ),
          ),
        ],
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.selected,
      MaterialState.focused,
      MaterialState.pressed,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.orange;
    }
    return Colors.yellow;
  }


  checkMobileNumberExists() async
  {


    //
    // final preferenceDataStorage = await SharedPreferences
    //     .getInstance();
    //
    // String? classid= preferenceDataStorage.getString(
    //     AppLiterals.Classid);
    //
    //
    // var date = new DateTime.now().toIso8601String();
    //
    // ApiServices apiServices=new ApiServices();
    //
    // String url = AppLiterals.baseurl + WebServiceMethodes.checkUserExists;
    //
    //
    // Map mp = new HashMap();
    //
    // mp['mobile'] = mobilecontroller.text;
    // mp['timestamp']=date;
    //
    //     classid.toString();
    //
    // String? response = await apiServices.postmethod(context,  mp, url
    // );
    // if(response.toString().isNotEmpty) {
    //
    //   var jsondata = jsonDecode(response.toString());
    //
    //   if(jsondata['status']!=1)
    //   {
    //
    //     Navigator.push(context,
    //         MaterialPageRoute(
    //             builder: (context) =>
    //                 VerifyOTP(
    //                     namecontroller.text.toString(),
    //                     mobilecontroller.text.toString(),
    //                     "",
    //                     emailcontroller.text.toString(),
    //                     passwordcontroller
    //                         .text.toString())));
    //   }
    //   else{
    //
    //     ScaffoldMessenger.of(context).showSnackBar(
    //       SnackBar(
    //         backgroundColor: Colors.black,
    //         content: Text("Mobile number is already exists",style: TextStyle(fontFamily: 'Poppins',color: Colors.white)),
    //       ),
    //     );
    //   }
    //
    //
    //
    //
    //
    //
    //
    // }



  }


  checkAllDatas(String name,String email,String countryid,String state,String mobilenumber,String spregid,String regcode,String password,String confirmpassword,String language,bool ischecked){


    if(name.isNotEmpty)
    {
      if(email.isNotEmpty)
      {

        if(BaseMethodes.isValidEmail(email)) {
          if (countryid.compareTo("0") != 0) {
            if (mobilenumber.isNotEmpty) {
              // if (sponserregCode.isNotEmpty && spregid.isNotEmpty) {
              if (password.isNotEmpty) {
                if (password.compareTo(confirmpassword) == 0) {

                  if (istermsconditionchecked) {


                    checkMobileNumber(mobilenumber);





                  }
                  else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                          "Please check your terms and conditions"),
                    ));
                  }

                }
                else {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(" Password confirmation failed"),
                  ));
                }
              }
              else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Enter password"),
                ));
              }
              // }
              // else {
              //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              //     content: Text("Select sponser"),
              //   ));
              // }
            }
            else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("Enter mobile number"),
              ));
            }
          }
          else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Select country"),
            ));
          }
        }
        else{

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter a valid email"),
          ));
        }



      }
      else{


        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Enter the email"),
        ));
      }
    }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Enter the name"),
      ));
    }




    // Navigator.push(
    //     context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

  }

  checkMobileNumber(String mobilenumber ) async
  {

    ApiServices apiServices=new ApiServices();
    var date = new DateTime.now().toIso8601String();

    String url=AppLiterals.saveapp_baseurl+WebServiceMethodes.getUserByMobile+'?mobile='+mobilenumber+'&timestamp='+date;

    String? response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    dynamic jsondata = jsonDecode(response);


    int status = jsondata['status'];

    if(status==0)
    {

      // Navigator.push(
      //     context, MaterialPageRoute(builder: (_) => VerifyOTP()));

      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) =>
                  VerifyOTP(
                      namecontroller.text.toString(),
                      mobilecontroller.text.toString(),
                      "",
                      emailcontroller.text.toString(),
                      passwordcontroller
                          .text.toString())));


    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("This mobile number  exist"),
      ));
    }




  }

}


