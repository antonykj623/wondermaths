import 'dart:collection';
import 'dart:convert';

import 'package:android_id/android_id.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:intl/intl.dart';
import 'package:myapp/design/ResponsiveInfo.dart';
import 'package:myapp/domain/profile_data_entity.dart';
import 'package:myapp/domain/wonder_maths_user_entity.dart';
import 'dart:ui';

import 'package:myapp/pages/Homepage.dart';
import 'package:myapp/pages/class_list.dart';
import 'package:myapp/pages/registration.dart';
import 'package:myapp/pages/verify_o_t_p.dart';
import 'package:myapp/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/AppLiterals.dart';
import '../constants/ServerMessageType.dart';
import '../constants/WebMethodes.dart';
import '../domain/member_data_entity.dart';
import '../network/apiservices.dart';

class Login extends StatefulWidget {


  Login();

  @override
  _LoginState createState() => _LoginState();

}


class _LoginState extends State<Login> {

  TextEditingController mobilecontroller=new TextEditingController();
  TextEditingController passwordcontroller=new TextEditingController();
  TextEditingController forgotpasswordmobilecontroller=new TextEditingController();



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    double baseWidth = 0;
    double fem = 1;
    double ffem = fem * 0.97;
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.black87,

        child: Card(
          color: Colors.black87,

          child:    Stack(


            children: [

              Align(

                alignment: FractionalOffset.topCenter,
                child:  Container(
                  width: double.infinity,
                    height: double.infinity,

                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/pages/images/learning-pic.png"),
                        fit: BoxFit.cover,
                      ),
          )




    ),
              ),

              Align(
                alignment: FractionalOffset.topCenter,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Colors.black87.withOpacity(0.9),
              ),
              ),

              Align(

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      // learningpicBeR (4:21)
                      margin: EdgeInsets.fromLTRB(0*fem, 150*fem, 9*fem, 14*fem),


                      child: Card(

    child: Padding(

    padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),



                      child :Image.asset("assets/pages/images/icondata.png",width: ResponsiveInfo.isSmallMobile(context)
                          ? ResponsiveInfo.isMobile(context) ? 80 :100:130,height: ResponsiveInfo.isSmallMobile(context)
                          ? ResponsiveInfo.isMobile(context) ? 80 :100:130,fit: BoxFit.fill,),))
                    ),
                    Container(
                      // frame1uaR (3:13)
                      margin: EdgeInsets.fromLTRB(15*fem, 0*fem, 15*fem, 29*fem),
                      padding: EdgeInsets.fromLTRB(31*fem, 0, 0, 0),
                      width: double.infinity,

                      decoration: BoxDecoration (
                        border: Border.all(color: Color(0xfcc0b9b9)),
                        color: Color(0xfff5f5f5),
                        borderRadius: BorderRadius.circular(10*fem),
                      ),
                      child: TextField(
                        // emailX61 (3:11)
                        controller: mobilecontroller,
                        keyboardType: TextInputType.phone,

                        decoration: InputDecoration(
                            border: InputBorder.none,
                            labelText: 'Mobile number',
                            hintText: 'Mobile number'
                        ),

                        style: TextStyle (


                          fontSize: 13*ffem,
                          fontWeight: FontWeight.w600,

                          color: Color(0x82000000),


                        ),
                      ),
                    ),

                    Container(
                      // frame1uaR (3:13)
                      margin: EdgeInsets.fromLTRB(15*fem, 0*fem, 15*fem, 29*fem),
                      padding: EdgeInsets.fromLTRB(31*fem, 0, 0, 0),
                      width: double.infinity,

                      decoration: BoxDecoration (
                        border: Border.all(color: Color(0xfcc0b9b9)),
                        color: Color(0xfff5f5f5),
                        borderRadius: BorderRadius.circular(10*fem),
                      ),
                      child: TextField(
                        // emailX61 (3:11)
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        controller: passwordcontroller,

                        decoration: InputDecoration(
                            border: InputBorder.none,
                            labelText: 'Password',
                            hintText: 'Password'

                        ),

                        style: TextStyle (


                          fontSize: 13*ffem,
                          fontWeight: FontWeight.w600,

                          color: Color(0x82000000),


                        ),
                      ),
                    ),







                    Container(
                      // forgotpasswordyMK (4:18)
                        margin: EdgeInsets.fromLTRB(150*fem, 0*fem, 0*fem, 30*fem),
                        child: TextButton(

                          onPressed: () {
                            forgotpasswordmobilecontroller.text="";

                            Dialog forgotpasswordDialog = Dialog(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                              child: Container(
                                height: 300.0,
                                width: 500.0,

                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding:  EdgeInsets.all(15.0),
                                      child: new Theme(data: new ThemeData(
                                          hintColor: Colors.black38
                                      ), child: TextField(
                                        controller: forgotpasswordmobilecontroller,
                                        keyboardType: TextInputType.number,

                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.black38, width: 0.5),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.black38, width: 0.5),
                                          ),
                                          hintText: 'Enter the mobile number',


                                        ),
                                      )),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(15.0),

                                      child: Container(

                                        width: 150,
                                        height: 55,
                                        decoration: BoxDecoration(

                                            color: Color(0xffFFD507), borderRadius: BorderRadius.circular(10)),
                                        child:Align(
                                          alignment: Alignment.center,
                                          child: TextButton(

                                            onPressed:() {


                                              if(forgotpasswordmobilecontroller.text.toString().isNotEmpty) {
                                                Navigator.of(
                                                    context, rootNavigator: true)
                                                    .pop();

                                                checkMobileNumber(forgotpasswordmobilecontroller.text.toString());



                                              }
                                              else{



                                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                                  content: Text("Please enter the mobile number"),
                                                ));
                                              }


                                            },

                                            child: Text('Submit', style: TextStyle(color: Colors.black87) ,) ,),
                                        ),



                                        //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                      ),


                                      // ,
                                    ),
                                    // Padding(padding: EdgeInsets.only(top: 50.0)),
                                    // TextButton(onPressed: () {
                                    //   Navigator.of(context).pop();
                                    // },
                                    //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                                  ],
                                ),
                              ),
                            );



                            showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);






                          },
                          child:  Text(
                            'Forgot password?',
                            style: TextStyle (

                              fontSize: 13*ffem,
                              fontWeight: FontWeight.w600,
                              height: 1.2175*ffem/fem,
                              color: Colors.white,
                            ),
                          ),
                        )



                    ),



                    Container(
                      // frame3sxV (3:17)
                        margin: EdgeInsets.fromLTRB(81*fem, 0*fem, 96*fem, 34*fem),
                        width: double.infinity,
                        height: 45*fem,
                        decoration: BoxDecoration (
                          border: Border.all(color: Color(0xfcFFD507)),
                          color: Color(0xffFFD507),
                          borderRadius: BorderRadius.circular(10*fem),
                        ),
                        child: GestureDetector(

                          child: Center(
                            child: Text(
                              'Login',
                              style: TextStyle (

                                fontSize: 15*ffem,
                                fontWeight: FontWeight.w700,
                                height: 1.2175*ffem/fem,
                                color: Colors.black87,
                              ),
                            ),
                          ),
                          onTap: (){
                            // Navigator.push(context,MaterialPageRoute(builder:(context) => Homepage()));

                            if(mobilecontroller.text.isNotEmpty)
                            {

                              if(passwordcontroller.text.isNotEmpty)
                              {


                                userLogin();
                              }
                              else{


                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text("Please enter your password"),
                                ));
                              }
                            }
                            else{


                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Please enter your mobile number"),
                              ));
                            }






                          },
                        )



                    ),



                    Container(
                      // donthaveanaccountsignup8dX (4:19)
                      margin: EdgeInsets.fromLTRB(0*fem, 0*fem, 12*fem, 0*fem),
                      child: RichText(
                        text: TextSpan(
                          style: TextStyle (

                            fontSize: 13*ffem,
                            fontWeight: FontWeight.w600,
                            height: 1.2175*ffem/fem,
                            color: Color(0xff000000),
                          ),
                          children: [
                            TextSpan(
                              text: 'Don’t have an account?',

                              style: TextStyle (

                                fontSize: 13*ffem,
                                fontWeight: FontWeight.w600,
                                height: 1.2175*ffem/fem,
                                color: Colors.white,
                              ),
                            ),
                            TextSpan(
                              text: ' ',
                            ),
                            TextSpan(
                              text: 'Sign Up',
                              recognizer: TapGestureRecognizer()..onTap = () {

                                Navigator.push(context,MaterialPageRoute(builder:(context) => Registration()));

                              },
                              style: TextStyle (

                                fontSize: 13*ffem,
                                fontWeight: FontWeight.w600,
                                height: 1.2175*ffem/fem,
                                color: Color(0xffFFD507),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                alignment: FractionalOffset.topCenter,
              )



            ],





          ),
        )




          );
  }



  checkMobileNumber(String mobilenumber ) async
  {

    ApiServices apiServices=new ApiServices();
    var date = new DateTime.now().toIso8601String();

    String url=AppLiterals.saveapp_baseurl+WebServiceMethodes.getUserByMobile+'?mobile='+mobilenumber+'&timestamp='+date;

    String? response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    dynamic jsondata = jsonDecode(response);


    int status = jsondata['status'];

    if(status==1)
    {



      // Navigator.push(
      //     context, MaterialPageRoute(builder: (_) => VerifyOTP()));

      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) =>
                  VerifyOTP(
                      "",
                    mobilenumber,
                      "",
                      "",
                      passwordcontroller
                          .text.toString())));


    }
    else{

      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        headerAnimationLoop: false,
        animType: AnimType.RIGHSLIDE,
        showCloseIcon: true,
        closeIcon: const Icon(Icons.close_fullscreen_outlined),
        title: 'Wonder Maths',
        desc: 'This mobile number not  exists',
        btnOkOnPress: () async {

        },

        btnOkColor: Colors.blue,
        onDissmissCallback: (type) {
          // debugPrint('Dialog Dissmiss from callback $type');
        },
      ).show();


      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text("This mobile number not  exists"),
      // ));
    }




  }







userLogin()async
{
  final preferenceDataStorage = await SharedPreferences
      .getInstance();

  String? classid= preferenceDataStorage.getString(
      AppLiterals.Classid);


  var date = new DateTime.now().toIso8601String();

  ApiServices apiServices=new ApiServices();

  String url = AppLiterals.saveapp_baseurl + WebServiceMethodes.UserLoginSave+"?timestamp="+date.toString();


  Map mp = new HashMap();
  mp['password'] = passwordcontroller.text;
  mp['mobile'] = mobilecontroller.text;
  mp['uuid'] = date;
  mp['timestamp'] = date;


  String? response = await apiServices.postmethod(context,  mp, url
  );

  print(response.toString());
  // final preferenceDataStorage = await SharedPreferences
  //     .getInstance();

if(response.toString().isNotEmpty) {

var jsondata = jsonDecode(response.toString());

if(jsondata['status']!=0)
{

String token=jsondata['token'];


preferenceDataStorage.setString(AppLiterals.Tokenkey, token);



// checkWonderMathsUser();
checkDeviceId(mobilecontroller.text);



}
else{


  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text("Login Failed"),
  ));
}







}

}




checkDeviceId(String mobilenumber)async
{
  ApiServices apiServices=new ApiServices();
  var date = new DateTime.now().toIso8601String();

  String url=AppLiterals.saveapp_baseurl+WebServiceMethodes.getUserByMobile+'?mobile='+mobilenumber+'&timestamp='+date;

  String? response = await apiServices.getmethod(context,  null, url
  );

  print(response);

  dynamic jsondata = jsonDecode(response);
  WonderMathsUserEntity mathsUserEntity=WonderMathsUserEntity.fromJson(jsondata);
  if(mathsUserEntity.status==1)
    {

      if(mathsUserEntity.data!=null)
        {

          // String deviceid=mathsUserEntity.data.wDeviceId;
          const androidId=AndroidId();
          String? deviceId = await androidId.getId();


          if(mathsUserEntity.data.wPlatform!=null && mathsUserEntity.data.wPlatform.isNotEmpty) {
            if (mathsUserEntity.data.wPlatform.compareTo("Android") == 0) {
              if (mathsUserEntity.data.wDeviceId.compareTo(
                  deviceId.toString()) == 0) {
                checkWonderMathsUser();
              }
              else {
                AwesomeDialog(
                  context: context,
                  dialogType: DialogType.INFO,
                  headerAnimationLoop: false,
                  animType: AnimType.RIGHSLIDE,
                  showCloseIcon: true,
                  closeIcon: const Icon(Icons.close_fullscreen_outlined),
                  title: 'Wonder Maths',
                  desc: 'You already logged in other device',
                  btnOkOnPress: () async {

                  },

                  btnOkColor: Colors.blue,
                  onDissmissCallback: (type) {
                    // debugPrint('Dialog Dissmiss from callback $type');
                  },
                ).show();
              }
            }
          }
          else{

          checkWonderMathsUser();

          }




        }





    }
  else{
    AwesomeDialog(
      context: context,
      dialogType: DialogType.INFO,
      headerAnimationLoop: false,
      animType: AnimType.RIGHSLIDE,
      showCloseIcon: true,
      closeIcon: const Icon(Icons.close_fullscreen_outlined),
      title: 'Wonder Maths',
      desc: 'User Authentication failed',
      btnOkOnPress: () async {

      },

      btnOkColor: Colors.blue,
      onDissmissCallback: (type) {
        // debugPrint('Dialog Dissmiss from callback $type');
      },
    ).show();

  }


}








checkWonderMathsUser()
async {
  final preferenceDataStorage = await SharedPreferences
      .getInstance();

 String? token= preferenceDataStorage.getString(AppLiterals.Tokenkey);

  var date = new DateTime.now().toIso8601String();

  ApiServices apiServices=new ApiServices();

  String url = AppLiterals.saveapp_baseurl + WebServiceMethodes.getMemberDetailsByID+"?timestamp="+date.toString();

  String? response = await apiServices.getmethod(context,  null, url
  );

  print(response);

  try {
    var js=jsonDecode(response.toString());

    MemberDataEntity entity=MemberDataEntity.fromJson(js);
    preferenceDataStorage.setBool(AppLiterals.activationperiodexpire, true);
    if(entity.status==1)
    {

      if(entity.message!=null)
      {


        if(entity.message.actvationProduct.compareTo(AppLiterals.productname)==0)
        {

          String date=entity.message.activationDate;

          DateTime accountsdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);

          int y=accountsdateparsed.year+1;

       DateTime  expirydate = DateTime(y, accountsdateparsed.month, accountsdateparsed.day );



          DateTime dtnow=new DateTime.now();

          if(expirydate.isBefore(dtnow))
          {

            preferenceDataStorage.setBool(AppLiterals.activationperiodexpire, true);
          }
          else{
            preferenceDataStorage.setBool(AppLiterals.activationperiodexpire, false);

          }


          Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) =>
                      Homepage()));

        }
        else{

          AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            headerAnimationLoop: false,
            animType: AnimType.RIGHSLIDE,
            showCloseIcon: true,
            closeIcon: const Icon(Icons.close_fullscreen_outlined),
            title: 'Wonder Maths',
            desc: 'Login failed',
            btnOkOnPress: () async {

            },

            btnOkColor: Colors.blue,
            onDissmissCallback: (type) {
              // debugPrint('Dialog Dissmiss from callback $type');
            },
          ).show();
        }



      }
      else{

        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          headerAnimationLoop: false,
          animType: AnimType.RIGHSLIDE,
          showCloseIcon: true,
          closeIcon: const Icon(Icons.close_fullscreen_outlined),
          title: 'Wonder Maths',
          desc: 'Login failed',
          btnOkOnPress: () async {

          },

          btnOkColor: Colors.blue,
          onDissmissCallback: (type) {
            // debugPrint('Dialog Dissmiss from callback $type');
          },
        ).show();
      }


    }
    else{

      getProfileData();

    }



  } catch(e) {
    // code that handles the exception


  }
  
  

  

}


  getProfileData() async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices = new ApiServices();

    String url = AppLiterals.
    saveapp_baseurl + WebServiceMethodes.getUserDetails + "?timestamp=" +
        date.toString();

    String? response = await apiServices.postmethod(context, null, url
    );
    print(response.toString());
    var js = jsonDecode(response.toString());

    ProfileDataEntity profileDataEntity = ProfileDataEntity.fromJson(js);

    if (profileDataEntity.status == 1) {


      String date=profileDataEntity.data.activationDate;

      DateTime accountsdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
      DateTime dtnow=new DateTime.now();

      if(accountsdateparsed.isBefore(dtnow))
      {

        datacount.setBool(AppLiterals.activationperiodexpire, true);
      }
      else{
        datacount.setBool(AppLiterals.activationperiodexpire, false);



      }
      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) =>
                  Homepage()));

      // setState(() {
      //   profiledata = profileDataEntity.data.fullName + "\n" +
      //       profileDataEntity.data.mobile;
      //   profileimage = profileDataEntity.data.profileImage;
      // });
    }
  }


}