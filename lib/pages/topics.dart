import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';

import 'package:myapp/pages/SubTopics.dart';
import 'package:myapp/utils.dart';

import '../design/ResponsiveInfo.dart';

class Topics extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 360;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        toolbarHeight:ResponsiveInfo.isMobile(context)? 50:70 ,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),


        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("8th standard topics",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 17:22,color: Colors.black),),
        centerTitle: false,








      ),

      body:  Stack(

        children: [


          Align(

            alignment: FractionalOffset.topCenter,


            child: ListView.builder(

                itemCount: 10,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(padding: EdgeInsets.all(10),

                      child: GestureDetector(

                        child: Card(
                          elevation: 5,

                          child: Padding(

                            padding: EdgeInsets.all(10),
                            child: ListTile(
                                leading:  Padding(padding: EdgeInsets.all(10),

                                  child: Icon(Icons.topic,size: 25, color: Colors.yellow,),

                                ),
                                trailing: Icon(Icons.keyboard_arrow_right,size: 25, color: Colors.black38,),
                                subtitle:  Text(
                                  " Lorem ipsum tekdjn",
                                  style: TextStyle(color: Colors.black38, fontSize: 13),
                                ),
                                title: Text(
                                  "Topic $index",
                                  style: TextStyle(color: Colors.black, fontSize: 13),
                                )),
                          )



                          ,
                        ),

                        onTap: (){

                          Navigator.push(context,MaterialPageRoute(builder:(context) => Subtopics()));

                        },
                      )




                  )







                    ;
                }
            ),
          )







        ],
      ),

    );
  }
}