import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';

import 'package:myapp/utils.dart';

import '../design/ResponsiveInfo.dart';

class SubtopicDescription extends StatelessWidget {








  @override
  Widget build(BuildContext context) {
    double baseWidth = 360;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        toolbarHeight:ResponsiveInfo.isMobile(context)? 50:70,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),


        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Subtopic",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 17:22,color: Colors.black),),
        centerTitle: false,








      ),

      body:  SingleChildScrollView(

        child: Column(


          children: [


            Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:14),


            child: Text(

              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", style: TextStyle(fontSize: 13),maxLines: 10,overflow: TextOverflow.ellipsis,

            ) ,


            ),

            Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


              child: ListView.builder(

                  itemCount: 10,
                  primary: false,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(padding: EdgeInsets.all(10),

                        child: GestureDetector(

                          child: Card(
                            elevation: 5,

                            child: Padding(

                              padding: EdgeInsets.all(10),
                              child: ListTile(
                                leading:  Padding(padding: EdgeInsets.all(10),

                                  child: Image.asset("assets/pages/images/youtube.png",width: ResponsiveInfo.isMobile(context)?40:55, height:  ResponsiveInfo.isMobile(context)?40:55,fit: BoxFit.fill,),

                                ),
                                subtitle:  Text(
                                  " Lorem ipsum tekdjn kilok kkmkm rewnnt ",
                                  style: TextStyle(color: Colors.black38, fontSize: 13),
                                ),
                                title: Text(
                                  "Subtopic $index",
                                  style: TextStyle(color: Colors.black, fontSize: 13),
                                ),

                                trailing: Icon(Icons.keyboard_arrow_right,size: 25, color: Colors.black38,),

                              ),
                            )



                            ,
                          ),

                          onTap: (){

                            Navigator.push(context,MaterialPageRoute(builder:(context) => SubtopicDescription()));

                          },
                        )




                    )







                    ;
                  }
              ),



            )









          ],

        ),


      ),

    );
  }
}