import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myapp/constants/AppLiterals.dart';
import 'package:myapp/constants/WebMethodes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../design/ResponsiveInfo.dart';
import '../domain/profile_data_entity.dart';
import '../network/apiservices.dart';

class Profile extends StatefulWidget {
   Profile() ;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {


  String profileimage="",mobile="",email="";

  TextEditingController namecontrller=new TextEditingController();
  TextEditingController emailcontroller=new TextEditingController();



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfile();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(

    appBar: AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text("Profile",style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?13:16),),
      centerTitle: false,

    ),

body: SingleChildScrollView(
    child: Column(children: <Widget>[
      Align(
        alignment: FractionalOffset.topCenter,

        child: Padding(
            padding: EdgeInsets.all(2),
            child: Container(

              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?120:150:180,
              width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 120:150:180,

              child:  Stack(children: [

                Align(
                    alignment: FractionalOffset.bottomCenter,

                    child:

                    Container(
                        height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?100:130:160,
                        width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 100:130:160,

                        child:CircleAvatar(
                          backgroundImage: NetworkImage(
                              profileimage,
                              scale: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?100:130:160


                          ),



                          // child: Image.network(
                          //   DataConstants.profileimgbaseurl+profileimage,
                          //   fit: BoxFit.fill,
                          //   errorBuilder: (context, url, error) => new Icon(Icons.account_circle_rounded,size: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?90:120:160,),
                          // ),


                        )

                      // child:Image.asset("images/user.png")
                    ) ),



                Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(

                      padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(45, 0, 0, 0) : EdgeInsets.fromLTRB(60, 0, 0, 0):EdgeInsets.fromLTRB(80, 0, 0, 0),

                      child: FloatingActionButton(
                        onPressed: () async {

                          Widget yesButton = TextButton(
                              child: Text("Yes",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
                              onPressed: () async {

                                Navigator.pop(context);

                                final ImagePicker _picker = ImagePicker();
                                // Pick an image
                                final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

                                // print(image!.path) ;

                                uploadImage(new File(image!.path) );

                                // if(image!=null) {
                                //
                                //
                                //   CroppedFile? croppedFile = await ImageCropper()
                                //       .cropImage(
                                //     sourcePath: image.path,
                                //     aspectRatioPresets: [
                                //       CropAspectRatioPreset.square,
                                //       CropAspectRatioPreset.ratio3x2,
                                //       CropAspectRatioPreset.original,
                                //       CropAspectRatioPreset.ratio4x3,
                                //       CropAspectRatioPreset.ratio16x9
                                //     ],
                                //     // androidUiSettings: AndroidUiSettings(
                                //     //     toolbarTitle: 'Cropper',
                                //     //     toolbarColor: Colors.deepOrange,
                                //     //     toolbarWidgetColor: Colors.white,
                                //     //     initAspectRatio: CropAspectRatioPreset
                                //     //         .original,
                                //     //     lockAspectRatio: false),
                                //     // iosUiSettings: IOSUiSettings(
                                //     //   minimumAspectRatio: 1.0,
                                //     // )
                                //   );
                                //
                                //
                                //
                                // }


                              });



                          Widget noButton = TextButton(
                            child: Text("No",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          );

                          // set up the AlertDialog
                          AlertDialog alert = AlertDialog(
                            title: Text("Wonder maths",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                            content: Text("We will keep your profile photo secure, We do not going to share outside.Do you want to continue ?",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                            actions: [yesButton, noButton],
                          );

                          // show the dialog
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return alert;
                            },
                          );







                        },
                        child: Icon(
                          Icons.edit,
                          color: Colors.white,
                          size: 29,
                        ),
                        backgroundColor: Color(0xffbfefcc),
                        tooltip: 'Capture Picture',
                        elevation: 5,
                        splashColor: Colors.grey,
                      ),
                    )



                )
              ]),
            )




        ),





      ),

      Padding(
        padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18) ,

        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          textDirection: TextDirection.ltr,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment:
          MainAxisAlignment.center,
          //Center Row contents horizontally,
          crossAxisAlignment:
          CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child:
            Text("Mobile Number :  " ,style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),

            ),flex:1),
            Expanded(child: Text("  "+mobile,style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19))
              ,flex: 1,)
          ],
        ),
      ),

      Padding(
        padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18),
        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.black
        ), child: TextField(

          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 0.5),
            ),
            hintText: "Name",

          ),
          onChanged: (text) {


          },
          controller: namecontrller,
        )),
      ),

      // Padding(
      //     padding: const EdgeInsets.all(15),
      //     child:Container(
      //       width: double.infinity,
      //       height: 60.0,
      //       decoration: BoxDecoration(
      //         border: Border.all(
      //           color: Colors.white,
      //           // red as border color
      //         ),
      //       ),
      //       child: DropdownButtonHideUnderline(
      //         child: ButtonTheme(
      //           alignedDropdown: true,
      //           child: InputDecorator(
      //             decoration: const InputDecoration(border: OutlineInputBorder()),
      //             child: DropdownButtonHideUnderline(
      //               child: DropdownButton(
      //
      //                 value: dropdownValue,
      //                 items: countr_data
      //                     .map<DropdownMenuItem<String>>((String value) {
      //                   return DropdownMenuItem<String>(
      //                     value: value,
      //                     child: Text(value),
      //                   );
      //                 }).toList(),
      //                 onChanged: (String? newValue) {
      //                   setState(() {
      //                     dropdownValue = newValue!;
      //
      //                     DataConnectionStatus.check().then((value)
      //                     {
      //
      //                       if (value != null && value) {
      //                         // Internet Present Case
      //
      //                         getState(dropdownValue);
      //
      //
      //                       }
      //                       else{
      //
      //                         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //                           content: Text("Check your internet connection"),
      //                         ));
      //
      //                       }
      //                     }
      //                    );
      //
      //
      //
      //                   });
      //
      //                 },
      //                 //  value: dropdownValue,
      //                 style: Theme.of(context).textTheme.bodyText1,
      //
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //     )),
      //
      //
      //
      // isstateVisible?  Padding(
      //     padding: const EdgeInsets.all(15),
      //     child:Container(
      //       width: double.infinity,
      //       height: 60.0,
      //       decoration: BoxDecoration(
      //         border: Border.all(
      //           color: Colors.white,
      //           // red as border color
      //         ),
      //       ),
      //       child: DropdownButtonHideUnderline(
      //
      //         child: ButtonTheme(
      //           alignedDropdown: true,
      //           child: InputDecorator(
      //             decoration: const InputDecoration(border: OutlineInputBorder()),
      //             child: DropdownButtonHideUnderline(
      //               child: DropdownButton(
      //
      //                 isExpanded: true,
      //                 value: dropdownstate,
      //                 items: statedata_data
      //                     .map<DropdownMenuItem<String>>((String value) {
      //                   return DropdownMenuItem<String>(
      //                     value: value,
      //                     child: Text(value),
      //                   );
      //                 }).toList(),
      //                 onChanged: (String? newValue) {
      //                   setState(() {
      //                     dropdownstate = newValue!;
      //
      //                     checkState(dropdownstate);
      //
      //
      //                   });
      //                 },
      //                 style: Theme.of(context).textTheme.bodyText1,
      //
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //     )):new Container(),























      Padding(
        padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:10.0,right: 10.0,top:10,bottom: 0):EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 0):EdgeInsets.only(left:18.0,right: 18.0,top:18,bottom: 0),
        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.black
        ), child: TextField(
          controller: emailcontroller,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 0.5),
            ),
            hintText: "Email",
          ),

          onChanged: (text) {


          },



        )),
      ),

      // Padding(
      //     padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18),
      //     child:Container(
      //       width: double.infinity,
      //       height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?   60.0:70:90,
      //       decoration: BoxDecoration(
      //         border: Border.all(
      //           color: Colors.white,
      //           // red as border color
      //         ),
      //       ),
      //       child: DropdownButtonHideUnderline(
      //         child: ButtonTheme(
      //           alignedDropdown: true,
      //           child: InputDecorator(
      //             decoration: const InputDecoration(border: OutlineInputBorder()),
      //             child: DropdownButtonHideUnderline(
      //               child: DropdownButton(
      //
      //                 value: languagedropdown,
      //                 items: DataConstants.arrofLanguages
      //                     .map<DropdownMenuItem<String>>((String value) {
      //                   return DropdownMenuItem<String>(
      //                     value: value,
      //                     child: Text(value),
      //                   );
      //                 }).toList(),
      //                 onChanged: (String? item) async{
      //
      //                   String languagedata="";
      //
      //                   if(item.toString().compareTo("తెలుగు")==0)
      //                   {
      //                     languagedata="te";
      //                   }
      //                   else if(item.toString().compareTo("English")==0)
      //                   {
      //                     languagedata="en";
      //                   }
      //
      //                   else if(item.toString().compareTo("मराठी")==0)
      //                   {
      //                     languagedata="mr";
      //                   }
      //                   else if(item.toString().compareTo("தமிழ்")==0)
      //                   {
      //                     languagedata="ta";
      //                   }
      //
      //                   else   if(item.toString().compareTo("ಕನ್ನಡ")==0)
      //                   {
      //                     languagedata="kn";
      //                   }
      //
      //                   else if(item.toString().compareTo("हिंदी")==0)
      //                   {
      //                     languagedata="hi";
      //                   }
      //
      //
      //                   else if(item.toString().compareTo("മലയാളം")==0)
      //                   {
      //                     languagedata="ml";
      //                   }
      //
      //                   else
      //                   {
      //                     languagedata="ar";
      //                   }
      //
      //                   final datacount = await SharedPreferences.getInstance();
      //                   datacount.setString(LanguageSections.lan, languagedata);
      //
      //                   setState(() {
      //                     languagedropdown = item!.toString();
      //
      //                     checkLanguage();
      //                     TemRegData.language=languagedropdown;
      //                   });
      //                 },
      //                 style: Theme.of(context).textTheme.bodyText1,
      //
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //     )),


      Padding(
        padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18),
        child :   Container(
          height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?50:70:90,
          width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 150:180:250,
          decoration: BoxDecoration(
              color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
          child: TextButton(
            onPressed: () {


              updateProfile(emailcontroller.text,"1","0",namecontrller.text);

            },
            child: Text(
              "Submit",
              style: TextStyle(color: Colors.white, fontSize: 15),
            ),
          ),
        ),),




    ])),



    );
  }

  uploadImage(File? file) async {
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");


    final datacount = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(AppLiterals.saveapp_baseurl+WebServiceMethodes.uploadUserProfile));
    request.headers.addAll( { "Authorization": datacount.getString(AppLiterals.Tokenkey)!,
      'Content-Type': 'application/x-www-form-urlencoded'});


    request.files.add(
        http.MultipartFile.fromBytes(
            'file',
            File(file!.path).readAsBytesSync(),
            filename: file.path.split("/").last
        )
    );

    // _progressDialog.dismissProgressDialog(context);
    var res = await request.send();
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    getProfile();

    print(responseString);

  }

  getProfile()
  async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    saveapp_baseurl+ WebServiceMethodes.getUserDetails+"?timestamp="+date.toString();

    String? response = await apiServices.postmethod(context,  null, url
    );
    print(response.toString());
    var js=jsonDecode(response.toString());

    ProfileDataEntity profileDataEntity=ProfileDataEntity.fromJson(js);

    if(profileDataEntity.status==1)
    {
      setState(() {

        namecontrller.text=profileDataEntity.data.fullName;
        emailcontroller.text=profileDataEntity.data.emailId;
        mobile=profileDataEntity.data.mobile;
        profileimage= AppLiterals.profileimgbaseurl+ profileDataEntity.data.profileImage;
      });
    }


  }

  updateProfile(String email,String countryid,String stateid,String name) async
  {


    final datacount = await SharedPreferences.getInstance();



    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    saveapp_baseurl+ WebServiceMethodes.updateProfile+"?timestamp="+date.toString();

    Map m=new Map();
    m['name']=name;
    m['user_email']=email;
    m['stateid']=stateid;
    m['language']="English";
    m['country_id']=countryid;
    m['timestamp']=date;




    String? response = await apiServices.postmethod(context,  m, url
    );
    print(response.toString());



    print(response);

    var json = jsonDecode(response.toString());

    if (json['status'] == 1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Profile updated successfully"),
      ));

      // Navigator.of(context).pop();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("failed"),
      ));
    }
  }


}
