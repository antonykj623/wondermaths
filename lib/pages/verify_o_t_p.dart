import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:myapp/design/ResponsiveInfo.dart';
import 'package:myapp/pages/Homepage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/AppLiterals.dart';
import '../constants/ServerMessageType.dart';
import '../constants/WebMethodes.dart';
import '../network/apiservices.dart';
import 'package:http/http.dart' as http;

import 'change_password.dart';

class VerifyOTP extends StatefulWidget {

  String name="",mobile="",schoolname="",email="",password="";



   VerifyOTP(this.name,this.mobile,this.schoolname,this.email,this.password) ;

  @override
  _VerifyOTPState createState() => _VerifyOTPState(this.name,this.mobile,this.schoolname,this.email,this.password);
}

class _VerifyOTPState extends State<VerifyOTP> {

  String name,mobile,schoolname,email,password;

  String otpcode="";



  _VerifyOTPState(this.name,this.mobile,this.schoolname,this.email,this.password);


  TextEditingController otpcodecontroller=new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    sendOtpCode(mobile);
  }

  @override
  Widget build(BuildContext context) {
    return      new Scaffold(
        backgroundColor: Colors.black12,
        resizeToAvoidBottomInset: false,

        appBar: AppBar(
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_new, color: Colors.white70),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Colors.black87,
          elevation: 0,
          title: Text("OTP Verification", style: TextStyle(
              fontSize: ResponsiveInfo.isMobile(context) ? 14 : 18,
              color: Colors.white70),),
          centerTitle: true,


        ),



        body:Container(



          color: Colors.black87,
          child: Column(

            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

              // SingleChildScrollView(
              // child: Column(
              //
              // children: <Widget>[

          Padding(
          padding:   EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),

            child:  Text(
                'Please enter the OTP code that sent to your mobile number '+mobile,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white70,
                    fontWeight: FontWeight.normal,


                    fontSize: 14),





              ),),

      // Padding(
      //   padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
      //
      //       child:ColorFiltered(
      //           colorFilter:
      //           ColorFilter.mode(Colors.yellow.withOpacity(0.7), BlendMode.color),
      //
      //
      //    child:   Image.asset('assets/pages/images/otp.png',width: ResponsiveInfo.isMobile(context)?100:130,height: ResponsiveInfo.isMobile(context)?100:130,)
      //
      //       )
      //
      // ),



              Padding(
                padding:  EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                // padding: EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration (
                    border: Border.all(color: Color(0xfcc0b9b9)),
                    color: Color(0xfff5f5f5),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: new Theme(data: new ThemeData(
                      hintColor: Colors.black26
                  ), child: TextField(
                    controller: otpcodecontroller,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black26, width: 0.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black26, width: 0.5),
                      ),
                      hintText: 'OTP Code',
                    ),
                  )),
                )


                ,
              ),

              Padding(
                padding: ResponsiveInfo.isMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:20,bottom: 20)
                    : EdgeInsets.only(left:20.0,right: 20.0,top:30,bottom: 30),
                // padding: EdgeInsets.all(15),
                child: TextButton(
                  onPressed: () {  sendOtpCode( mobile); },
                  child : Text('Did not get OTP ? Resend',
                    style: TextStyle(
                        color: Colors.white70,
                        fontWeight: FontWeight.normal,
                        fontSize: 14),),


                ),
              ),

              Padding(
                padding:  EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 10),
                child :   Container(
                  height: 50,
                  width: 150,
                  decoration: BoxDecoration(
                      color: Color(0xffFFD507), borderRadius: BorderRadius.circular(10)),
                  child: TextButton(
                    onPressed: () {

                      if(otpcodecontroller.value.text.isNotEmpty)
                      {

                        if(otpcodecontroller.value.text.compareTo(otpcode.toString())==0)
                        {

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("OTP Verification Successful"),
                          ));


                          if(name.isNotEmpty) {
                            postDataForRegistration();
                          }
                          else{

                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ChangePassword(

                                          mobile,
                                        )));


                          }





                        }

                        else{
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Incorrect otp code"),
                          ));

                        }



                      }
                      else{
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Please enter your otp code"),
                        ));

                      }





                    },
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.black87, fontSize: 15),
                    ),
                  ),
                ),),

              // Padding(
              //   padding:  EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 10),
              //   child :   Text(otpcode),)




              // ]
              // ),
              // ),
            ],
          ),
        ));;
  }

  Future<void> sendOtpCode(String mobile) async {

    var rng = new Random();
    var code = rng.nextInt(9000) + 1000;

    setState(() {

      otpcode=code.toString();
    });

    print("OTP code : "+otpcode);

    print("mobile number : "+mobile);


    var date = new DateTime.now().toIso8601String();


    String message=AppLiterals.buildServerMessage(ServerMessageType.forgot_password,code.toString(),"","");

    String u=message.replaceAll(" ", "%20");


    print('the message is '+u);

    String url=AppLiterals.smsbaseurl+WebServiceMethodes.smsMethode+"?token="+AppLiterals.apikey+
        "&sender="+AppLiterals.sender+
        "&number="+mobile+
        "&route="+AppLiterals.route+
        "&type="+AppLiterals.type+
        "&sms="+u+"&templateid="+AppLiterals.forgotpasstemplateid;

    print(mobile);



    var dataasync = await http.get(
      Uri.parse(url),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },



    );


    String response = dataasync.body;

    print("Response : "+response);



  }



  postDataForRegistration() async
  {

  var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.saveapp_baseurl + WebServiceMethodes.userAuthenticate+"?timestamp="+date.toString();

  Map mp = new HashMap();
    mp['name'] = name;
    mp['mobile'] = mobile;

    mp['email'] =
    email;
    mp['password'] =
    password;
  mp['uuid'] =
  date;
  mp['timestamp'] =
  date;
  mp['country_id'] ="1";
  mp['sp_reg_code'] ="";
  mp['sp_reg_id'] ="";
  mp['stateid'] ="0";
  mp['language']="en";

 String? response = await apiServices.postmethod(context,  mp, url
    );
    if(response.toString().isNotEmpty) {

      var jsondata = jsonDecode(response.toString());

      if(jsondata['status']==1)
      {

        String token=jsondata['token'];

        final preferenceDataStorage = await SharedPreferences
            .getInstance();

        preferenceDataStorage.setString(AppLiterals.Tokenkey, token);


        Navigator.push(context,
            MaterialPageRoute(
                builder: (context) =>
                    Homepage()));
      }







    }



  }




}
