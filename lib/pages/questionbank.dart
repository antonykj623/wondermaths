import 'dart:convert';

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/aptitude_topics_entity.dart';
import '../domain/speed_maths_data_entity.dart';
import '../network/apiservices.dart';
import 'package:http/http.dart' as http;

class Questionbank extends StatefulWidget {
   Questionbank() ;

  @override
  _QuestionbankState createState() => _QuestionbankState();
}

class _QuestionbankState extends State<Questionbank> {

  List<String>sylabustype=["Middle School","High School"];

  List<String>testtype=["Aptitude Test","Mental Ability Test"];

  String aptitudefirstvalue="Select Subject";
  String mentalfirstvalue="Select Mental Ability Test";

  String test="";


  List<String>aptitudetests=["Select Aptitude Test","Test 1","Test 2"];
  List<String>mentalabilitytests=["Select Mental Ability Test","Test 1","Test 2"];

  List<String>classes=["Basic Level","Medium Level","Advanced Level"];

  int selectedindex=0,selectedtestindex=0,selectedclassindex=0;

  List<SpeedMathsDataData>speddmathsdata=[];

  List<String>subjects=[];

  List<AptitudeTopicsData> topicsdata=[];

  late AptitudeTopicsData selectedvalue;

  List<AptitudeTopicsData>topics_sorted=[];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMentalabilityTestData();
    getAptitudeTestData();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Color(0xffFFD507)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.black87,
        elevation: 0,
        title: Text("Question Bank",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Color(0xffFFD507)),),
        centerTitle: true,




      ),

      body: Container(

        child: Stack(


          children: [

            Align(

                alignment: FractionalOffset.topCenter,

                child: SingleChildScrollView(

                  child:    Column(

                    children: [




                      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                        child: SizedBox(
                          width: double.infinity,
                          height: ResponsiveInfo.isMobile(context)? 60 : 90 ,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,

                            children: [


                              Expanded(child: GestureDetector(

                                child: Column(

                                  children: [

                                    Text(
                                      "Aptitude Test" ,
                                      style:  TextStyle(
                                          color:(selectedtestindex==0)?  Color(0xffFFD507) : Colors.white70,
                                          fontFamily: 'poppins'),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: ResponsiveInfo.isMobile(context)?10:15),

                                      child: Container(
                                        width: double.infinity,
                                        height: ResponsiveInfo.isMobile(context)?1:2,
                                        color: (selectedtestindex==0)?  Color(0xffFFD507) : Colors.black,

                                      ),

                                    )

                                  ],


                                ),

                                onTap: (){

                                  setState(() {

                                    selectedtestindex=0;
                                  });
                                },
                              )


                                ,flex: 1,),

                              Expanded(child: GestureDetector(

                                child: Column(

                                  children: [

                                    Text(
                                      "Mental Ability Test" ,
                                      style:  TextStyle(
                                          color:(selectedtestindex==1)?  Color(0xffFFD507): Colors.white70,
                                          fontFamily: 'poppins'),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: ResponsiveInfo.isMobile(context)?10:15),

                                      child: Container(
                                        width: double.infinity,
                                        height: ResponsiveInfo.isMobile(context)?1:2,
                                        color: (selectedtestindex==1)?  Color(0xffFFD507) : Colors.black,

                                      ),

                                    )

                                  ],


                                ),

                                onTap: (){

                                  setState(() {

                                    selectedtestindex=1;
                                  });
                                },


                              )



                                ,flex: 1,)



                            ],

                          ),


                        ),

                      ),






                      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                        child: (selectedtestindex==0)? Stack(
                          children: [

                            Align(

                                alignment: FractionalOffset.topCenter,

                                child: (subjects.length>0)?
                                Container(

                                  child:

                                DropdownMenu<String>(
                                  initialSelection:  subjects.first ,
                                  onSelected: (String? value) {
                                    // This is called when the user selects an item.
                                    setState(() {
                                      aptitudefirstvalue = value!;

                                      if(aptitudefirstvalue.compareTo("Select Subject")!=0) {
                                        for (int i = 0; i <
                                            topicsdata.length; i++) {
                                          String subj = topicsdata[i]
                                              .subject;
                                          if (subj.compareTo(
                                              aptitudefirstvalue) == 0) {
                                            selectedvalue = topicsdata[i];
                                            getSubjectListByTopicId(selectedvalue.topic);

                                            break;
                                          }
                                        }
                                      }

                                      // getBudgetData();






                                    });
                                  },
                                  dropdownMenuEntries: subjects.map<DropdownMenuEntry<String>>((String value) {
                                    return DropdownMenuEntry<String>(value: value, label: value);
                                  }).toList(),
                                ) ,
                                  color:  Color(0xffFFD507)): Container()








                              // Container(
                              //   height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,
                              //
                              //   decoration: BoxDecoration(
                              //       border: Border.all(color: Colors.black38)),
                              //
                              //   child:  DropdownButtonHideUnderline(
                              //
                              //     child: ButtonTheme(
                              //       alignedDropdown: true,
                              //       child: InputDecorator(
                              //         decoration: const InputDecoration(border: OutlineInputBorder()),
                              //         child: DropdownButtonHideUnderline(
                              //           child: DropdownButton(
                              //
                              //             isExpanded: true,
                              //             value: aptitudefirstvalue,
                              //             items:  subjects
                              //                 .map<DropdownMenuItem<String>>((String value) {
                              //               return DropdownMenuItem<String>(
                              //                 value: value,
                              //                 child: Text(value),
                              //               );
                              //             }).toList() ,
                              //             onChanged: (String? newValue) {
                              //               setState(() {
                              //                 aptitudefirstvalue = newValue!;
                              //
                              //                 if(aptitudefirstvalue.compareTo("Select Subject")!=0) {
                              //                   for (int i = 0; i <
                              //                       topicsdata.length; i++) {
                              //                     String subj = topicsdata[i]
                              //                         .subject;
                              //                     if (subj.compareTo(
                              //                         aptitudefirstvalue) == 0) {
                              //                       selectedvalue = topicsdata[i];
                              //                       getSubjectListByTopicId(selectedvalue.topic);
                              //
                              //                       break;
                              //                     }
                              //                   }
                              //                 }
                              //
                              //                 // getBudgetData();
                              //
                              //
                              //
                              //
                              //
                              //
                              //               });
                              //             },
                              //             style: Theme.of(context).textTheme.bodyText1,
                              //
                              //           ),
                              //         ),
                              //       ),
                              //     ),
                              //   ),
                              //
                              //
                              // ),
                            ),

                            Align(

                              alignment: FractionalOffset.topCenter,

                              child: Padding(padding: EdgeInsets.only(top: ResponsiveInfo.isMobile(context)?60:90),


                                child:ListView.builder(
                                    itemCount: topics_sorted.length,
                                    shrinkWrap: true,
                                    primary: false,
                                    itemBuilder: (BuildContext context, int index) {
                                      return Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                                      child:Card(

                                        child: Container(

                                          child: ListTile(
                                              leading:  Icon(Icons.picture_as_pdf,size: ResponsiveInfo.isMobile(context)?25:35,color: Color(0xffFFD507),),
                                              trailing:  TextButton(

                                                onPressed: () async {


                                                  // PermissionStatus ispermissionaccepted=
                                                  //
                                                  // await Permission.manageExternalStorage.request();
                                                  //
                                                  // if(ispermissionaccepted.isGranted) {



                                                  var date = new DateTime.now()
                                                      .microsecond.toString();


                                                  String urldata = AppLiterals
                                                      .questionbakdataurl +
                                                      topics_sorted[index].fileName;

                                                  Directory? _path = await getExternalStorageDirectory();
                                                  String _localPath = _path!.path +
                                                      "/" +
                                                      date.toString() + ".pdf";
                                                  final savedDir = File(
                                                      _localPath);
                                                  bool hasExisted = await savedDir
                                                      .exists();
                                                  if (!hasExisted) {
                                                    savedDir.create();
                                                  }


                                                  String path = _localPath;

                                                  var response = await http.get(
                                                      Uri.parse(urldata));

                                                  File file2 = new File(
                                                      path); // <-- 2
                                                  file2.writeAsBytesSync(
                                                      response.bodyBytes);

                                                  bool fileExists = await file2
                                                      .exists();

                                                  if (fileExists) {


                                                    OpenFile.open(file2.path).then((value) {
                                                      // File f = File(file2.path);
                                                      // f.delete();
                                                    });



                                                  }




                                                },
                                                child: Text(
                                                  "View",
                                                  style: TextStyle(color: Color(0xffFFD507), fontSize:ResponsiveInfo.isMobile(context)? 15:17),
                                                ),
                                              ),



                                              title: Text(topics_sorted[index].chapter, style: TextStyle(color: Colors.white70, fontSize:ResponsiveInfo.isMobile(context)? 14:16))),

                                          decoration: BoxDecoration(
                                            // Box decoration takes a gradient
                                              gradient: LinearGradient(
                                                // Where the linear gradient begins and ends
                                                begin: Alignment.topCenter,
                                                end: Alignment.bottomCenter,
                                                // Add one stop for each color. Stops should increase from 0 to 1
                                                stops: [0.1, 0.3, 0.6, 0.9],
                                                colors: [
                                                  // Colors are easy thanks to Flutter's Colors class.
                                                  Colors.black26,
                                                  Colors.black45,
                                                  Colors.black54,
                                                  Colors.black,
                                                ],
                                              )),
                                        ),



                                        elevation: 8,
                                      ) ,

                                      )




                                      ;
                                    }) ,


                              ),
                            )

                          ],
                        )


                            : GridView.builder(
                          itemCount: speddmathsdata.length,
                          primary: false,
                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                          itemBuilder: (BuildContext context, int index) {
                            return  GestureDetector(

                              child:Padding(

                                child:  Card(
                                    child: Container(

                                      width: ResponsiveInfo.isMobile(context)?120:160,
                                      height: ResponsiveInfo.isMobile(context)?150:200,
                                      decoration: BoxDecoration(
                                        // Box decoration takes a gradient
                                          gradient: LinearGradient(
                                            // Where the linear gradient begins and ends
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            // Add one stop for each color. Stops should increase from 0 to 1
                                            stops: [0.1, 0.3, 0.6, 0.9],
                                            colors: [
                                              // Colors are easy thanks to Flutter's Colors class.
                                              Colors.black26,
                                              Colors.black45,
                                              Colors.black54,
                                              Colors.black,
                                            ],
                                          )),

                                      child:   Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,

                                        children: [

                                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                                            child:  Icon(Icons.picture_as_pdf,size: ResponsiveInfo.isMobile(context)?30:45,color:Color(0xffFFD507),),
                                          ),

                                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                                            child:


                                            Text(speddmathsdata[index].name.replaceAll(".pdf", ""),textAlign : TextAlign.center,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?12:15,color: Colors.white70),),
                                          )


                                        ],



                                        //just for testing, will fill with image later
                                      ),

                                    )




                                ) ,
                                padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),
                              ),




                              onTap: () async {


                                // var uri = Uri.parse(AppLiterals.mentalabilitytestdataurl+speddmathsdata[index].name);
                                // if (await canLaunchUrl(uri)){
                                //   await launchUrl(uri);
                                // } else {
                                //   // can't launch url
                                // }



                                var date = new DateTime.now()
                                    .toIso8601String();


                                String urldata = AppLiterals.mentalabilitytestdataurl+speddmathsdata[index].name;

                                Directory? _path = await getExternalStorageDirectory();
                                String _localPath = _path!.path +
                                    "/" +
                                    date.toString() + ".pdf";
                                final savedDir = File(
                                    _localPath);
                                bool hasExisted = await savedDir
                                    .exists();
                                if (!hasExisted) {
                                  savedDir.create();
                                }


                                String path = _localPath;

                                var response = await http.get(
                                    Uri.parse(urldata));

                                File file2 = new File(
                                    path); // <-- 2
                                file2.writeAsBytesSync(
                                    response.bodyBytes);

                                bool fileExists = await file2
                                    .exists();

                                if (fileExists) {


                                  OpenFile.open(file2.path).then((value) {
                                    // File f = File(file2.path);
                                    // f.delete();
                                  });



                                }





                              },
                            )


                            ;
                          },
                        ),


                      )








                    ],



                  ) ,
                )





            )



          ],


        ),
        color: Colors.black87,
        height: double.infinity,
        width: double.infinity,
      )







    );
  }


  getMentalabilityTestData()async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getMentaAbilityFiles+"?timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );
    print(response);
    var js=jsonDecode(response);
    SpeedMathsDataEntity entity=SpeedMathsDataEntity.fromJson(js);


    if(entity.status==1)
    {
      List<SpeedMathsDataData> spmaths=[];
      List<SpeedMathsDataData> spmaths_sorted=[];

      if(entity.data.length>0)
      {

        for(int i=0;i<entity.data.length;i++)
        {
          if(entity.data[i].name.compareTo(".")==0 || entity.data[i].name.compareTo("..")==0 ){
            entity.data.removeAt(i);
          }
          else {
            spmaths.add(entity.data[i]);
          }
        }

        spmaths.sort((a, b) => a.name.compareTo(b.name));



        setState(() {

          speddmathsdata.addAll(spmaths);
        });


      }



    }




  }

  getAptitudeTestData()async{

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getAptitudetopics+"?timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );
    print(response);
    var js=jsonDecode(response);

    AptitudeTopicsEntity entity=AptitudeTopicsEntity.fromJson(js);

    List<AptitudeTopicsData>entitydata=[];

    if(entity.status==1)
      {

        if(entity.data.length>0)
          {

            // subjects.clear();
            setState(() {

              AptitudeTopicsData topicsData=new AptitudeTopicsData();
              topicsData.id="0";
              topicsData.subject=aptitudefirstvalue;
              topicsdata.add(topicsData);
              // entitydata.add(topicsData);

              topicsdata.addAll(entity.data);


              for(int i=0;i<topicsdata.length;i++)
                {

                  subjects.add(topicsdata[i].subject);
                }



            });





          }


      }
    else{


    }



  }

  getSubjectListByTopicId(String topicid)async{
    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getSubjectDataByTopicId+"?topic_id="+topicid+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );
    print(response);
    var js=jsonDecode(response);
    AptitudeTopicsEntity entity=AptitudeTopicsEntity.fromJson(js);



    if(entity.status==1)
    {

      if(entity.data.length>0)
      {

        // subjects.clear();
        setState(() {



          topics_sorted.addAll(entity.data);






        });





      }


    }
    else{


    }

  }

}
