import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../design/ResponsiveInfo.dart';

class WebBrowserPage extends StatefulWidget {

  String urldata;

   WebBrowserPage(this.urldata) ;

  @override
  _WebBrowserPageState createState() => _WebBrowserPageState(this.urldata);
}

class _WebBrowserPageState extends State<WebBrowserPage> {

  String urldata;
  _WebBrowserPageState(this.urldata);


 late WebViewController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))

      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse("https://mysaving.in/IntegraAccount/api/ViewPdfByUrl.php"));
  }




  @override
  Widget build(BuildContext context) {




    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Colors.black),),
        centerTitle: true,




      ),
       body: WebViewWidget(controller: controller),


    );
  }
}
