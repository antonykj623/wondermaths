import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:page_indicator/page_indicator.dart';

import '../constants/AppLiterals.dart';
import '../constants/WebMethodes.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/aptitude_test_entity.dart';
import '../domain/question_data_by_test_id_entity.dart';
import '../network/apiservices.dart';

class Scholarship extends StatefulWidget {
   Scholarship() ;

  @override
  _ScholarshipState createState() => _ScholarshipState();
}

class _ScholarshipState extends State<Scholarship> {


  List<String>sylabustype=["Middle School","High School"];

  List<String>testtype=["Aptitude Test","Mental Ability Test"];

  String aptitudefirstvalue="Select Aptitude Test";
  String mentalfirstvalue="Select Mental Ability Test";

  String test="";

  int selectedquestionindex=0;

  List<String>aptitudetests=[];
  List<String>mentalabilitytests=[];

  int selectedindex=0,selectedtestindex=0;
  List<AptitudeTestData>aptitudeTestDropdown=[];

  List<QuestionDataByTestIdData> questions=[];

  List<Widget> nsdwidget = [];
  late QuestionDataByTestIdDataQuestionOptions questionoption_selected;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAptitudeTestByparams("Middleschool");
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_new, color: Color(0xffFFD507)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.black87,
        elevation: 0,
        title: Text("Scholarship/olympiad",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 14:18,color: Color(0xffFFD507)),),
        centerTitle: true,




      ),

      body:Container(


        child:  Stack(


          children: [

            Align(

                alignment: FractionalOffset.topCenter,

                child: SingleChildScrollView(

                  child: Column(

                    children: [

                      SizedBox(
                        width: double.infinity,
                        height: ResponsiveInfo.isMobile(context)? 60 : 90 ,

                        child: (sylabustype.length>0)? ListView.builder(
                            itemCount: sylabustype.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                  padding: EdgeInsets.all(
                                      ResponsiveInfo.isMobile(context) ? 10 : 15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(0xffFFD507)),
                                      borderRadius: BorderRadius.circular(8),
                                      color: (selectedindex==index)? Color(0xffFFD507) : Colors.black87,
                                    ),
                                    height: ResponsiveInfo.isMobile(context) ? 40 : 55,

                                    child: TextButton(
                                        onPressed: () async {


                                          setState(() {
                                            selectedindex=index;

                                            if(selectedindex==0)
                                            {

                                              getAptitudeTestByparams("Middleschool");
                                            }
                                            else{

                                              getAptitudeTestByparams("Highschool");
                                            }

                                          });

                                        },
                                        child: Text(
                                          sylabustype[index] ,
                                          style:  TextStyle(
                                              color:(selectedindex==index)?  Colors.white : Color(0xffFFD507),
                                              fontFamily: 'poppins'),
                                        )),
                                  )
                              ) ;
                            }) : Container(),
                      ),

                      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                        child: SizedBox(
                          width: double.infinity,
                          height: ResponsiveInfo.isMobile(context)? 60 : 90 ,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,

                            children: [


                              Expanded(child: GestureDetector(

                                child: Column(

                                  children: [

                                    Text(
                                      "Aptitude Test" ,
                                      style:  TextStyle(
                                          color:(selectedtestindex==0)?  Color(0xffFFD507) : Colors.white70,
                                          fontFamily: 'poppins'),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: ResponsiveInfo.isMobile(context)?10:15),

                                      child: Container(
                                        width: double.infinity,
                                        height: ResponsiveInfo.isMobile(context)?1:2,
                                        color: (selectedtestindex==0)?  Color(0xffFFD507) : Colors.black,

                                      ),

                                    )

                                  ],


                                ),

                                onTap: (){

                                  setState(() {


                                    selectedtestindex=0;
                                  });

                                  if(selectedindex==0)
                                  {

                                    getAptitudeTestByparams("Middleschool");
                                  }
                                  else{

                                    getAptitudeTestByparams("Highschool");
                                  }
                                },
                              )


                                ,flex: 1,),

                              Expanded(child: GestureDetector(

                                child: Column(

                                  children: [

                                    Text(
                                      "Mental Ability Test" ,
                                      style:  TextStyle(
                                          color:(selectedtestindex==1)?  Color(0xffFFD507) : Colors.white70,
                                          fontFamily: 'poppins'),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: ResponsiveInfo.isMobile(context)?10:15),

                                      child: Container(
                                        width: double.infinity,
                                        height: ResponsiveInfo.isMobile(context)?1:2,
                                        color: (selectedtestindex==1)?  Color(0xffFFD507) : Colors.black,

                                      ),

                                    )

                                  ],


                                ),

                                onTap: (){

                                  setState(() {

                                    selectedtestindex=1;
                                  });


                                  if(selectedindex==0)
                                  {

                                    getMentalAbilityTestByparams("Middleschool");
                                  }
                                  else{

                                    getMentalAbilityTestByparams("Highschool");
                                  }



                                },


                              )



                                ,flex: 1,)



                            ],

                          ),


                        ),

                      ),


                      (selectedtestindex==1) ?Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                          child: (mentalabilitytests.length>0)? Container(

                            child: DropdownMenu<String>(
                                initialSelection: mentalabilitytests.first,
                                onSelected: (String? newValue) {
                                  // This is called when the user selects an item.
                                  setState(() {

                                    // if(selectedtestindex==0)
                                    // {

                                    mentalfirstvalue=newValue!;

                                    // if(aptitudefirstvalue.compareTo("Select Aptitude Test")!=0) {
                                    //   for (int i = 0; i <
                                    //       aptitudeTestDropdown.length; i++) {
                                    //     String subj = aptitudeTestDropdown[i]
                                    //         .testName;
                                    //     if (subj.compareTo(
                                    //         aptitudefirstvalue) == 0) {
                                    //       // selectedvalue = topicsdata[i];
                                    //       getAptitudeQuestions(aptitudeTestDropdown[i].id);
                                    //
                                    //       break;
                                    //     }
                                    //   }
                                    // }

                                    // getAptitudeQuestions(aptitudefirstvalue);
                                    // }



                                    if(selectedtestindex==1)
                                    {
                                      mentalfirstvalue=newValue!;

                                      if(mentalfirstvalue.compareTo("Select Mental Ability Test")!=0) {
                                        for (int i = 0; i <
                                            aptitudeTestDropdown.length; i++) {
                                          String subj = aptitudeTestDropdown[i]
                                              .testName;
                                          if (subj.compareTo(
                                              mentalfirstvalue) == 0) {
                                            // selectedvalue = topicsdata[i];
                                            getMentalAbilityQuestions(aptitudeTestDropdown[i].id);

                                            break;
                                          }
                                        }
                                      }





                                    }



                                    // getBudgetData();






                                  });
                                },
                                dropdownMenuEntries: mentalabilitytests.map<DropdownMenuEntry<String>>((String value) {
                                  return DropdownMenuEntry<String>(value: value, label: value);
                                }).toList()
                            )  ,
                            color: Color(0xffFFD507),
                          )

                         : Container()










                        // Container(
                        //   height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,
                        //
                        //   decoration: BoxDecoration(
                        //       border: Border.all(color: Colors.black38)),
                        //
                        //   child:  DropdownButtonHideUnderline(
                        //
                        //     child: ButtonTheme(
                        //       alignedDropdown: true,
                        //       child: InputDecorator(
                        //         decoration: const InputDecoration(border: OutlineInputBorder()),
                        //         child: DropdownButtonHideUnderline(
                        //           child: DropdownButton(
                        //
                        //             isExpanded: true,
                        //             value: (selectedtestindex==0)?aptitudefirstvalue: mentalfirstvalue,
                        //             items: (selectedtestindex==0)? aptitudetests
                        //                 .map<DropdownMenuItem<String>>((String value) {
                        //               return DropdownMenuItem<String>(
                        //                 value: value,
                        //                 child: Text(value,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?11:13)),
                        //               );
                        //             }).toList() : c
                        //                 .map<DropdownMenuItem<String>>((String value) {
                        //               return DropdownMenuItem<String>(
                        //                 value: value,
                        //                 child: Text(value,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?11:13)),
                        //               );
                        //             }).toList(),
                        //             onChanged: (String? newValue) {
                        //               setState(() {
                        //
                        //                 if(selectedtestindex==0)
                        //                 {
                        //
                        //                   aptitudefirstvalue = newValue!;
                        //
                        //                   if(aptitudefirstvalue.compareTo("Select Aptitude Test")!=0) {
                        //                     for (int i = 0; i <
                        //                         aptitudeTestDropdown.length; i++) {
                        //                       String subj = aptitudeTestDropdown[i]
                        //                           .testName;
                        //                       if (subj.compareTo(
                        //                           aptitudefirstvalue) == 0) {
                        //                         // selectedvalue = topicsdata[i];
                        //                         getAptitudeQuestions(aptitudeTestDropdown[i].id);
                        //
                        //                         break;
                        //                       }
                        //                     }
                        //                   }
                        //
                        //                   // getAptitudeQuestions(aptitudefirstvalue);
                        //                 }
                        //                 else if(selectedtestindex==1)
                        //                 {
                        //                   mentalfirstvalue=newValue!;
                        //
                        //                   if(mentalfirstvalue.compareTo("Select Mental Ability Test")!=0) {
                        //                     for (int i = 0; i <
                        //                         aptitudeTestDropdown.length; i++) {
                        //                       String subj = aptitudeTestDropdown[i]
                        //                           .testName;
                        //                       if (subj.compareTo(
                        //                           mentalfirstvalue) == 0) {
                        //                         // selectedvalue = topicsdata[i];
                        //                         getMentalAbilityQuestions(aptitudeTestDropdown[i].id);
                        //
                        //                         break;
                        //                       }
                        //                     }
                        //                   }
                        //
                        //
                        //
                        //
                        //
                        //                 }
                        //
                        //
                        //
                        //                 // getBudgetData();
                        //
                        //
                        //
                        //
                        //
                        //
                        //               });
                        //             },
                        //             style: Theme.of(context).textTheme.bodyText1,
                        //
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        //
                        //
                        // ),


                      ):Container(),


                      (selectedtestindex==0)?   Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                          child: (aptitudetests.length>0)? Container(

                            child:DropdownMenu<String>(
                                initialSelection: aptitudetests.first,
                                onSelected: (String? newValue) {
                                  // This is called when the user selects an item.
                                  setState(() {

                                    // if(selectedtestindex==0)
                                    // {

                                    aptitudefirstvalue = newValue!;

                                    if(aptitudefirstvalue.compareTo("Select Aptitude Test")!=0) {
                                      for (int i = 0; i <
                                          aptitudeTestDropdown.length; i++) {
                                        String subj = aptitudeTestDropdown[i]
                                            .testName;
                                        if (subj.compareTo(
                                            aptitudefirstvalue) == 0) {
                                          // selectedvalue = topicsdata[i];
                                          getAptitudeQuestions(aptitudeTestDropdown[i].id);

                                          break;
                                        }
                                      }
                                    }

                                    // getAptitudeQuestions(aptitudefirstvalue);
                                    // }



                                    // else if(selectedtestindex==1)
                                    // {
                                    //   mentalfirstvalue=newValue!;
                                    //
                                    //   if(mentalfirstvalue.compareTo("Select Mental Ability Test")!=0) {
                                    //     for (int i = 0; i <
                                    //         aptitudeTestDropdown.length; i++) {
                                    //       String subj = aptitudeTestDropdown[i]
                                    //           .testName;
                                    //       if (subj.compareTo(
                                    //           mentalfirstvalue) == 0) {
                                    //         // selectedvalue = topicsdata[i];
                                    //         getMentalAbilityQuestions(aptitudeTestDropdown[i].id);
                                    //
                                    //         break;
                                    //       }
                                    //     }
                                    //   }
                                    //
                                    //
                                    //
                                    //
                                    //
                                    // }



                                    // getBudgetData();






                                  });
                                },
                                dropdownMenuEntries: aptitudetests.map<DropdownMenuEntry<String>>((String value) {
                                  return DropdownMenuEntry<String>(value: value, label: value);
                                }).toList()
                            )  ,
                            color: Color(0xffFFD507),
                          )


                          : Container()










                        // Container(
                        //   height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,
                        //
                        //   decoration: BoxDecoration(
                        //       border: Border.all(color: Colors.black38)),
                        //
                        //   child:  DropdownButtonHideUnderline(
                        //
                        //     child: ButtonTheme(
                        //       alignedDropdown: true,
                        //       child: InputDecorator(
                        //         decoration: const InputDecoration(border: OutlineInputBorder()),
                        //         child: DropdownButtonHideUnderline(
                        //           child: DropdownButton(
                        //
                        //             isExpanded: true,
                        //             value: (selectedtestindex==0)?aptitudefirstvalue: mentalfirstvalue,
                        //             items: (selectedtestindex==0)? aptitudetests
                        //                 .map<DropdownMenuItem<String>>((String value) {
                        //               return DropdownMenuItem<String>(
                        //                 value: value,
                        //                 child: Text(value,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?11:13)),
                        //               );
                        //             }).toList() : c
                        //                 .map<DropdownMenuItem<String>>((String value) {
                        //               return DropdownMenuItem<String>(
                        //                 value: value,
                        //                 child: Text(value,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?11:13)),
                        //               );
                        //             }).toList(),
                        //             onChanged: (String? newValue) {
                        //               setState(() {
                        //
                        //                 if(selectedtestindex==0)
                        //                 {
                        //
                        //                   aptitudefirstvalue = newValue!;
                        //
                        //                   if(aptitudefirstvalue.compareTo("Select Aptitude Test")!=0) {
                        //                     for (int i = 0; i <
                        //                         aptitudeTestDropdown.length; i++) {
                        //                       String subj = aptitudeTestDropdown[i]
                        //                           .testName;
                        //                       if (subj.compareTo(
                        //                           aptitudefirstvalue) == 0) {
                        //                         // selectedvalue = topicsdata[i];
                        //                         getAptitudeQuestions(aptitudeTestDropdown[i].id);
                        //
                        //                         break;
                        //                       }
                        //                     }
                        //                   }
                        //
                        //                   // getAptitudeQuestions(aptitudefirstvalue);
                        //                 }
                        //                 else if(selectedtestindex==1)
                        //                 {
                        //                   mentalfirstvalue=newValue!;
                        //
                        //                   if(mentalfirstvalue.compareTo("Select Mental Ability Test")!=0) {
                        //                     for (int i = 0; i <
                        //                         aptitudeTestDropdown.length; i++) {
                        //                       String subj = aptitudeTestDropdown[i]
                        //                           .testName;
                        //                       if (subj.compareTo(
                        //                           mentalfirstvalue) == 0) {
                        //                         // selectedvalue = topicsdata[i];
                        //                         getMentalAbilityQuestions(aptitudeTestDropdown[i].id);
                        //
                        //                         break;
                        //                       }
                        //                     }
                        //                   }
                        //
                        //
                        //
                        //
                        //
                        //                 }
                        //
                        //
                        //
                        //                 // getBudgetData();
                        //
                        //
                        //
                        //
                        //
                        //
                        //               });
                        //             },
                        //             style: Theme.of(context).textTheme.bodyText1,
                        //
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        //
                        //
                        // ),


                      ):Container(),


                      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                        child: Container(
                            height: (MediaQuery.of(context).size.height),
                            width: double.infinity,
                            child: PageIndicatorContainer(
                                length: nsdwidget.length,
                                align: IndicatorAlign.bottom,
                                indicatorSpace: 0,
                                padding:  EdgeInsets.all(0),

                                indicatorColor: Colors.white,
                                indicatorSelectorColor: Colors.blueGrey,
                                shape: IndicatorShape.circle(size: 0),
                                child: PageView.builder(
                                  scrollDirection: Axis.horizontal,

                                  controller: pageController,
                                  itemBuilder: (BuildContext context, int index) {

                                    print("sdnv : "+index.toString());




                                    selectedquestionindex=index;




                                    return nsdwidget[index];
                                  },
                                  itemCount: nsdwidget.length,
                                  // children: nsdwidget,
                                ))),


                      )








                    ],



                  ) ,
                )





            )



          ],


        ),
        color: Colors.black87,
        height: double.infinity,
        width: double.infinity,
      )






    );
  }

  getMentalAbilityQuestions(String testid) async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getMentalAbilityQuestionsByID+"?testid="+testid+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);

    QuestionDataByTestIdEntity questionDataByTestIdEntity=QuestionDataByTestIdEntity.fromJson(js);

    prepareQuestionAnswerWidget(questionDataByTestIdEntity.data);






  }

  getAptitudeQuestions(String testid) async
  {

    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getAptitudeQuestionListbytestId+"?testid="+testid+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);

    QuestionDataByTestIdEntity questionDataByTestIdEntity=QuestionDataByTestIdEntity.fromJson(js);

    prepareQuestionAnswerWidget(questionDataByTestIdEntity.data);






  }

  // prepareQuestionAnswerWidget(List<QuestionDataByTestIdData> data)
  // {
  //   setState(() {
  //
  //     if(data.length>0)
  //     {
  //       nsdwidget.clear();
  //       questions.clear();
  //
  //       questions.addAll(data);
  //
  //       for(int i=0;i<questions.length;i++)
  //       {
  //         QuestionDataByTestIdData data1=questions[i];
  //
  //         QuestionDataByTestIdDataQuestion byTestIdDataQuestion=data1.question[0];
  //
  //
  //
  //         nsdwidget.add(Container(
  //           height: (MediaQuery.of(context).size.height) ,
  //           width: double.infinity,
  //           child: Card(
  //               child: Column(
  //
  //                 children: [
  //
  //                   Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),
  //
  //                       child:HtmlWidget(
  //
  //                         byTestIdDataQuestion.question,
  //
  //
  //                         customStylesBuilder: (element) {
  //                           if (element.classes.contains('foo')) {
  //                             return {'color': 'red'};
  //                           }
  //
  //                           return null;
  //                         },
  //
  //
  //                         onErrorBuilder: (context, element, error) => Text('$element error: $error'),
  //                         onLoadingBuilder: (context, element, loadingProgress) => CircularProgressIndicator(),
  //
  //                         renderMode: RenderMode.column,
  //
  //                         textStyle: TextStyle(fontSize: 14),
  //                       )
  //
  //
  //
  //                   ),
  //
  //                   Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),
  //
  //                     child:ListView.builder(
  //                         itemCount: data1.questionOptions.length,
  //                         primary: false,
  //                         shrinkWrap: true,
  //                         itemBuilder: (BuildContext context, int index) {
  //                           return ListTile(
  //                               leading: GestureDetector(
  //
  //                                 child:(data1.questionOptions[index].selected==0)?
  //
  //                                 Icon(Icons.radio_button_off,size: ResponsiveInfo.isMobile(context)?25:30,color: Colors.black12,) :
  //
  //                                 Icon(Icons.radio_button_checked,size: ResponsiveInfo.isMobile(context)?25:30,color: Color(0xffFFD507),)
  //                                 ,
  //
  //                                 onTap: (){
  //
  //                                   for(int i=0;i<data1.questionOptions.length;i++)
  //                                   {
  //
  //                                     data1.questionOptions[i].selected=0;
  //                                   }
  //
  //
  //                                   data1.questionOptions[index].selected=1;
  //
  //
  //                                   prepareQuestionAnswerWidget(data);
  //
  //
  //
  //
  //                                 },
  //                               )
  //
  //                               ,
  //
  //                               title: HtmlWidget(data1.questionOptions[index].option));
  //                         }) ,
  //
  //                   ),
  //
  //
  //                   Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),
  //
  //
  //                     child: Row(
  //                       crossAxisAlignment: CrossAxisAlignment.center,
  //                       mainAxisAlignment: MainAxisAlignment.center,
  //
  //                       children: [
  //
  //
  //
  //
  //                         Expanded(child: TextButton(onPressed: () {
  //
  //                           bool selected=false;
  //
  //
  //
  //                           for(int i=0;i<data1.questionOptions.length;i++) {
  //                             if (data1.questionOptions[i].selected == 1) {
  //
  //                               selected=true;
  //                               questionoption_selected=data1.questionOptions[i];
  //                               break;
  //
  //                             }
  //                           }
  //
  //                           if(selected)
  //                           {
  //
  //                             String optionnumber=questionoption_selected.optNumber;
  //                             String answer=   byTestIdDataQuestion.answer;
  //
  //                             showDialog(
  //                                 context: context,
  //                                 builder: (BuildContext context){
  //                                   return AlertDialog(
  //                                     title: (optionnumber.compareTo(answer)==0)? Text("Right Answer",style: TextStyle(color: Color(0xffFFD507),fontSize: ResponsiveInfo.isMobile(context)?14:17),) :
  //                                     Text("Wrong Answer",style: TextStyle(color: Colors.red,fontSize: ResponsiveInfo.isMobile(context)?14:17),),
  //                                     content: SingleChildScrollView(
  //
  //                                       child: Column(
  //                                         mainAxisAlignment: MainAxisAlignment.center,
  //                                         crossAxisAlignment: CrossAxisAlignment.center,
  //                                         children: [
  //
  //                                           Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),
  //
  //
  //                                             child: HtmlWidget(
  //
  //                                               byTestIdDataQuestion.explanation,
  //
  //
  //                                               customStylesBuilder: (element) {
  //                                                 if (element.classes.contains('foo')) {
  //                                                   return {'color': 'red'};
  //                                                 }
  //
  //                                                 return null;
  //                                               },
  //
  //
  //                                               onErrorBuilder: (context, element, error) => Text('$element error: $error'),
  //                                               onLoadingBuilder: (context, element, loadingProgress) => CircularProgressIndicator(),
  //
  //                                               renderMode: RenderMode.column,
  //
  //                                               textStyle: TextStyle(fontSize: 14),
  //                                             ),
  //
  //                                           ),
  //
  //
  //                                           Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),
  //
  //
  //                                             child:
  //
  //
  //                                             TextButton(onPressed: () {
  //
  //                                               Navigator.pop(context);
  //
  //                                             }, child: Text("Close",style: TextStyle(color: Colors.blue,fontSize: ResponsiveInfo.isMobile(context)?14:18),),
  //
  //
  //
  //                                             ),
  //
  //                                           )
  //
  //
  //
  //
  //                                         ],
  //
  //
  //                                       ),
  //
  //
  //                                     ),
  //                                   );
  //                                 }
  //                             );
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //                           }
  //                           else{
  //
  //
  //                             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //                               content: Text("Please select an answer"),
  //                             ));
  //                           }
  //
  //
  //
  //
  //
  //                         }, child: Text("Submit",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?13:17),),
  //
  //
  //                         ),flex: 1,),
  //
  //
  //
  //                         (selectedquestionindex==pageController.page!.toInt()-1)?Container():  Expanded(child: TextButton(onPressed: () {
  //
  //
  //
  //                           pageController.animateToPage(pageController.page!.toInt() +1,
  //                               duration: Duration(milliseconds: 400),
  //                               curve: Curves.easeIn
  //                           );
  //
  //
  //                         }, child: Text("Next",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?13:17),),
  //
  //
  //                         ),flex: 1,)
  //
  //
  //                       ],
  //
  //
  //                     ),
  //
  //
  //
  //                   )
  //
  //
  //
  //
  //
  //                 ],
  //
  //               )
  //
  //
  //
  //
  //           ),
  //         ));
  //
  //
  //
  //
  //       }
  //
  //
  //
  //
  //     }
  //
  //   });
  // }

  prepareQuestionAnswerWidget(List<QuestionDataByTestIdData> data)
  {
    setState(() {

      if(data.length>0)
      {
        nsdwidget.clear();
        questions.clear();

        questions.addAll(data);
        // questions.shuffle();

        for(int i=0;i<questions.length;i++)
        {
          QuestionDataByTestIdData data1=questions[i];

          QuestionDataByTestIdDataQuestion byTestIdDataQuestion=data1.question[0];



          nsdwidget.add(Container(
            height: (MediaQuery.of(context).size.height) ,
            width: double.infinity,
            child: Card(
                child: Column(

                  children: [

                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                        child:HtmlWidget(

                          byTestIdDataQuestion.question,


                          customStylesBuilder: (element) {
                            if (element.classes.contains('foo')) {
                              return {'color': 'red'};
                            }

                            return null;
                          },


                          onErrorBuilder: (context, element, error) => Text('$element error: $error'),
                          onLoadingBuilder: (context, element, loadingProgress) => CircularProgressIndicator(),

                          renderMode: RenderMode.column,

                          textStyle: TextStyle(fontSize: 14),
                        )



                    ),

                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),

                      child:ListView.builder(
                          itemCount: data1.questionOptions.length,
                          primary: false,
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                                leading: GestureDetector(

                                  child:(data1.questionOptions[index].selected==0)?

                                  Icon(Icons.radio_button_off,size: ResponsiveInfo.isMobile(context)?25:30,color: Colors.black12,) :

                                  Icon(Icons.radio_button_checked,size: ResponsiveInfo.isMobile(context)?25:30,color: Color(0xffFFD507),)
                                  ,

                                  onTap: (){

                                    for(int i=0;i<data1.questionOptions.length;i++)
                                    {

                                      data1.questionOptions[i].selected=0;
                                    }


                                    data1.questionOptions[index].selected=1;


                                    prepareQuestionAnswerWidget(data);




                                  },
                                )

                                ,

                                title: HtmlWidget(data1.questionOptions[index].option));
                          }) ,

                    ),


                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?5:10),


                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,

                        children: [




                          Expanded(child: TextButton(onPressed: () {

                            bool selected=false;



                            for(int i=0;i<data1.questionOptions.length;i++) {
                              if (data1.questionOptions[i].selected == 1) {

                                selected=true;
                                questionoption_selected=data1.questionOptions[i];
                                break;

                              }
                            }

                            if(selected)
                            {

                              String optionnumber=questionoption_selected.optNumber;
                              String answer=   byTestIdDataQuestion.answer;

                              showDialog(
                                  context: context,
                                  builder: (BuildContext context){
                                    return AlertDialog(
                                      title: (optionnumber.compareTo(answer)==0)? Text("Right Answer",style: TextStyle(color: Colors.green,fontSize: ResponsiveInfo.isMobile(context)?14:17),) :
                                      Text("Wrong Answer",style: TextStyle(color: Colors.red,fontSize: ResponsiveInfo.isMobile(context)?14:17),),
                                      content: Container(
                                          width: double.infinity,
                                          height: MediaQuery.of(context).size.height/2.4,



                                          child: SingleChildScrollView(

                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [

                                                Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                                                  child: HtmlWidget(

                                                    byTestIdDataQuestion.explanation,

                                                  ),

                                                ),


                                                Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?10:15),


                                                    child: Container(
                                                      width:ResponsiveInfo.isMobile(context)?100:150,
                                                      height:ResponsiveInfo.isMobile(context)?60:80,
                                                      color:Colors.blue,

                                                      child:TextButton(onPressed: () {

                                                        Navigator.pop(context);

                                                      }, child: Text("Close",style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)?14:18),),



                                                      ),
                                                    )




                                                ),


                                                Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?5:10),


                                                    child: Row(

                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        mainAxisAlignment: MainAxisAlignment.center,


                                                        children:[

                                                          Expanded(child: Text((pageController.page!.toInt()+1).toString()+"/"+questions.length.toString()),flex: 1,)

                                                          ,


                                                          ((nsdwidget.length.toInt())==questions.length.toInt()-1)?Container():  Expanded(child: TextButton(onPressed: () {

                                                            Navigator.pop(context);

                                                            pageController.animateToPage(pageController.page!.toInt() +1,
                                                                duration: Duration(milliseconds: 400),
                                                                curve: Curves.easeIn
                                                            );


                                                          }, child: Text(">",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?20:30),),


                                                          ),flex: 1,)



                                                        ]

                                                    )







                                                )




                                              ],


                                            ),
                                          )













                                      ),
                                    );
                                  }
                              );











                            }
                            else{


                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Please select an answer"),
                              ));
                            }





                          }, child: Text("Submit",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?13:17),),


                          ),flex: 1,),





                          ((nsdwidget.length.toInt())==questions.length.toInt()-1)?Container():  Expanded(child: TextButton(onPressed: () {



                            pageController.animateToPage(pageController.page!.toInt() +1,
                                duration: Duration(milliseconds: 400),
                                curve: Curves.easeIn
                            );


                          }, child: Text(">",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?20:30),),


                          ),flex: 1,)


                        ],


                      ),



                    )
                    ,
                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?5:10),


                      child: Text((nsdwidget.length.toInt()+1).toString()+"/"+questions.length.toString()),

                    )





                  ],

                )




            ),
          ));




        }




      }

    });
  }




  getAptitudeTestByparams(String params)
  async {
    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getAptitudeTestByparams+"?param="+params+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);
    AptitudeTestEntity entity=AptitudeTestEntity.fromJson(js);
    if(entity.status==1)
    {


      // List<AptitudeTestData>aptitudetests=entity.data;
      if(entity.data.length>0)
      {

        setState(() {
          aptitudetests.clear();
          aptitudeTestDropdown.clear();
          aptitudetests.add(aptitudefirstvalue);
          for(int i=0;i<entity.data.length;i++)
          {

            aptitudetests.add(entity.data[i].testName);

          }

          AptitudeTestData data=new AptitudeTestData();
          data.testName=aptitudefirstvalue;
          aptitudeTestDropdown.add(data);

          aptitudeTestDropdown.addAll(entity.data);

        });




      }




    }
    else{



    }




  }


  getMentalAbilityTestByparams(String params)
  async {
    var date = new DateTime.now().toIso8601String();

    ApiServices apiServices=new ApiServices();

    String url = AppLiterals.
    wondermaths_baseurl+ WebServiceMethodes.getMentalAbilityTestbyParam+"?param="+params+"&timestamp="+date.toString();

    String response = await apiServices.getmethod(context,  null, url
    );

    print(response);

    var js=jsonDecode(response);
    AptitudeTestEntity entity=AptitudeTestEntity.fromJson(js);
    if(entity.status==1)
    {


      // List<AptitudeTestData>aptitudetests=entity.data;
      if(entity.data.length>0)
      {

        setState(() {
          mentalabilitytests.clear();
          aptitudeTestDropdown.clear();
          mentalabilitytests.add(mentalfirstvalue);
          for(int i=0;i<entity.data.length;i++)
          {

            mentalabilitytests.add(entity.data[i].testName);

          }

          AptitudeTestData data=new AptitudeTestData();
          data.testName=aptitudefirstvalue;
          aptitudeTestDropdown.add(data);

          aptitudeTestDropdown.addAll(entity.data);

        });




      }




    }
    else{



    }




  }
}
