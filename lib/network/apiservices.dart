import 'package:flutter/cupertino.dart';
import 'package:myapp/constants/AppLiterals.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
class  ApiServices{


  Future<String?> postmethod(BuildContext context,dynamic requestModel,String urlmethode) async {


    String data="";
    var date = new DateTime.now().toIso8601String();

    final preferenceDataStorage = await SharedPreferences
        .getInstance();
    String? token = preferenceDataStorage
        .getString(
        AppLiterals.Tokenkey);
    print(token);
    print(urlmethode);

    var url = Uri.parse(urlmethode);
    final response = await http.post(url, headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': (token != null) ? token : "",
      'timestamp':date
    }, body: requestModel);
    print(response.body.toString());
    if (response.statusCode == 200) {


      data=response.body.toString();

      print(data);


    } else {
      throw Exception("Failed to Data");
    }



    return data;
  }


  Future<String> getmethod(BuildContext context,dynamic requestModel,String url) async {

    String responsedata="";




    final preferenceDataStorage = await SharedPreferences
        .getInstance();
    String? token = preferenceDataStorage
        .getString(
        AppLiterals.Tokenkey);
    print(url);
    print(token);
    Map<String, String> requestHeaders = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': token.toString()
    };
    var dataasync = await http.get(
        Uri.parse(url),

        headers: requestHeaders

    );
    responsedata = dataasync.body;





    return responsedata;
  }
}
