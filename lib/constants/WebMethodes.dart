class WebServiceMethodes{



  static  String UserLoginSave='UserLogin.php';
  static String getMemberDetailsByID="getMemberDetailsByID.php";
  static  String getUserByMobile="getUserByMobile.php";
  static  String userAuthenticate="UserAuthenticateForWonderMaths.php";
  static String getSliderImages="getSliderImages.php";
  static String getSpeedMaths="getSpeedMaths.php";
  static String getCareerSuccess="getCareerSuccess.php";
  static String getMentaAbilityFiles="getMentaAbilityFiles.php";
  static String getAptitudetopics="getAptitudetopics.php";
  static String getSubjectDataByTopicId="getSubjectDataByTopicId.php";
  static String getAptitudeTestByparams="getAptitudeTestByparams.php";
  static String getMentalAbilityTestbyParam="getMentalAbilityTestbyParam.php";
  static String getAptitudeQuestionListbytestId="getAptitudeQuestionListbytestId.php";
  static String getMentalAbilityQuestionsByID="getMentalAbilityQuestionsByID.php";
  static String getVideosbyClass="getVideosbyClass.php";
  static String getTopicForPdfByClass="getTopicForPdfByClass.php";
  static String getPdfByTopicIdAndClass="getPdfByTopicIdAndClass.php";
  static String getClassWiseAptitudeTest="getClassWiseAptitudeTest.php";
  static String getClassWiseMentalAbilityTests="getClassWiseMentalAbilityTests.php";
  static  String getUserDetails="getUserDetails.php";
  static final String updateProfile="UserProfileUpdate.php";
  static final String uploadUserProfile="uploadUserProfile.php";
  static final String getNotifications="getNotifications.php";
  static final String deleteAccount="deleteAccount.php";
  static final String smsMethode="httpapi";
  static final String changePassword="changePassword.php";
  static final String wonderMathsVersion="wonderMathsVersion.php";
  static final String updateWondermathsDeviceId="updateWondermathsDeviceId.php";




}