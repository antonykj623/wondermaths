import 'ServerMessageType.dart';

class AppLiterals{



  static String baseurl="https://mysaving.in/WonderMaths/api/";
  static final String saveapp_baseurl="https://mysaving.in/IntegraAccount/api/";
  static final String wondermaths_baseurl="https://mysaving.in/IntegraAccount/WonderMaths/api/";
  static final String wondermaths_slider_image_baseurl="https://mysaving.in/IntegraAccount/WonderMaths/images/";


  static final String speedmathsurl="https://mysaving.in/IntegraAccount/WonderMaths/courses/speed-maths/";
  static final String careersuccessurl="https://mysaving.in/IntegraAccount/WonderMaths/courses/career-success/";
  static final String mentalabilitytestdataurl="https://mysaving.in/IntegraAccount/WonderMaths/courses/mentalability/";
  static final String questionbakdataurl="https://mysaving.in/IntegraAccount/WonderMaths/courses/topicsPdf/";

  static final  String profileimgbaseurl="https://mysaving.in/uploads/profile/";
  static final String activationredirecturl="https://mysaveapp.com/purchase";
  static final String smsbaseurl="http://eapoluenterprise.in/httpapi/";

  static final String domain="https://mysaving.in/";






  static String Tokenkey="tokenkey";

  static String Classid="Classid";

  static String productname="wonder-maths";

  static String activationperiodexpire="activationperiodexpire";






  static final String sender="CGSAVE";
  static final String forgotpasstemplateid="1007856104698741987";
  static final String registrationtemplateid="1007625690429475781";
  static final String registration_Confirm_templateid="1007134283594642980";

  static final String route="2";

  static final String type="1";

  static final String apikey="bf25917c3254cfe9f50694f24884f23a";




  static String buildServerMessage(int Type, String otp,String username,String password)
  {
    String message="";

    if(Type==ServerMessageType.forgot_password)
    {

      message="Your OTP for forgot Password is "+otp+" .CGSAVE";
    }

    if(Type==ServerMessageType.registration_Confirm_password)
    {

      message="Your registration is successful. Your Registration ID "+username+" Passowrd "+password+" .CGSAVE";
    }

    if(Type==ServerMessageType.registration)
    {

      message="Dear "+username+" Welcome to Wonder Maths App - My Personal App. Your OTP is "+otp+" CGSAVE";
    }


    return message;
  }



}